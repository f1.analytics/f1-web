const HtmlWebPackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
	context: path.resolve(__dirname, 'src/react'),
	mode: 'development',
	entry: ['@babel/polyfill', './index.js'],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader'
					},
					{
						loader: 'eslint-loader',
						options: {
							fix: true
						}
					}
				]
			},
			{
				test: /\.(s*)css$/,
				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader'
					},
					{
						loader: 'sass-loader'
					}
				]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: {
					loader: 'file-loader'
				}
			},
			{
				test: [/\.eot$/, /\.ttf$/, /\.svg$/, /\.woff$/, /\.woff2$/],
				use: {
					loader: 'file-loader'
				}
			},
			{
				test: /\.svg$/,
				loader: 'svg-react-loader'
			}
		]
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: 'index.html'
		})
	],
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			'react-spring/renderprops$': 'react-spring/renderprops.cjs'
		}
	},
	devtool: 'inline-source-map',
	devServer: {
		historyApiFallback: true,
		proxy: {
			'/api': 'http://localhost:3000'
		}
	}
};
