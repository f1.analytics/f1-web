const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SentryPlugin = require('@sentry/webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');

module.exports = {
	context: path.resolve(__dirname, 'src/react'),
	mode: 'production',
	devtool: '',
	entry: ['@babel/polyfill', './index.js'],
	output: {
		filename: '[name].[contenthash].bundle.js',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: 'babel-loader'
					}
				]
			},
			{
				test: /\.(s*)css$/,
				use: [
		            MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader'
					},
					{
						loader: 'sass-loader'
					}
				]
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: {
					loader: 'file-loader'
				}
			},
			{
				test: [/\.eot$/, /\.ttf$/, /\.svg$/, /\.woff$/, /\.woff2$/],
				use: {
					loader: 'file-loader'
				}
			},
			{
				test: /\.svg$/,
				loader: 'svg-react-loader'
			}
		]
	},
	optimization: {
		splitChunks: {
			cacheGroups: {
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: 'vendors',
					chunks: 'all'
				}
			}
		}
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: 'index.html'
		}),
        new SentryPlugin({
			include: './src/react'
        }),
		new webpack.SourceMapDevToolPlugin({
			filename: '[name].[contenthash].js.map',
			exclude: ['vendor'],
			append: '//# sourceMappingURL=[url]'
		}),
		new BrotliPlugin({
			asset: '[path].br[query]',
			test: /\.(js|css|html|svg)$/,
			threshold: 10240,
			minRatio: 0.8
		}),
	    new MiniCssExtractPlugin({
	      filename: '[name].css',
	      chunkFilename: '[id].css',
	      ignoreOrder: false
	    })
	],
	resolve: {
		extensions: ['.js', '.jsx'],
		alias: {
			'react-spring/renderprops$': 'react-spring/renderprops.cjs'
		}
	}
};
