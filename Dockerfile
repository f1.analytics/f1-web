FROM node:8

ARG SENTRY_AUTH_TOKEN
ENV SENTRY_AUTH_TOKEN=$SENTRY_AUTH_TOKEN

WORKDIR /usr/src/f1-web
COPY . .
RUN npm install
RUN npm run build
EXPOSE 80
CMD [ "npm", "start" ]
