import axios from 'axios';
import store from './store';
import jwt from 'jsonwebtoken';
import { refreshAccessToken } from './api';
import { setAccessToken, clearSession } from './actions';
import { toastResponseError } from './utility';

const jwtInstance = axios.create();

jwtInstance.interceptors.request.use(async config => {
	let accessToken = store.getState().session.accessToken;

	if (accessToken) {
		if (isTokenExpired(accessToken)) {
			await refreshExpiredToken();
			accessToken = store.getState().session.accessToken;
		}
		config.headers.Authorization = `Bearer ${accessToken}`;
	}

	return config;
}, error => Promise.reject(error));

const isTokenExpired = token => {
	const now = new Date();
	const expTime = jwt.decode(token).exp;
	const timeLeft = expTime * 1000 - now.getTime();
	const timeLeftInMinutes = timeLeft / (60 * 1000);

	return timeLeftInMinutes - 1 < 0;
};

const refreshExpiredToken = async () => {
	try {
		const refreshToken = store.getState().session.refreshToken;
		const { accessToken } = await refreshAccessToken(refreshToken);
		store.dispatch(setAccessToken(accessToken));
	} catch (error) {
		store.dispatch(clearSession());
		toastResponseError(error);
	}
};

export const jwtAxios = jwtInstance;
