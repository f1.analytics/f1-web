import {
	LOGIN,
	LOGIN_FINISHED,
	REGISTER,
	REGISTER_FINISHED,
	SET_EMAIL_TAKEN_ERROR,
	SET_USERNAME_TAKEN_ERROR,
	SET_WRONG_CREDENTIALS_ERROR,
} from './user-types';

export const initialState = {
	loginLoading: false,
	registerLoading: false,
	emailTakenError: false,
	usernameTakenError: false,
	wrongCredentialsError: false,
};

const user = (state = initialState, action) => {
	switch (action.type) {
		case LOGIN:
			return Object.assign({}, state, {
				loginLoading: true,
			});
		case LOGIN_FINISHED:
			return Object.assign({}, state, {
				loginLoading: false,
			});
		case REGISTER:
			return Object.assign({}, state, {
				registerLoading: true,
			});
		case REGISTER_FINISHED:
			return Object.assign({}, state, {
				registerLoading: false,
			});
		case SET_USERNAME_TAKEN_ERROR:
			return Object.assign({}, state, {
				usernameTakenError: action.value,
			});
		case SET_EMAIL_TAKEN_ERROR:
			return Object.assign({}, state, {
				emailTakenError: action.value,
			});
		case SET_WRONG_CREDENTIALS_ERROR:
			return Object.assign({}, state, {
				wrongCredentialsError: action.value,
			});
		default:
			return state;
	}
};

export default user;
