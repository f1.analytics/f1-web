import { takeLatest } from 'redux-saga/effects';
import { LOGIN, REGISTER } from './user-types';
import { responseCodes } from '../../constants';
import * as userAPI from './user-api';
import userSaga, { handleLogin, handleRegister } from './user-saga';
import { recordSaga } from '../../utility';
import { toastResponseError } from '../../utility/ui';
import {
	loginFinished,
	registerFinished,
	setTokens,
	setUserData,
	setUsernameTakenError,
	setEmailTakenError,
	setWrongCredentialsError,
} from '../../actions';

jest.mock('../../utility/ui', () => ({
	toastResponseError: jest.fn(),
}));

describe('User saga tests', () => {
	const email = 'email@gmail.com';
	const username = 'user1';
	const password = 'password1234';
	const userId = 2;

	beforeEach(() => {
		jest.resetAllMocks();
	});

	describe('Function userSaga tests', () => {
		const gen = userSaga();

		it('Should listen to redux actions', () => {
			expect(gen.next().value).toEqual(takeLatest(LOGIN, handleLogin));
			expect(gen.next().value).toEqual(takeLatest(REGISTER, handleRegister));
			expect(gen.next().done).toBe(true);
		});
	});

	describe('Function handleLogin tests', () => {
		it('Should get tokens, get userData and finish login action', async () => {
			const accessToken = 'accessToken';
			const refreshToken = 'refreshToken';
			const userData = { email };
			userAPI.login = jest.fn(() => ({ accessToken, refreshToken, userId }));
			userAPI.getUserData = jest.fn(() => userData);

			const dispatched = await recordSaga(handleLogin, { email, password });

			expect(userAPI.login).toHaveBeenCalledTimes(1);
			expect(userAPI.login).toHaveBeenCalledWith(email, password);
			expect(dispatched).toContainEqual(setTokens(accessToken, refreshToken));
			expect(userAPI.getUserData).toHaveBeenCalledTimes(1);
			expect(userAPI.getUserData).toHaveBeenCalledWith(userId);
			expect(dispatched).toContainEqual(setUserData(userData));
			expect(dispatched).toContainEqual(loginFinished());
		});

		it('Should show error and finish login action', async () => {
			const error = new Error('test');
			userAPI.login = jest.fn(() => {
				throw error;
			});

			const dispatched = await recordSaga(handleLogin, { email, password });

			expect(toastResponseError).toHaveBeenCalledTimes(1);
			expect(toastResponseError).toHaveBeenCalledWith(error);
			expect(dispatched).toContainEqual(loginFinished());
		});

		it('Should set wrong credentials error', async () => {
			const error = new Error('test');
			error.response = { data: { code: responseCodes.WRONG_CREDENTIALS } };
			userAPI.login = jest.fn(() => {
				throw error;
			});

			const dispatched = await recordSaga(handleLogin, { email, password });

			expect(dispatched).toContainEqual(setWrongCredentialsError(true));
		});
	});

	describe('Function handleRegister tests', () => {
		it('Should register user, immediately login and finish register', async () => {
			userAPI.register = jest.fn();
			userAPI.login = jest.fn();

			const dispatched = await recordSaga(handleRegister, { email, username, password });

			expect(userAPI.register).toHaveBeenCalledTimes(1);
			expect(userAPI.register).toHaveBeenCalledWith(email, username, password);
			expect(userAPI.login).toHaveBeenCalledTimes(1);
			expect(userAPI.login).toHaveBeenCalledWith(email, password);
			expect(dispatched).toContainEqual(registerFinished());
		});

		it('Should show error and finish register action', async () => {
			const error = new Error('test');
			userAPI.register = jest.fn(() => {
				throw error;
			});

			const dispatched = await recordSaga(handleRegister, { email, username, password });

			expect(toastResponseError).toHaveBeenCalledTimes(1);
			expect(toastResponseError).toHaveBeenCalledWith(error);
			expect(dispatched).toContainEqual(registerFinished());
		});

		it('Should set usernameTakenError', async () => {
			const error = new Error('test');
			error.response = { data: { code: 'usernameTaken' } };
			userAPI.register = jest.fn(() => {
				throw error;
			});

			const dispatched = await recordSaga(handleRegister, { email, username, password });

			expect(dispatched).toContainEqual(setUsernameTakenError(true));
		});

		it('Should set emailTakenError', async () => {
			const error = new Error('test');
			error.response = { data: { code: 'emailTaken' } };
			userAPI.register = jest.fn(() => {
				throw error;
			});

			const dispatched = await recordSaga(handleRegister, { email, username, password });

			expect(dispatched).toContainEqual(setEmailTakenError(true));
		});
	});
});
