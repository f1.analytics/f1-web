import reducer, { initialState } from './user-reducer';
import {
	loginFinished,
	login,
	register,
	registerFinished,
	setUsernameTakenError,
	setEmailTakenError,
	setWrongCredentialsError,
} from './user-actions';

describe('User reducer tests', () => {
	const email = 'email@gmail.com';
	const username = 'user1';
	const password = 'password123';

	it('Should return initial state', () => {
		expect(reducer(undefined, {})).toEqual(initialState);
	});

	it('Should handle login', () => {
		expect(reducer(undefined, login(email, password)).loginLoading).toBe(true);
	});

	it('Should handle loginFinished', () => {
		expect(reducer(undefined, loginFinished()).loginLoading).toBe(false);
	});

	it('Should handle register', () => {
		expect(reducer(undefined, register(email, username, password)).registerLoading).toBe(true);
	});

	it('Should handle registerFinished', () => {
		expect(reducer(undefined, registerFinished()).registerLoading).toBe(false);
	});

	it('Should handle setUsernameTakenError', () => {
		expect(reducer(undefined, setUsernameTakenError(true)).usernameTakenError).toBe(true);
	});

	it('Should handle registerFinished', () => {
		expect(reducer(undefined, setEmailTakenError(true)).emailTakenError).toBe(true);
	});

	it('Should handle setWrongCredentialsError', () => {
		expect(reducer(undefined, setWrongCredentialsError(true)).wrongCredentialsError).toBe(true);
	});
});
