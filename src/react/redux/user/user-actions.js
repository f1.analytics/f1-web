import {
	LOGIN,
	LOGIN_FINISHED,
	REGISTER,
	REGISTER_FINISHED,
	SET_USERNAME_TAKEN_ERROR,
	SET_EMAIL_TAKEN_ERROR,
	SET_WRONG_CREDENTIALS_ERROR,
} from './user-types';

export const login = (email, password) => ({
	type: LOGIN,
	email,
	password,
});

export const register = (email, username, password) => ({
	type: REGISTER,
	email,
	username,
	password,
});

export const loginFinished = () => ({
	type: LOGIN_FINISHED,
});

export const registerFinished = () => ({
	type: REGISTER_FINISHED,
});

export const setUsernameTakenError = value => ({
	type: SET_USERNAME_TAKEN_ERROR,
	value,
});

export const setEmailTakenError = value => ({
	type: SET_EMAIL_TAKEN_ERROR,
	value,
});

export const setWrongCredentialsError = value => ({
	type: SET_WRONG_CREDENTIALS_ERROR,
	value,
});
