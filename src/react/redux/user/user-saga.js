import { takeLatest, put, call } from 'redux-saga/effects';
import { LOGIN, REGISTER } from './user-types';
import * as userAPI from './user-api';
import { responseCodes } from '../../constants';
import { toastResponseError } from '../../utility';
import {
	loginFinished,
	registerFinished,
	setTokens,
	setUserData,
	setUsernameTakenError,
	setEmailTakenError,
	setWrongCredentialsError,
} from '../../actions';

export function* handleLogin(payload) {
	try {
		const { email, password } = payload;

		const { accessToken, refreshToken, userId } = yield call(userAPI.login, email, password);
		yield put(setTokens(accessToken, refreshToken));

		const userData = yield call(userAPI.getUserData, userId);
		yield put(setUserData(userData));

		yield put(loginFinished());
	} catch (error) {
		yield call(setErrorIfWrongCredentials, error);
		yield call(toastResponseError, error);
		yield put(loginFinished());
	}
}

export function* handleRegister(payload) {
	try {
		const { email, username, password } = payload;

		yield call(userAPI.register, email, username, password);
		yield call(handleLogin, { email, password });
		yield put(registerFinished());
	} catch (error) {
		yield call(setErrorIfFieldTaken, error);
		yield call(toastResponseError, error);
		yield put(registerFinished());
	}
}

function* setErrorIfWrongCredentials(error) {
	if (error.response) {
		if (error.response.data.code === responseCodes.WRONG_CREDENTIALS) {
			yield put(setWrongCredentialsError(true));
		}
	}
}

function* setErrorIfFieldTaken(error) {
	if (error.response) {
		if (error.response.data.code === responseCodes.USERNAME_TAKEN) {
			yield put(setUsernameTakenError(true));
		} else if (error.response.data.code === responseCodes.EMAIL_TAKEN) {
			yield put(setEmailTakenError(true));
		}
	}
}

export default function* userSaga() {
	yield takeLatest(LOGIN, handleLogin);
	yield takeLatest(REGISTER, handleRegister);
}
