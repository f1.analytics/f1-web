import axios from 'axios';
import { jwtAxios } from '../../axios';

export const login = async (email, password) => {
	const { data } = await axios.post('/api/tokens', {
		email,
		password,
	}).catch(error => {
		throw error;
	});

	return data;
};

export const refreshAccessToken = async refreshToken => {
	const { data } = await axios.post('/api/tokens/refresh', {
		refreshToken,
	}).catch(error => {
		throw error;
	});

	return data;
};

export const register = async (email, username, password) => {
	await axios.post('/api/users', {
		email,
		username,
		password,
	}).catch(error => {
		throw error;
	});
};

export const getUserData = async (userId) => {
	const { data } = await jwtAxios.get(`/api/users/${userId}`).catch(error => {
		throw error;
	});

	return data;
};
