import { jwtAxios } from '../../axios';

export const getDashboardTypes = async () => {
  const { data } = await jwtAxios.get('/api/settings/dashboardTypes').catch(error => {
    throw error;
  });
  return data;
};
export const getDashboardType = async (id) => {
  const { data } = await jwtAxios.get(`/api/settings/dashboardTypes/${id}`).catch(error => {
    throw error;
  });
  return data;
};
export const getDataQueries = async () => {
  const { data } = await jwtAxios.get('/api/settings/dataQueryOptions').catch(error => {
    throw error;
  });
  return data;
};
export const getSubjectSelections = async () => {
  const { data } = await jwtAxios.get('/api/settings/subjectSelections').catch(error => {
    throw error;
  });
  return data;
};
export const saveDashboardType = async (dashboardType) => {
  await jwtAxios.put('/api/settings/dashboardTypes', dashboardType).catch(error => {
    throw error;
  });
};
export const deleteDashboardType = async (id) => {
  await jwtAxios.delete(`/api/settings/dashboardTypes/${id}`, id).catch(error => {
    throw error;
  });
};
export const getSubjectSelection = async (id) => {
  const { data } = await jwtAxios.get(`/api/settings/subjectSelections/${id}`).catch(error => {
    throw error;
  });
  return data;
};
export const saveSubjectSelection = async (subjectSelection) => {
  await jwtAxios.put('/api/settings/subjectSelections', subjectSelection).catch(error => {
    throw error;
  });
};
export const deleteSubjectSelection = async (id) => {
  await jwtAxios.delete(`/api/settings/subjectSelections/${id}`, id).catch(error => {
    throw error;
  });
};
export const getDataQuery = async (id) => {
  const { data } = await jwtAxios.get(`/api/settings/dataQueryOptions/${id}`).catch(error => {
    throw error;
  });
  return data;
};
export const getExampleResolvedData = async (dataQueryId) => {
  const { data } = await jwtAxios.get(`/api/data/example/${dataQueryId}`).catch(error => {
    throw error;
  });
  return data;
};

