import { takeLatest, call, put } from 'redux-saga/effects';

import {
  GET_DASHBOARD_TYPES,
  GET_DASHBOARD_TYPE,
  GET_DATA_QUERIES,
  GET_SUBJECT_SELECTIONS,
  SAVE_DASHBOARD_TYPE,
  DELETE_DASHBOARD_TYPE,
  SAVE_SUBJECT_SELECTION,
  GET_SUBJECT_SELECTION,
  DELETE_SUBJECT_SELECTION,
  GET_DATA_QUERY,
  GET_EXAMPLE_RESOLVED_DATA,
} from './admin-types';
import * as adminAPI from './admin-api';
import { toastResponseError } from '../../utility';
import {
  setDashboardTypes,
  setDashboardType,
  setDataQueries,
  setSubjectSelections,
  deleteDashboardTypeFinish,
  setSubjectSelection,
  saveSubjectSelectionFinish,
  deleteSubjectSelectionFinish,
  setDataQuery, setExampleResolvedData,
} from './admin-actions';

export function* handleDashboardTypesFetch() {
  try {
    const dashboardTypes = yield call(adminAPI.getDashboardTypes);
    yield put(setDashboardTypes(dashboardTypes));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(setDashboardTypes(null));
  }
}

export function* handleDashboardTypeFetch({ payload }) {
  try {
    const dashboardType = yield call(adminAPI.getDashboardType, payload.id);
    yield put(setDashboardType(dashboardType));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(setDashboardType(null));
  }
}

export function* handleDataQueriesFetch() {
  try {
    const dataQueries = yield call(adminAPI.getDataQueries);
    yield put(setDataQueries(dataQueries));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(setDataQueries(null));
  }
}

export function* handleSubjectSelectionsFetch() {
  try {
    const subjectSelections = yield call(adminAPI.getSubjectSelections);
    yield put(setSubjectSelections(subjectSelections));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(setSubjectSelections(null));
  }
}

export function* handleSaveDashboardType({ payload }) {
  try {
    yield call(adminAPI.saveDashboardType, payload.dashboardType);
    yield put(setDashboardType(payload.dashboardType));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(setDashboardType(null));
  }
}

export function* handleDeleteDashboardType({ payload }) {
  try {
    yield call(adminAPI.deleteDashboardType, payload.id);
    yield put(deleteDashboardTypeFinish(true));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(deleteDashboardTypeFinish(false));
  }
}

export function* handleSubjectSelectionFetch({ payload }) {
  try {
    const subjectSelection = yield call(adminAPI.getSubjectSelection, payload.id);
    yield put(setSubjectSelection(subjectSelection));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(setSubjectSelection(null));
  }
}

export function* handleSaveSubjectSelection({ payload }) {
  try {
    yield call(adminAPI.saveSubjectSelection, payload.subjectSelection);
    yield put(saveSubjectSelectionFinish(true));
    yield put(setSubjectSelection(payload.subjectSelection));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(saveSubjectSelectionFinish(false));
    yield put(setSubjectSelection(null));
  }
}

export function* handleDeleteSubjectSelection({ payload }) {
  try {
    yield call(adminAPI.deleteSubjectSelection, payload.id);
    yield put(deleteSubjectSelectionFinish(true));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(deleteSubjectSelectionFinish(false));
  }
}

export function* handleGetDataQuery({ payload }) {
  try {
    const dataQuery = yield call(adminAPI.getDataQuery, payload.id);
    yield put(setDataQuery(dataQuery));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(setDataQuery(null));
  }
}

export function* handleGetExampleResolvedData({ payload }) {
  try {
    const exampleResolvedData = yield call(adminAPI.getExampleResolvedData, payload.dataQueryId);
    yield put(setExampleResolvedData(exampleResolvedData));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(setExampleResolvedData(null));
  }
}

export default function* adminSaga() {
  yield takeLatest(GET_DASHBOARD_TYPES, handleDashboardTypesFetch);
  yield takeLatest(GET_DASHBOARD_TYPE, handleDashboardTypeFetch);
  yield takeLatest(GET_DATA_QUERIES, handleDataQueriesFetch);
  yield takeLatest(GET_SUBJECT_SELECTIONS, handleSubjectSelectionsFetch);
  yield takeLatest(SAVE_DASHBOARD_TYPE, handleSaveDashboardType);
  yield takeLatest(DELETE_DASHBOARD_TYPE, handleDeleteDashboardType);
  yield takeLatest(GET_SUBJECT_SELECTION, handleSubjectSelectionFetch);
  yield takeLatest(SAVE_SUBJECT_SELECTION, handleSaveSubjectSelection);
  yield takeLatest(DELETE_SUBJECT_SELECTION, handleDeleteSubjectSelection);
  yield takeLatest(GET_DATA_QUERY, handleGetDataQuery);
  yield takeLatest(GET_EXAMPLE_RESOLVED_DATA, handleGetExampleResolvedData);
}
