import {
  GET_DASHBOARD_TYPES,
  GET_DASHBOARD_TYPE,
  GET_DATA_QUERIES,
  SET_DASHBOARD_TYPES,
  SET_DASHBOARD_TYPE,
  SET_DATA_QUERIES,
  GET_SUBJECT_SELECTIONS,
  SET_SUBJECT_SELECTIONS,
  SAVE_DASHBOARD_TYPE,
  DELETE_DASHBOARD_TYPE,
  DELETE_DASHBOARD_TYPE_FINISH,
  GET_SUBJECT_SELECTION,
  SET_SUBJECT_SELECTION,
  SAVE_SUBJECT_SELECTION,
  SAVE_SUBJECT_SELECTION_FINISH,
  DELETE_SUBJECT_SELECTION,
  DELETE_SUBJECT_SELECTION_FINISH,
  GET_DATA_QUERY,
  SET_DATA_QUERY,
  GET_EXAMPLE_RESOLVED_DATA,
  SET_EXAMPLE_RESOLVED_DATA,
} from './admin-types';

export const getDashboardTypes = () => ({
  type: GET_DASHBOARD_TYPES,
});
export const setDashboardTypes = (dashboardTypes) => ({
  type: SET_DASHBOARD_TYPES,
  payload: {
    dashboardTypes,
  },
});
export const getDashboardType = (id) => ({
  type: GET_DASHBOARD_TYPE,
  payload: {
    id,
  },
});
export const setDashboardType = (dashboardType) => ({
  type: SET_DASHBOARD_TYPE,
  payload: {
    dashboardType,
  },
});
export const getDataQueries = () => ({
  type: GET_DATA_QUERIES,
});
export const setDataQueries = (dataQueries) => ({
  type: SET_DATA_QUERIES,
  payload: {
    dataQueries,
  },
});
export const getSubjectSelections = () => ({
  type: GET_SUBJECT_SELECTIONS,
});
export const setSubjectSelections = (subjectSelections) => ({
  type: SET_SUBJECT_SELECTIONS,
  payload: {
    subjectSelections,
  },
});
export const saveDashboardType = (dashboardType) => ({
  type: SAVE_DASHBOARD_TYPE,
  payload: {
    dashboardType,
  },
});
export const deleteDashboardType = (id) => ({
  type: DELETE_DASHBOARD_TYPE,
  payload: {
    id,
  },
});
export const deleteDashboardTypeFinish = (isSuccess) => ({
  type: DELETE_DASHBOARD_TYPE_FINISH,
  payload: {
    isSuccess,
  },
});
export const getSubjectSelection = (id) => ({
  type: GET_SUBJECT_SELECTION,
  payload: {
    id,
  },
});
export const setSubjectSelection = (subjectSelection) => ({
  type: SET_SUBJECT_SELECTION,
  payload: {
    subjectSelection,
  },
});
export const saveSubjectSelection = (subjectSelection) => ({
  type: SAVE_SUBJECT_SELECTION,
  payload: {
    subjectSelection,
  },
});
export const saveSubjectSelectionFinish = (isSuccess) => ({
  type: SAVE_SUBJECT_SELECTION_FINISH,
  payload: {
    isSuccess,
  },
});
export const deleteSubjectSelection = (id) => ({
  type: DELETE_SUBJECT_SELECTION,
  payload: {
    id,
  },
});
export const deleteSubjectSelectionFinish = (isSuccess) => ({
  type: DELETE_SUBJECT_SELECTION_FINISH,
  payload: {
    isSuccess,
  },
});
export const getDataQuery = (id) => ({
  type: GET_DATA_QUERY,
  payload: {
    id,
  },
});
export const setDataQuery = (dataQuery) => ({
  type: SET_DATA_QUERY,
  payload: {
    dataQuery,
  },
});
export const getExampleResolvedData = (dataQueryId) => ({
  type: GET_EXAMPLE_RESOLVED_DATA,
  payload: {
    dataQueryId,
  },
});
export const setExampleResolvedData = (exampleResolvedData) => ({
  type: SET_EXAMPLE_RESOLVED_DATA,
  payload: {
    exampleResolvedData,
  },
});
