import {
  GET_DASHBOARD_TYPES,
  GET_DASHBOARD_TYPE,
  GET_DATA_QUERIES,
  GET_SUBJECT_SELECTIONS,
  SET_DASHBOARD_TYPES,
  SET_DASHBOARD_TYPE,
  SET_DATA_QUERIES,
  SET_SUBJECT_SELECTIONS,
  SAVE_DASHBOARD_TYPE,
  DELETE_DASHBOARD_TYPE,
  DELETE_DASHBOARD_TYPE_FINISH,
  GET_SUBJECT_SELECTION,
  SET_SUBJECT_SELECTION,
  SAVE_SUBJECT_SELECTION,
  SAVE_SUBJECT_SELECTION_FINISH,
  DELETE_SUBJECT_SELECTION,
  DELETE_SUBJECT_SELECTION_FINISH,
  GET_DATA_QUERY,
  SET_DATA_QUERY,
  GET_EXAMPLE_RESOLVED_DATA,
  SET_EXAMPLE_RESOLVED_DATA,
} from './admin-types';

export const initialState = {
  dashboardTypes: null,
  dashboardType: null,
  dashboardTypesLoading: false,
  dashboardTypeLoading: false,
  dataQueries: null,
  dataQueriesLoading: false,
  subjectSelections: null,
  subjectSelectionsLoading: false,
  saveDashboardTypeLoading: false,
  deleteDashboardTypeLoading: false,
  subjectSelection: null,
  subjectSelectionLoading: false,
  saveSubjectSelectionLoading: false,
  deleteSubjectSelectionLoading: false,
  dataQuery: null,
  dataQueryLoading: false,
  exampleResolvedData: null,
  exampleResolvedDataLoading: false,
};

const dashboardTypes = (state = initialState, action) => {
  switch (action.type) {
    case GET_DASHBOARD_TYPES:
      return Object.assign({}, state, {
        dashboardTypesLoading: true,
        dashboardTypes: null,
      });
    case SET_DASHBOARD_TYPES:
      return Object.assign({}, state, {
        dashboardTypes: action.payload.dashboardTypes,
        dashboardTypesLoading: false,
      });
    case GET_DASHBOARD_TYPE:
      return Object.assign({}, state, {
        dashboardTypeLoading: true,
        dashboardType: null,
      });
    case SET_DASHBOARD_TYPE:
      return Object.assign({}, state, {
        dashboardType: action.payload.dashboardType,
        dashboardTypeLoading: false,
        saveDashboardTypeLoading: false,
      });
    case GET_DATA_QUERIES:
      return Object.assign({}, state, {
        dataQueriesLoading: true,
        dataQueries: null,
      });
    case SET_DATA_QUERIES:
      return Object.assign({}, state, {
        dataQueries: action.payload.dataQueries,
        dataQueriesLoading: false,
      });
    case GET_SUBJECT_SELECTIONS:
      return Object.assign({}, state, {
        subjectSelections: null,
        subjectSelectionsLoading: true,
      });
    case SET_SUBJECT_SELECTIONS:
      return Object.assign({}, state, {
        subjectSelections: action.payload.subjectSelections,
        subjectSelectionsLoading: false,
      });
    case SAVE_DASHBOARD_TYPE:
      return Object.assign({}, state, {
        saveDashboardTypeLoading: true,
      });
    case DELETE_DASHBOARD_TYPE:
      return Object.assign({}, state, {
        deleteDashboardTypeLoading: true,
      });
    case DELETE_DASHBOARD_TYPE_FINISH:
      return Object.assign({}, state, {
        deleteDashboardTypeLoading: false,
      });
    case GET_SUBJECT_SELECTION:
      return Object.assign({}, state, {
        subjectSelection: null,
        subjectSelectionLoading: true,
      });
    case SET_SUBJECT_SELECTION:
      return Object.assign({}, state, {
        subjectSelection: action.payload.subjectSelection,
        subjectSelectionLoading: false,
      });
    case SAVE_SUBJECT_SELECTION:
      return Object.assign({}, state, {
        saveSubjectSelectionLoading: true,
      });
    case SAVE_SUBJECT_SELECTION_FINISH:
      return Object.assign({}, state, {
        saveSubjectSelectionLoading: false,
      });
    case DELETE_SUBJECT_SELECTION:
      return Object.assign({}, state, {
        deleteSubjectSelectionLoading: true,
      });
    case DELETE_SUBJECT_SELECTION_FINISH:
      return Object.assign({}, state, {
        deleteSubjectSelectionLoading: false,
      });
    case GET_DATA_QUERY:
      return Object.assign({}, state, {
        dataQuery: null,
        dataQueryLoading: true,
      });
    case SET_DATA_QUERY:
      return Object.assign({}, state, {
        dataQuery: action.payload.dataQuery,
        dataQueryLoading: false,
      });
    case GET_EXAMPLE_RESOLVED_DATA:
      return Object.assign({}, state, {
        exampleResolvedData: null,
        exampleResolvedDataLoading: true,
      });
    case SET_EXAMPLE_RESOLVED_DATA:
      return Object.assign({}, state, {
        exampleResolvedData: action.payload.exampleResolvedData,
        exampleResolvedDataLoading: false,
      });
    default:
      return state;
  }
};

export default dashboardTypes;
