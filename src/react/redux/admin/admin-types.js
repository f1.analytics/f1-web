export const GET_DASHBOARD_TYPES = 'ADMIN/GET_DASHBOARD_TYPES';
export const SET_DASHBOARD_TYPES = 'ADMIN/SET_DASHBOARD_TYPES';
export const GET_DASHBOARD_TYPE = 'ADMIN/GET_DASHBOARD_TYPE';
export const SET_DASHBOARD_TYPE = 'ADMIN/SET_DASHBOARD_TYPE';
export const GET_DATA_QUERIES = 'ADMIN/GET_DATA_QUERIES';
export const SET_DATA_QUERIES = 'ADMIN/SET_DATA_QUERIES';
export const GET_SUBJECT_SELECTIONS = 'ADMIN/GET_SUBJECT_SELECTIONS';
export const SET_SUBJECT_SELECTIONS = 'ADMIN/SET_SUBJECT_SELECTIONS';
export const SAVE_DASHBOARD_TYPE = 'ADMIN/SAVE_DASHBOARD_TYPE';
export const DELETE_DASHBOARD_TYPE = 'ADMIN/DELETE_DASHBOARD_TYPE';
export const DELETE_DASHBOARD_TYPE_FINISH = 'ADMIN/DELETE_DASHBOARD_TYPE_FINISH';
export const GET_SUBJECT_SELECTION = 'ADMIN/GET_SUBJECT_SELECTION';
export const SET_SUBJECT_SELECTION = 'ADMIN/SET_SUBJECT_SELECTION';
export const SAVE_SUBJECT_SELECTION = 'ADMIN/SAVE_SUBJECT_SELECTION';
export const SAVE_SUBJECT_SELECTION_FINISH = 'ADMIN/SAVE_SUBJECT_SELECTION_FINISH';
export const DELETE_SUBJECT_SELECTION = 'ADMIN/DELETE_SUBJECT_SELECTION';
export const DELETE_SUBJECT_SELECTION_FINISH = 'ADMIN/DELETE_SUBJECT_SELECTION_FINISH';
export const GET_DATA_QUERY = 'ADMIN/GET_DATA_QUERY';
export const SET_DATA_QUERY = 'ADMIN/SET_DATA_QUERY';
export const GET_EXAMPLE_RESOLVED_DATA = 'ADMIN/GET_EXAMPLE_RESOLVED_DATA';
export const SET_EXAMPLE_RESOLVED_DATA = 'ADMIN/SET_EXAMPLE_RESOLVED_DATA';

