import reducer, { initialState } from './ui-reducer';
import { screenSizes } from '../../constants';
import {
	toggleSidebar,
	closeSidebar,
	updateScreenSize,
	updateIsTop,
} from './ui-actions';

describe('UI reducer tests', () => {
	it('Should return initial state', () => {
		expect(reducer(undefined, {})).toEqual(initialState);
	});

	it('Should handle toggleSidebar', () => {
		let state = initialState;

		state = reducer(state, toggleSidebar());
		expect(state.isSidebarOpen).toEqual(true);
		state = reducer(state, toggleSidebar());
		expect(state.isSidebarOpen).toEqual(false);
	});

	it('Should handle closeSidebar', () => {
		const state = initialState;
		state.isSidebarOpen = true;

		expect(reducer(state, closeSidebar()).isSidebarOpen).toEqual(false);
	});

	it('Should handle updateScreenSize and keep sidebar open with mobile screenSize', () => {
		const screenSize = screenSizes.SMALL;
		let state = initialState;
		state.isSidebarOpen = true;

		state = reducer(state, updateScreenSize(screenSize));

		expect(state.screenSize).toEqual(screenSize);
		expect(state.isSidebarOpen).toBe(true);
	});

	it('Should handle updateScreenSize and close sidebar with desktop screenSize', () => {
		const screenSize = screenSizes.MEDIUM;
		let state = initialState;
		state.isSidebarOpen = true;

		state = reducer(state, updateScreenSize(screenSize));

		expect(state.screenSize).toEqual(screenSize);
		expect(state.isSidebarOpen).toBe(false);
	});

	it('Should handle updateIsTop', () => {
		const state = reducer({}, updateIsTop(false));

		expect(state.isTop).toBe(false);
	});
});
