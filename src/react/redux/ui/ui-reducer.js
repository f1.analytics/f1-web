import { isMobile } from '../../utility';
import {
	TOGGLE_SIDEBAR,
	CLOSE_SIDEBAR,
	UPDATE_SCREEN_SIZE,
	UPDATE_IS_TOP,
} from './ui-types';

export const initialState = {
	isSidebarOpen: false,
	screenSize: null,
	isTop: true,
};

const ui = (state = initialState, action) => {
	switch (action.type) {
		case TOGGLE_SIDEBAR:
			return Object.assign({}, state, {
				isSidebarOpen: !state.isSidebarOpen,
			});
		case CLOSE_SIDEBAR:
			return Object.assign({}, state, {
				isSidebarOpen: false,
			});
		case UPDATE_SCREEN_SIZE:
			return Object.assign({}, state, {
				screenSize: action.screenSize,
				isSidebarOpen: !isMobile(action.screenSize) ? false : state.isSidebarOpen,
			});
		case UPDATE_IS_TOP:
			return Object.assign({}, state, {
				isTop: action.isTop,
			});
		default:
			return state;
	}
};

export default ui;
