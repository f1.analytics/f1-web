import {
	TOGGLE_SIDEBAR,
	UPDATE_SCREEN_SIZE,
	CLOSE_SIDEBAR,
	UPDATE_IS_TOP,
} from './ui-types';

export const toggleSidebar = () => ({
	type: TOGGLE_SIDEBAR,
});

export const closeSidebar = () => ({
	type: CLOSE_SIDEBAR,
});

export const updateScreenSize = screenSize => ({
	type: UPDATE_SCREEN_SIZE,
	screenSize,
});

export const updateIsTop = isTop => ({
	type: UPDATE_IS_TOP,
	isTop,
});
