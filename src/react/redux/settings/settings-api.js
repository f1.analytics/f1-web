import axios from 'axios';

export const getSettings = async () => {
  const { data } = await axios.get('/api/settings').catch(error => {
    throw error;
  });
  return data;
};
