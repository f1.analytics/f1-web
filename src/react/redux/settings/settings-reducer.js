import { GET_SETTINGS, GET_SETTINGS_FAILURE, GET_SETTINGS_SUCCESS } from './settings-types';

export const initialState = {
  settings: {
    dashboardTypes: [],
  },
  settingsLoading: false,
};

const settings = (state = initialState, action) => {
  switch (action.type) {
    case GET_SETTINGS:
      return Object.assign({}, state, {
        settingsLoading: true,
      });
    case GET_SETTINGS_SUCCESS:
      return Object.assign({}, state, {
        settingsLoading: false,
        settings: action.payload.settings,
      });
    case GET_SETTINGS_FAILURE:
      return Object.assign({}, state, {
        settingsLoading: false,
        settings: initialState.settings,
      });
    default:
      return state;
  }
};

export default settings;
