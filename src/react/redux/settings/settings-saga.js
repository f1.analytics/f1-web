import { takeLatest, call, put } from 'redux-saga/effects';

import { GET_SETTINGS } from './settings-types';
import * as settingsAPI from './settings-api';
import { toastResponseError } from '../../utility';
import { getSettingsSuccess, getSettingsFailure } from './settings-actions';

export function* handleSettingsFetch() {
  try {
    const settings = yield call(settingsAPI.getSettings);
    yield put(getSettingsSuccess(settings));
  } catch (error) {
    yield call(toastResponseError, error);
    yield put(getSettingsFailure());
  }
}

export default function* settingsSaga() {
  yield takeLatest(GET_SETTINGS, handleSettingsFetch);
}
