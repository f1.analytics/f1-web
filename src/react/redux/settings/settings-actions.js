import {
  GET_SETTINGS,
  GET_SETTINGS_SUCCESS,
  GET_SETTINGS_FAILURE,
} from './settings-types';

export const getSettings = () => ({
  type: GET_SETTINGS,
});
export const getSettingsSuccess = (settings) => ({
  type: GET_SETTINGS_SUCCESS,
  payload: {
    settings,
  },
});
export const getSettingsFailure = () => ({
  type: GET_SETTINGS_FAILURE,
});

