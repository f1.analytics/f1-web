import {
	CLEAR_SESSION,
	SET_USER_DATA,
	SET_TOKENS,
	SET_ACCESS_TOKEN,
} from './session-types';

export const initialState = {
	userData: null,
	accessToken: null,
	refreshToken: null,
};

const session = (state = initialState, action) => {
	switch (action.type) {
		case SET_USER_DATA:
			return Object.assign({}, state, {
				userData: action.userData,
			});
		case SET_TOKENS:
			return Object.assign({}, state, {
				accessToken: action.accessToken,
				refreshToken: action.refreshToken,
			});
		case SET_ACCESS_TOKEN:
			return Object.assign({}, state, {
				accessToken: action.accessToken,
			});
		case CLEAR_SESSION:
			return initialState;
		default:
			return state;
	}
};

export default session;
