import {
	CLEAR_SESSION,
	SET_USER_DATA,
	SET_TOKENS,
	SET_ACCESS_TOKEN,
} from './session-types';

export const clearSession = () => ({
	type: CLEAR_SESSION,
});

export const setUserData = userData => ({
	type: SET_USER_DATA,
	userData,
});

export const setTokens = (accessToken, refreshToken) => ({
	type: SET_TOKENS,
	accessToken,
	refreshToken,
});

export const setAccessToken = accessToken => ({
	type: SET_ACCESS_TOKEN,
	accessToken,
});
