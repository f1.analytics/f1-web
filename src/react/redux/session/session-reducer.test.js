import reducer, { initialState } from './session-reducer';
import {
	clearSession,
	setUserData,
	setTokens,
	setAccessToken,
} from './session-actions';

describe('Session reducer tests', () => {
	it('Should return initial state', () => {
		expect(reducer(undefined, {})).toEqual(initialState);
	});

	it('Should handle clearSession', () => {
		expect(reducer(undefined, clearSession())).toEqual(initialState);
	});

	it('Should handle setUserData', () => {
		const userData = { username: 'user1' };

		expect(reducer(undefined, setUserData(userData)).userData).toEqual(userData);
	});

	it('Should handle setTokens', () => {
		const accessToken = 'accessToken';
		const refreshToken ='refreshToken';

		const state = reducer(undefined, setTokens(accessToken, refreshToken));

		expect(state.accessToken).toBe(accessToken);
		expect(state.refreshToken).toBe(refreshToken);
	});

	it('Should handle setAccessToken', () => {
		const accessToken = 'accessToken2';

		const state = reducer(undefined, setAccessToken(accessToken));

		expect(state.accessToken).toBe(accessToken);
	});
});
