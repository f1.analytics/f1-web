import {
    GET_DASHBOARDS,
    SET_DASHBOARDS,
    GET_DASHBOARD,
    SET_DASHBOARD,
    GET_SUBJECTS,
    SET_SUBJECTS,
    GET_FILTER_OPTIONS,
    SET_FILTER_OPTIONS,
    GET_SETTINGS_FILTER_OPTIONS,
    SET_SETTINGS_FILTER_OPTIONS,
    SET_SELECTED_SUBJECT,
    UPDATE_DASHBOARD,
    UPDATE_DASHBOARD_FINSIH,
    CREATE_DASHBOARD,
    CREATE_DASHBOARD_FINISH,
    DELETE_DASHBOARD,
    DELETE_DASHBOARD_FINISH,
    CREATE_CARD,
    CREATE_CARD_FINISH,
} from './dashboards-types';

export const initialState = {
    dashboards: null,
    dashboardsLoading: false,
    dashboard: null,
    dashboardLoading: false,
    subjects: [],
    subjectsLoading: false,
    filterOptions: [],
    filterOptionsLoading: false,
    settingsFilterOptions: null,
    settingsFilterOptionsLoading: false,
    subject: null,
    updateDashboardLoading: false,
    updateDashboardError: false,
    createDashboardLoading: false,
    createdDashboardId: null,
    deleteDashboardLoading: false,
    deleteDashboardError: false,
    creatingCard: false,
    createCardError: false,
};

const dashboards = (state = initialState, action) => {
    switch (action.type) {
        case GET_DASHBOARDS:
            return Object.assign({}, state, {
                dashboardsLoading: true,
                createdDashboardId: null,
            });
        case SET_DASHBOARDS:
            return Object.assign({}, state, {
                dashboards: action.dashboards,
                dashboardsLoading: false,
            });
        case GET_DASHBOARD:
            return Object.assign({}, state, {
                dashboardLoading: true,
                dashboard: null,
                subjects: null,
                filterOptions: null,
                subject: null,
                createdDashboardId: null,
            });
        case SET_DASHBOARD:
            return Object.assign({}, state, {
                dashboardLoading: false,
                dashboard: action.dashboard,
            });
        case GET_SUBJECTS:
            return Object.assign({}, state, {
                subjects: null,
                subjectsLoading: true,
            });
        case SET_SUBJECTS:
            return Object.assign({}, state, {
                subjectsLoading: false,
                subjects: action.payload.subjects,
                subject: null,
            });
        case GET_FILTER_OPTIONS:
            return Object.assign({}, state, {
                filterOptions: null,
                filterOptionsLoading: true,
            });
        case SET_FILTER_OPTIONS:
            return Object.assign({}, state, {
                filterOptions: action.filterOptions,
                filterOptionsLoading: false,
            });
        case GET_SETTINGS_FILTER_OPTIONS:
            return Object.assign({}, state, {
                settingsFilterOptions: null,
                settingsFilterOptionsLoading: true,
            });
        case SET_SETTINGS_FILTER_OPTIONS:
            return Object.assign({}, state, {
                settingsFilterOptions: action.filterOptions,
                settingsFilterOptionsLoading: false,
            });
        case SET_SELECTED_SUBJECT:
            return Object.assign({}, state, {
                subject: action.subject,
            });
        case UPDATE_DASHBOARD:
            return Object.assign({}, state, {
                updateDashboardLoading: true,
            });
        case UPDATE_DASHBOARD_FINSIH:
            return Object.assign({}, state, {
                updateDashboardLoading: false,
                updateDashboardError: action.dashboard === null,
                dashboard: action.dashboard ? action.dashboard : state.dashboard,
            });
        case CREATE_DASHBOARD:
            return Object.assign({}, state, {
                createDashboardLoading: true,
            });
        case CREATE_DASHBOARD_FINISH:
            return Object.assign({}, state, {
                createDashboardLoading: false,
                createdDashboardId: action.dashboardId,
            });
        case DELETE_DASHBOARD:
            return Object.assign({}, state, {
                deleteDashboardLoading: true,
            });
        case DELETE_DASHBOARD_FINISH:
            return Object.assign({}, state, {
                deleteDashboardLoading: false,
                deleteDashboardError: !action.isSuccess,
            });
        case CREATE_CARD:
            return Object.assign({}, state, {
                creatingCard: true,
                createCardError: false,
            });
        case CREATE_CARD_FINISH:
            return Object.assign({}, state, {
                creatingCard: false,
                createCardError: !action.isSuccess,
            });
        default:
            return state;
    }
};

export default dashboards;
