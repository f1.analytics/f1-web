import reducer, { initialState } from './dashboards-reducer';
import {
    getDashboards,
    setDashboards,
    getDashboard,
    setDashboard,
    getSubjects,
    setSubjects,
    getFilterOptions,
    setFilterOptions,
    setSelectedSubject,
    getSettingsFilterOptions,
    setSettingsFilterOptions,
    updateDashboard,
    updateDashboardFinish,
    createDashboard,
    createDashboardFinish,
    deleteDashboard,
    deleteDashboardFinish,
    createCard,
    createCardFinish,
} from './dashboards-actions';

describe('Dashboards reducer tests', () => {
    const dashboardId = 5;
    const createdDashboardId = 4;
    const dashboards = [{ type: 'driver' }];
    const subjects = [{ key: 2016, value: 2016, text: 2016 }];
    const filterOptions = [{ key: 2016, value: 2016, text: 2016 }];
    const subject = 4;

    it('Should return initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('Should handle get dashboards', () => {
        const state = reducer({ createdDashboardId }, getDashboards());

        expect(state.dashboardsLoading).toBe(true);
        expect(state.createdDashboardId).toBe(null);
    });

    it('Should handle set dashboards', () => {
        const state = reducer(undefined, setDashboards(dashboards));

        expect(state.dashboards).toEqual(dashboards);
        expect(state.dashboardsLoading).toBe(false);
    });

    it('Should handle get dashboard', () => {
        const state = reducer({ dashboard: dashboards[0], subjects: subjects, filterOptions, subject }, getDashboard(dashboardId));

        expect(state.dashboardLoading).toBe(true);
        expect(state.dashboard).toBe(null);
        expect(state.subjects).toBe(null);
        expect(state.filterOptions).toBe(null);
        expect(state.subject).toBe(null);
    });

    it('Should handle set dashboard', () => {
        const state = reducer(undefined, setDashboard(dashboards[0]));

        expect(state.dashboard).toEqual(dashboards[0]);
        expect(state.dashboardLoading).toBe(false);
    });

    it('Should handle get selection options', () => {
        const state = reducer({ subjects: subjects }, getSubjects());

        expect(state.subjects).toBe(null);
        expect(state.subjectsLoading).toBe(true);
    });

    it('Should handle set selection options', () => {
        const state = reducer({ subject }, setSubjects(subjects));

        expect(state.subjects).toEqual(subjects);
        expect(state.subjectsLoading).toBe(false);
        expect(state.subject).toBe(null);
    });

    it('Should handle get filter options', () => {
        const state = reducer({ filterOptions }, getFilterOptions());

        expect(state.filterOptions).toBe(null);
        expect(state.filterOptionsLoading).toBe(true);
    });

    it('Should handle set filter options', () => {
        const state = reducer(undefined, setFilterOptions(filterOptions));

        expect(state.filterOptions).toBe(filterOptions);
        expect(state.filterOptionsLoading).toBe(false);
    });

    it('Should handle set selected subject', () => {
        const state = reducer(undefined, setSelectedSubject(subject));

        expect(state.subject).toBe(subject);
    });

    it('Should handle getting settings filter options', () => {
        const state = reducer(undefined, getSettingsFilterOptions());

        expect(state.settingsFilterOptions).toBe(null);
        expect(state.settingsFilterOptionsLoading).toBe(true);
    });

    it('Should handle setting settings filter options', () => {
        const state = reducer(undefined, setSettingsFilterOptions(filterOptions));

        expect(state.settingsFilterOptions).toBe(filterOptions);
        expect(state.settingsFilterOptionsLoading).toBe(false);
    });


    it('Should handle update dashboard', () => {
        const state = reducer(undefined, updateDashboard());

        expect(state.updateDashboardLoading).toBe(true);
    });

    it('Should handle update dashboard finish error', () => {
        const state = reducer(undefined, updateDashboardFinish(dashboards[0]));

        expect(state.dashboard).toBe(dashboards[0]);
        expect(state.updateDashboardLoading).toBe(false);
        expect(state.updateDashboardError).toBe(false);
    });

    it('Should handle update dashboard finish error', () => {
        const state = reducer({ dashboard: dashboards[0] }, updateDashboardFinish(null));

        expect(state.dashboard).toBe(dashboards[0]);
        expect(state.updateDashboardLoading).toBe(false);
        expect(state.updateDashboardError).toBe(true);
    });

    it('Should handle create dashboard', () => {
        const state = reducer(undefined, createDashboard());

        expect(state.createDashboardLoading).toBe(true);
    });

    it('Should handle update dashboard', () => {
        const state = reducer(undefined, createDashboardFinish(createdDashboardId));

        expect(state.createDashboardLoading).toBe(false);
        expect(state.createdDashboardId).toBe(createdDashboardId);
    });

    it('Should handle delete dashboard', () => {
        const state = reducer(undefined, deleteDashboard());

        expect(state.deleteDashboardLoading).toBe(true);
    });

    it('Should handle delete dashboard finish success', () => {
        const state = reducer({ deleteDashboardLoading: true, deleteDashboardError: true }, deleteDashboardFinish(true));

        expect(state.deleteDashboardLoading).toBe(false);
        expect(state.deleteDashboardError).toBe(false);
    });

    it('Should handle delete dashboard finish failure', () => {
        const state = reducer({ deleteDashboardLoading: true, deleteDashboardError: true }, deleteDashboardFinish(false));

        expect(state.deleteDashboardLoading).toBe(false);
        expect(state.deleteDashboardError).toBe(true);
    });

    it('Should handle create card', () => {
        const state = reducer({ createCardError: true }, createCard());

        expect(state.creatingCard).toBe(true);
        expect(state.createCardError).toBe(false);
    });

    it('Should handle create card finish success', () => {
        const state = reducer({ createCardError: true }, createCardFinish(true));

        expect(state.creatingCard).toBe(false);
        expect(state.createCardError).toBe(false);
    });

    it('Should handle create card finish failure', () => {
        const state = reducer({ createCardError: false }, createCardFinish(false));

        expect(state.creatingCard).toBe(false);
        expect(state.createCardError).toBe(true);
    });
});
