import * as sagaEffects from 'redux-saga/effects';
import { dashboardTypes } from '../../constants';
import * as dashboardsAPI from './dashboards-api';
import { recordSaga } from '../../utility';
import { toastResponseError } from '../../utility/ui';
import {
    GET_DASHBOARDS,
    GET_DASHBOARD,
    GET_SUBJECTS,
    GET_FILTER_OPTIONS,
    GET_SETTINGS_FILTER_OPTIONS,
    UPDATE_DASHBOARD,
    CREATE_DASHBOARD,
    DELETE_DASHBOARD,
    CREATE_CARD,
} from './dashboards-types';
import dashboardsSaga, {
    handleDashboardListFetch,
    handleDashboardFetch,
    handleSubjectsFetch,
    handleFilterOptionsFetch,
    handleSettingsFilterOptionsFetch,
    handleUpdateDashboard,
    handleCreateDashboard,
    handleDeleteDashboard,
    handleCreateCard,
} from './dashboards-saga';
import {
    setDashboard,
    setSubjects,
    setFilterOptions,
    setSettingsFilterOptions,
    updateDashboardFinish,
    createDashboardFinish,
    deleteDashboardFinish,
} from './dashboards-actions';

jest.mock('../../utility/ui', () => ({
    toastResponseError: jest.fn(),
}));
jest.mock( './dashboards-api');

describe('Dashboards saga tests', () => {
    const dashboardId = 4;
    const dashboards = [{
        type: dashboardTypes.driver.value,
        title: 'Driver test',
        chartCount: 6,
        dataCards: [],
    }, {
        type: dashboardTypes.season.value,
        title: 'Season test',
        chartCount: 0,
        dataCards: [],
    }];

    beforeEach(() => {
        jest.resetAllMocks();
    });

    describe('Function dashboardsSaga tests', () => {
        const gen = dashboardsSaga();

        it('Should listen to redux actions', () => {
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(GET_DASHBOARDS, handleDashboardListFetch));
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(GET_DASHBOARD, handleDashboardFetch));
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(GET_SUBJECTS, handleSubjectsFetch));
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(GET_FILTER_OPTIONS, handleFilterOptionsFetch));
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(GET_SETTINGS_FILTER_OPTIONS, handleSettingsFilterOptionsFetch));
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(UPDATE_DASHBOARD, handleUpdateDashboard));
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(CREATE_DASHBOARD, handleCreateDashboard));
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(DELETE_DASHBOARD, handleDeleteDashboard));
            expect(gen.next().value).toEqual(sagaEffects.takeLatest(CREATE_CARD, handleCreateCard));
            expect(gen.next().done).toBe(true);
        });
    });

    describe('Function handleDashboardFetch tests', () => {
        it('Should get dashboard', async () => {
            dashboardsAPI.getDashboard.mockImplementation(jest.fn(() => dashboards[0]));

            const dispatched = await recordSaga(handleDashboardFetch, { dashboardId });

            expect(dashboardsAPI.getDashboard).toHaveBeenCalledTimes(1);
            expect(dashboardsAPI.getDashboard).toHaveBeenCalledWith(dashboardId);
            expect(dispatched).toContainEqual(setDashboard(dashboards[0]));
        });

        it('Should show error', async () => {
            const error = new Error('test');
            dashboardsAPI.getDashboard.mockImplementation(jest.fn(() => {
                throw error;
            }));

            const dispatched = await recordSaga(handleDashboardFetch, { dashboardId });

            expect(toastResponseError).toHaveBeenCalledTimes(1);
            expect(toastResponseError).toHaveBeenCalledWith(error);
            expect(dispatched).toContainEqual(setDashboard(null));
        });
    });

    describe('Function handleSelectionOptionsFetch tests', () => {
        const selectionOptions = [{ key: 2017, value: 2017, text: 2017 }];
        const subjectSelectionId = 1;
        const filterValue = 4;
        it('Should get selection options', async () => {
            dashboardsAPI.getSubjects.mockImplementation(jest.fn(() => selectionOptions));

            const dispatched = await recordSaga(handleSubjectsFetch, { payload: { subjectSelectionId, filterValue } });

            expect(dashboardsAPI.getSubjects).toHaveBeenCalledTimes(1);
            expect(dashboardsAPI.getSubjects).toHaveBeenCalledWith(subjectSelectionId, filterValue);
            expect(dispatched).toContainEqual(setSubjects(selectionOptions));
        });

        it('Should show error', async () => {
            const error = new Error('test');
            dashboardsAPI.getSubjects.mockImplementation(jest.fn(() => {
                throw error;
            }));

            const dispatched = await recordSaga(handleSubjectsFetch, { payload: { subjectSelectionId, filterValue } });

            expect(toastResponseError).toHaveBeenCalledTimes(1);
            expect(toastResponseError).toHaveBeenCalledWith(error);
            expect(dispatched).toContainEqual(setSubjects([]));
        });
    });

    describe('Function handleFilterOptionsFetch tests', () => {
        const filterOptions = [{ key: 2017, value: 2017, text: 2017 }];
        const subjectSelectionId = 2;
        it('Should get filter options', async () => {
            dashboardsAPI.getFilterOptions.mockImplementation(jest.fn(() => filterOptions));

            const dispatched = await recordSaga(handleFilterOptionsFetch, { subjectSelectionId });

            expect(dashboardsAPI.getFilterOptions).toHaveBeenCalledTimes(1);
            expect(dashboardsAPI.getFilterOptions).toHaveBeenCalledWith(subjectSelectionId);
            expect(dispatched).toContainEqual(setFilterOptions(filterOptions));
        });

        it('Should show error', async () => {
            const error = new Error('test');
            dashboardsAPI.getFilterOptions.mockImplementation(jest.fn(() => {
                throw error;
            }));

            const dispatched = await recordSaga(handleFilterOptionsFetch, {});

            expect(toastResponseError).toHaveBeenCalledTimes(1);
            expect(toastResponseError).toHaveBeenCalledWith(error);
            expect(dispatched).toContainEqual(setFilterOptions([]));
        });
    });

    describe('Function handleSettingsFilterOptionsFetch tests', () => {
        const filterOptions = [{ key: 2017, value: 2017, text: 2017 }];
        const subjectSelectionId = 3;

        it('Should get selection options', async () => {
            dashboardsAPI.getFilterOptions.mockImplementation(jest.fn(() => filterOptions));

            const dispatched = await recordSaga(handleSettingsFilterOptionsFetch, { subjectSelectionId });

            expect(dashboardsAPI.getFilterOptions).toHaveBeenCalledTimes(1);
            expect(dashboardsAPI.getFilterOptions).toHaveBeenCalledWith(subjectSelectionId);
            expect(dispatched).toContainEqual(setSettingsFilterOptions(filterOptions));
        });

        it('Should show error', async () => {
            const error = new Error('test');
            dashboardsAPI.getFilterOptions.mockImplementation(jest.fn(() => {
                throw error;
            }));

            const dispatched = await recordSaga(handleSettingsFilterOptionsFetch, {});

            expect(toastResponseError).toHaveBeenCalledTimes(1);
            expect(toastResponseError).toHaveBeenCalledWith(error);
            expect(dispatched).toContainEqual(setSettingsFilterOptions(null));
        });
    });

    describe('Function handleUpdateDashboard tests', () => {
        const dashboard = [{ id: 2, type: 'driver' }];

        it('Should update dashboard', async () => {
            dashboardsAPI.updateDashboard.mockImplementation(jest.fn(() => {}));
            dashboardsAPI.getDashboard.mockImplementation(jest.fn(() => dashboard));

            const dispatched = await recordSaga(handleUpdateDashboard, { dashboard });

            expect(dashboardsAPI.updateDashboard).toHaveBeenCalledTimes(1);
            expect(dashboardsAPI.updateDashboard).toHaveBeenCalledWith(dashboard);
            expect(dispatched).toContainEqual(updateDashboardFinish(dashboard));
        });

        it('Should show error', async () => {
            const error = new Error('test');
            dashboardsAPI.updateDashboard.mockImplementation(jest.fn(() => {
                throw error;
            }));

            const dispatched = await recordSaga(handleUpdateDashboard, {});

            expect(toastResponseError).toHaveBeenCalledTimes(1);
            expect(toastResponseError).toHaveBeenCalledWith(error);
            expect(dispatched).toContainEqual(updateDashboardFinish(null));
        });
    });

    describe('Function handleCreateDashboard tests', () => {
        const dashboard = [{ id: 2, type: 'driver' }];
        const dashboardId = 2;

        it('Should create dashboard', async () => {
            dashboardsAPI.createDashboard.mockImplementation(jest.fn(() => ({ id: dashboardId })));

            const dispatched = await recordSaga(handleCreateDashboard, { dashboard });

            expect(dashboardsAPI.createDashboard).toHaveBeenCalledTimes(1);
            expect(dashboardsAPI.createDashboard).toHaveBeenCalledWith(dashboard);
            expect(dispatched).toContainEqual(createDashboardFinish(dashboardId));
        });

        it('Should show error', async () => {
            const error = new Error('test');
            dashboardsAPI.createDashboard.mockImplementation(jest.fn(() => {
                throw error;
            }));

            const dispatched = await recordSaga(handleCreateDashboard, {});

            expect(toastResponseError).toHaveBeenCalledTimes(1);
            expect(toastResponseError).toHaveBeenCalledWith(error);
            expect(dispatched).toContainEqual(createDashboardFinish(null));
        });
    });

    describe('Function handleDeleteDashboard tests', () => {
        const dashboardId = 2;

        it('Should delete dashboard', async () => {
            dashboardsAPI.deleteDashboard.mockImplementation(jest.fn(() => {}));

            const dispatched = await recordSaga(handleDeleteDashboard, { dashboardId });

            expect(dashboardsAPI.deleteDashboard).toHaveBeenCalledTimes(1);
            expect(dashboardsAPI.deleteDashboard).toHaveBeenCalledWith(dashboardId);
            expect(dispatched).toContainEqual(deleteDashboardFinish(true));
        });

        it('Should show error', async () => {
            const error = new Error('test');
            dashboardsAPI.deleteDashboard.mockImplementation(jest.fn(() => {
                throw error;
            }));

            const dispatched = await recordSaga(handleDeleteDashboard, {});

            expect(toastResponseError).toHaveBeenCalledTimes(1);
            expect(toastResponseError).toHaveBeenCalledWith(error);
            expect(dispatched).toContainEqual(deleteDashboardFinish(false));
        });
    });
});
