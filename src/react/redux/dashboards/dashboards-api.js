import axios from 'axios';
import { jwtAxios } from '../../axios';

export const getDashboardsDefault = async () => {
    const { data } = await axios.get('/api/dashboards/default').catch(error => {
        throw error;
    });

    return data;
};

export const getDashboards = async () => {
    const { data } = await jwtAxios.get('/api/dashboards').catch(error => {
        throw error;
    });

    return data;
};

export const getDashboard = async dashboardId => {
    const { data } = await jwtAxios.get(`/api/dashboards/${dashboardId}`).catch(error => {
        throw error;
    });

    return data;
};

export const updateDashboard = async dashboard => {
    await jwtAxios.put('/api/dashboards/',
        dashboard
    ).catch(error => {
        throw error;
    });
};

export const createDashboard = async dashboard => {
    const { data } = await jwtAxios.post('/api/dashboards/',
        dashboard
    ).catch(error => {
        throw error;
    });

    return data;
};

export const deleteDashboard = async dashboardId => {
    await jwtAxios.delete(`/api/dashboards/${dashboardId}`).catch(error => {
        throw error;
    });
};

export const getSubjects = async (subjectSelectionId, filterValue) => {
    const { data } = await jwtAxios.get(
      `/api/dashboards/subjectSelections/${subjectSelectionId}/subjects${filterValue ? `?filterValue=${filterValue}` : ''}`
    ).catch(error => {
        throw error;
    });

    return data;
};

export const getFilterOptions = async subjectSelectionId => {
    const { data } = await jwtAxios.get(
      `/api/dashboards/subjectSelections/${subjectSelectionId}/filterOptions`
    ).catch(error => {
        throw error;
    });

    return data;
};

export const getCardData = async (cardId, query, cancelToken) => {
    const { data } = await jwtAxios.get(`/api/dataCards/data/${cardId}?${query}`, {
        cancelToken,
    }).catch(error => {
        throw error;
    });

    return data;
};

export const deleteCard = async cardId => {
    await jwtAxios.delete(`/api/dataCards/${cardId}`).catch(error => {
        throw error;
    });
};

export const createCard = async card => {
    const { data } = await jwtAxios.post('/api/dataCards/',
        card
    ).catch(error => {
        throw error;
    });

    return data;
};
