import {
    GET_DASHBOARDS,
    SET_DASHBOARDS,
    GET_DASHBOARD,
    SET_DASHBOARD,
    GET_SUBJECTS,
    SET_SUBJECTS,
    GET_FILTER_OPTIONS,
    SET_FILTER_OPTIONS,
    SET_SELECTED_SUBJECT,
    GET_SETTINGS_FILTER_OPTIONS,
    SET_SETTINGS_FILTER_OPTIONS,
    UPDATE_DASHBOARD,
    UPDATE_DASHBOARD_FINSIH,
    CREATE_DASHBOARD,
    CREATE_DASHBOARD_FINISH,
    DELETE_DASHBOARD,
    DELETE_DASHBOARD_FINISH,
    CREATE_CARD,
    CREATE_CARD_FINISH,
} from './dashboards-types';

export const getDashboards = () => ({
    type: GET_DASHBOARDS,
});

export const setDashboards = dashboards => ({
    type: SET_DASHBOARDS,
    dashboards,
});

export const getDashboard = dashboardId => ({
	type: GET_DASHBOARD,
	dashboardId,
});

export const setDashboard = dashboard => ({
	type: SET_DASHBOARD,
	dashboard,
});

export const getSubjects = (subjectSelectionId, filterValue) => ({
    type: GET_SUBJECTS,
    payload: {
        subjectSelectionId,
        filterValue,
    },
});

export const setSubjects = subjects => ({
    type: SET_SUBJECTS,
    payload: {
        subjects,
    },
});

export const getFilterOptions = subjectSelectionId => ({
    type: GET_FILTER_OPTIONS,
    subjectSelectionId,
});

export const setFilterOptions = filterOptions => ({
    type: SET_FILTER_OPTIONS,
    filterOptions,
});

export const getSettingsFilterOptions = subjectSelectionId => ({
    type: GET_SETTINGS_FILTER_OPTIONS,
    subjectSelectionId,
});

export const setSettingsFilterOptions = filterOptions => ({
    type: SET_SETTINGS_FILTER_OPTIONS,
    filterOptions,
});

export const setSelectedSubject = subject => ({
    type: SET_SELECTED_SUBJECT,
    subject,
});

export const updateDashboard = dashboard => ({
    type: UPDATE_DASHBOARD,
    dashboard,
});

export const updateDashboardFinish = dashboard => ({
    type: UPDATE_DASHBOARD_FINSIH,
    dashboard,
});

export const createDashboard = dashboard => ({
    type: CREATE_DASHBOARD,
    dashboard,
});

export const createDashboardFinish = dashboardId => ({
    type: CREATE_DASHBOARD_FINISH,
    dashboardId,
});

export const deleteDashboard = dashboardId => ({
    type: DELETE_DASHBOARD,
    dashboardId,
});

export const deleteDashboardFinish = isSuccess => ({
    type: DELETE_DASHBOARD_FINISH,
    isSuccess,
});

export const createCard = card => ({
    type: CREATE_CARD,
    card,
});

export const createCardFinish = isSuccess => ({
    type: CREATE_CARD_FINISH,
    isSuccess,
});
