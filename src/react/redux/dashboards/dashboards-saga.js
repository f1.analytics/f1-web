import { takeLatest, call, put, select } from 'redux-saga/effects';

import * as dashboardsAPI from './dashboards-api';
import { toastResponseError } from '../../utility';
import {
    setDashboards,
    setDashboard,
    setSubjects,
    setFilterOptions,
    setSettingsFilterOptions,
    updateDashboardFinish,
    createDashboardFinish,
    deleteDashboardFinish,
    createCardFinish,
} from './dashboards-actions';
import {
    GET_DASHBOARDS,
    GET_DASHBOARD,
    GET_SUBJECTS,
    GET_FILTER_OPTIONS,
    GET_SETTINGS_FILTER_OPTIONS,
    UPDATE_DASHBOARD,
    CREATE_DASHBOARD,
    DELETE_DASHBOARD,
    CREATE_CARD,
} from './dashboards-types';

export function* handleDashboardListFetch() {
    try {
        const dashboards = yield call(dashboardsAPI.getDashboards);
        yield put(setDashboards(dashboards));
    } catch (error) {
        yield call(toastResponseError, error);
        yield put(setDashboards(null));
    }
}

export function* handleDashboardFetch(payload) {
	try {
		const dashboard = yield call(dashboardsAPI.getDashboard, payload.dashboardId);
		yield put(setDashboard(dashboard));
	} catch (error) {
		yield call(toastResponseError, error);
		yield put(setDashboard(null));
	}
}

export function* handleSubjectsFetch({ payload }) {
   try {
        const { subjectSelectionId, filterValue } = payload;
        const subjects = yield call(dashboardsAPI.getSubjects, subjectSelectionId, filterValue);
        yield put(setSubjects(subjects));
    } catch (error) {
        yield call(toastResponseError, error);
        yield put(setSubjects([]));
    }
}

export function* handleFilterOptionsFetch(payload) {
    try {
        const { subjectSelectionId } = payload;
        const filterOptions = yield call(dashboardsAPI.getFilterOptions, subjectSelectionId);
        yield put(setFilterOptions(filterOptions));
    } catch (error) {
        yield call(toastResponseError, error);
        yield put(setFilterOptions([]));
    }
}

export function* handleSettingsFilterOptionsFetch(payload) {
    try {
        const filterOptions = yield call(dashboardsAPI.getFilterOptions, payload.subjectSelectionId);
        yield put(setSettingsFilterOptions(filterOptions));
    } catch (error) {
        yield call(toastResponseError, error);
        yield put(setSettingsFilterOptions(null));
    }
}

export function* handleUpdateDashboard(payload) {
    try {
        yield call(dashboardsAPI.updateDashboard, payload.dashboard);
        const dashboard = yield call(dashboardsAPI.getDashboard, payload.dashboard.id);
        yield put(updateDashboardFinish(dashboard));
    } catch (error) {
        yield call(toastResponseError, error);
        yield put(updateDashboardFinish(null));
    }
}


export function* handleCreateDashboard(payload) {
    try {
        const { id } = yield call(dashboardsAPI.createDashboard, payload.dashboard);
        yield put(createDashboardFinish(id));
    } catch (error) {
        yield call(toastResponseError, error);
        yield put(createDashboardFinish(null));
    }
}

export function* handleDeleteDashboard(payload) {
    try {
        yield call(dashboardsAPI.deleteDashboard, payload.dashboardId);
        yield put(deleteDashboardFinish(true));
    } catch (error) {
        yield call(toastResponseError, error);
        yield put(deleteDashboardFinish(false));
    }
}

export function* handleCreateCard(payload) {
    try {
        const { card } = payload;
        const { id } = yield call(dashboardsAPI.createCard, card);

        const dashboard = yield select(state => state.dashboards.dashboard);
        dashboard.dataCards.push({ ...card, id });

        yield put(setDashboard(dashboard));
        yield put(createCardFinish(true));
    } catch (error) {
        yield call(toastResponseError, error);
        yield put(createCardFinish(false));
    }
}

export default function* dashboardsSaga() {
    yield takeLatest(GET_DASHBOARDS, handleDashboardListFetch);
    yield takeLatest(GET_DASHBOARD, handleDashboardFetch);
    yield takeLatest(GET_SUBJECTS, handleSubjectsFetch);
    yield takeLatest(GET_FILTER_OPTIONS, handleFilterOptionsFetch);
    yield takeLatest(GET_SETTINGS_FILTER_OPTIONS, handleSettingsFilterOptionsFetch);
    yield takeLatest(UPDATE_DASHBOARD, handleUpdateDashboard);
    yield takeLatest(CREATE_DASHBOARD, handleCreateDashboard);
    yield takeLatest(DELETE_DASHBOARD, handleDeleteDashboard);
    yield takeLatest(CREATE_CARD, handleCreateCard);
}
