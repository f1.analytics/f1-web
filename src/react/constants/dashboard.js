export const filterTypes = {
    season: {
        value: 'season',
        text: 'Season',
        secondaryFilter: {
            placeholder: 'Select Season',
        },
    },
    driverConstructor: {
        value: 'driverConstructor',
        text: 'Constructor',
        secondaryFilter: {
            placeholder: 'Select constructor',
        },
    },
    driverNationality: {
        value: 'driverNationality',
        text: 'Driver nationality',
        secondaryFilter: {
            placeholder: 'Select nationality',
        },
    },
    driver: {
        value: 'driver',
        text: 'Driver',
        secondaryFilter: {
            placeholder: 'Select driver',
        },
    },
    constructorNationality: {
        value: 'constructorNationality',
        text: 'Constructor nationality',
        secondaryFilter: {
            placeholder: 'Select nationality',
        },
    },
    champions: {
        value: 'champions',
        text: 'Champions',
        selectionQuery: 'standing=1',
    },
};

export const cardDisplayTypes = {
    CHART: 'chart',
    INFO_BOX: 'infoBox',
};

export const cardDisplaySubTypes = {
    barChart: {
        value: 'barChart',
        className: 'bar-chart',
    },
    lineChart: {
        value: 'lineChart',
        className: 'line-chart',
    },
};

export const cardTypes = {
    driverInfoBox: {
        value: 'driverInfoBox',
    },
    driverStandings: {
        value: 'driverStandings',
    },
    driverResultsCount: {
        value: 'driverResultsCount',
    },
    driverResultsSum: {
        value: 'driverResultsSum',
    },
    driverResultsAverage: {
        value: 'driverResultsAverage',
    },
    driverQualifyingCount: {
        value: 'qualifyingWinsCount',
        note: '2003 onwards',
    },
    constructorInfoBox: {
        value: 'constructorInfoBox',
    },
    constructorStandings: {
        value: 'constructorStandings',
        note: '1958 onwards',
    },
    constructorResultsCount: {
        value: 'constructorResultsCount',
    },
    constructorResultsSum: {
        value: 'constructorResultsSum',
    },
    constructorQualifyingCount: {
        value: 'constructorQualifyingCount',
    },
    constructorResultsAverage: {
        value: 'constructorResultsAverage',
    },
};

export const dashboardTypes = {
    driver: {
        value: 'driver',
        icon: 'driver',
        text: 'Driver',
        selection: {
            placeholder: 'Select driver',
            filterTypes: [
                filterTypes.season,
                filterTypes.driverConstructor,
                filterTypes.driverNationality,
                filterTypes.champions,
            ],
        },
        defaultCards: [
            {
                value: 'championInfoBox',
                title: 'Success statistics',
                type: cardTypes.driverInfoBox.value,
                displayType: cardDisplayTypes.INFO_BOX,
                config: { infoBoxSections: [
                    { value: 'standingsCount', title: 'Championship wins', query: 'queryKey=position&queryValues=1' },
                    { value: 'resultsCount', title: 'Race wins', query: 'queryKey=position&queryValues=1' },
                    { value: 'resultsCount', title: 'Race podiums', query: 'queryKey=position&queryValues=1,2,3' },
                    { value: 'resultsCount', title: 'Pole positions', query: 'queryKey=grid&queryValues=1' },
                    { value: 'resultsAverage', title: 'Average race finish', query: 'queryKey=position' },
                ]},
            },
            {
                value: 'championshipStandings',
                title: 'Championship standings',
                type: cardTypes.driverStandings.value,
                displayType: cardDisplayTypes.CHART,
                config: { displaySubType: cardDisplaySubTypes.lineChart.value },
            },
            {
                value: 'raceWins',
                title: 'Race wins',
                type: cardTypes.driverResultsCount.value,
                displayType: cardDisplayTypes.CHART,
                config: { displaySubType: cardDisplaySubTypes.barChart.value, query: 'queryKey=position&queryValues=1' },
            },
            {
                value: 'racePodiums',
                title: 'Race podiums',
                type: cardTypes.driverResultsCount.value,
                displayType: cardDisplayTypes.CHART,
                config: { displaySubType: cardDisplaySubTypes.barChart.value, query: 'queryKey=position&queryValues=1,2,3'  },
            },
            {
                value: 'polePositions',
                title: 'Pole positions',
                type: cardTypes.driverResultsCount.value,
                displayType: cardDisplayTypes.CHART,
                config: { displaySubType: cardDisplaySubTypes.barChart.value, query: 'queryKey=grid&queryValues=1' },
            },
            {
                value: 'averageRaceResults',
                title: 'Average race results',
                type: cardTypes.driverResultsAverage.value,
                displayType: cardDisplayTypes.CHART,
                config: { displaySubType: cardDisplaySubTypes.lineChart.value, query: 'queryKey=position' },
            },
            {
                value: 'championshipPoints',
                title: 'Championship points',
                type: cardTypes.driverResultsSum.value,
                displayType: cardDisplayTypes.CHART,
                config: { displaySubType: cardDisplaySubTypes.lineChart.value, query: 'queryKey=points' },
            },
        ],
    },
    constructor: {
        value: 'constructor',
        icon: 'constructor',
        text: 'Constructor',
        selection: {
            placeholder: 'Select constructor',
            filterTypes: [
                filterTypes.season,
                filterTypes.driver,
                filterTypes.constructorNationality,
                filterTypes.champions,
            ],
        },
        defaultCards: [
            {
                value: 'championInfoBox',
                title: 'Success statistics',
                type: cardTypes.constructorInfoBox.value,
                displayType: cardDisplayTypes.INFO_BOX,
                config: { infoBoxSections: [
                    { value: 'standingsCount', title: 'Championship wins', query: 'queryKey=position&queryValues=1' },
                    { value: 'resultsCount', title: 'Race wins', query: 'queryKey=position&queryValues=1' },
                    { value: 'resultsCount', title: 'Race podiums', query: 'queryKey=position&queryValues=1,2,3' },
                    { value: 'resultsCount', title: 'Pole positions', query: 'queryKey=grid&queryValues=1' },
                    { value: 'resultsAverage', title: 'Average race finish', query: 'queryKey=position' },
                ]},
            },
            {
                value: 'championshipStandings',
                title: 'Championship standings',
                type: cardTypes.constructorStandings.value,
                displayType: cardDisplayTypes.CHART,
                config: { displayType: cardDisplayTypes.CHART, displaySubType: cardDisplaySubTypes.lineChart.value },
            },
            {
                value: 'raceWins',
                title: 'Race wins',
                type: cardTypes.constructorResultsCount.value,
                displayType: cardDisplayTypes.CHART,
                config: { displayType: cardDisplayTypes.CHART, displaySubType: cardDisplaySubTypes.barChart.value, query: 'queryKey=position&queryValues=1' },
            },
            {
                value: 'racePodiums',
                title: 'Race podiums',
                type: cardTypes.constructorResultsCount.value,
                displayType: cardDisplayTypes.CHART,
                config: {displayType: cardDisplayTypes.CHART, displaySubType: cardDisplaySubTypes.barChart.value, query: 'queryKey=position&queryValues=1,2,3'  },
            },
            {
                value: 'polePositions',
                title: 'Pole positions',
                type: cardTypes.constructorResultsCount.value,
                displayType: cardDisplayTypes.CHART,
                config: { displayType: cardDisplayTypes.CHART, displaySubType: cardDisplaySubTypes.barChart.value, query: 'queryKey=grid&queryValues=1' },
            },
            {
                value: 'averageRaceResults',
                title: 'Average race results',
                type: cardTypes.constructorResultsAverage.value,
                displayType: cardDisplayTypes.CHART,
                config: { displayType: cardDisplayTypes.CHART, displaySubType: cardDisplaySubTypes.lineChart.value, query: 'queryKey=position' },
            },
            {
                value: 'championshipPoints',
                title: 'Championship points',
                type: cardTypes.constructorResultsSum.value,
                displayType: cardDisplayTypes.CHART,
                config: { displayType: cardDisplayTypes.CHART, displaySubType: cardDisplaySubTypes.lineChart.value, query: 'queryKey=points' },
            },
        ],
    },
    season: {
        value: 'season',
        icon: 'season',
        text: 'Season',
        selection: {
            placeholder: 'Select season',
        },
    },
    circuit: {
        value: 'circuit',
        icon: 'circuit',
        text: 'Circuit',
        selection: {
            placeholder: 'Select circuit',
        },
    },
};

export const dataFormatTypes = {
    YEAR: 'year',
};
