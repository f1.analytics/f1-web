import sassVariables from '../sass/variablesExport.scss';
import { responseCodes } from './api';

export const menuOptions = {
	ADMIN: {
		value: 'admin',
		path: '/admin',
		text: 'Admin',
		icon: 'wrench',
		component: 'Admin',
	},
	DASHBOARD: {
		value: 'dashboard',
		path: '/dashboard',
		text: 'Dashboard',
		icon: 'pie graph',
		component: 'DashboardList',
	},
	LOGIN: {
		value: 'login',
		path: '/login',
		text: 'Login',
		icon: 'user',
		component: 'Login',
	},
	SIGN_UP: {
		value: 'signup',
		path: '/signup',
		text: 'Sign up',
		icon: 'plus',
		component: 'Register',
	},
	LOGOUT: {
		value: 'logout',
		path: '/',
		text: 'Logout',
		icon: 'sign-out',
	},
};

export const screenSizes = {
	SMALL: {
		index: 0,
		widthLimit: parseInt(sassVariables.mobileMaxWidth) + 1,
	},
	MEDIUM: {
		index: 1,
		widthLimit: 10000,
	},
};

export const errors = {
	INVALID_EMAIL: {
		value: 'invalidEmail',
		text: 'Invalid email.',
		type: 'warning',
	},
	EMAIL_MISMATCH: {
		value: 'emailMismatch',
		text: 'Emails don\'t match.',
		type: 'warning',
	},
	SHORT_PASSWORD: {
		value: 'shortPassword',
		text: 'Password too short.',
		type: 'warning',
	},
	PASSWORD_MISMATCH: {
		value: 'passwordMismatch',
		text: 'Passwords don\'t match.',
		type: 'warning',
	},
	EMAIL_TAKEN: {
		value: responseCodes.EMAIL_TAKEN,
		text: 'This email is already taken.',
		type: 'warning',
	},
	USERNAME_TAKEN: {
		value: responseCodes.USERNAME_TAKEN,
		text: 'This username is already taken.',
		type: 'warning',
	},
	WRONG_CREDENTIALS: {
		value: responseCodes.WRONG_CREDENTIALS,
		text: 'Wrong email or password.',
		type: 'warning',
	},
	INTERNAL_SERVER_ERROR: {
		value: 'internalError',
		text: 'Ooops, something went wrong!',
		type: 'error',
	},
};

export const text = {
	resultCount: 'Results count',
	qualifyingCount: 'Qualifying count',
	position: 'Position',
	year: 'Season',
	resultAverage: 'Average',
	resultSum: 'Sum',
};
