export const responseCodes = {
	WRONG_CREDENTIALS: 'wrongCredentials',
	USERNAME_TAKEN: 'usernameTaken',
	EMAIL_TAKEN: 'emailTaken',
};
