import PropTypes from 'prop-types';

export const numberOrStringPropType = PropTypes.oneOfType([
  PropTypes.number,
  PropTypes.string,
]);

export const variablePropType = PropTypes.shape({
  exportAs: PropTypes.string.isRequired,
  exportFrom: PropTypes.string.isRequired,
  exportKey: PropTypes.string.isRequired,
});

export const dataQueryPropType = PropTypes.shape({
  id: numberOrStringPropType.isRequired,
  variables: PropTypes.arrayOf(variablePropType),
});

export const selectConfigPropType = PropTypes.shape({
  placeholder: PropTypes.string.isRequired,
  dataQuery: dataQueryPropType.isRequired,
  variables: PropTypes.arrayOf(variablePropType),
});

export const subjectSelectionConfigPropType = PropTypes.shape({
  selectionTypePlaceholder: PropTypes.string.isRequired,
  subjectSelect: selectConfigPropType.isRequired,
  filterSelect: selectConfigPropType,
});

export const leanDashboardTypePropType = PropTypes.shape({
  id: numberOrStringPropType.isRequired,
  name: PropTypes.string.isRequired,
});

export const richSubjectSelectionPropType = PropTypes.shape({
  config: subjectSelectionConfigPropType.isRequired,
  dashboardType: leanDashboardTypePropType,
  id: numberOrStringPropType.isRequired,
  name: PropTypes.string.isRequired,
});

export const leanSubjectSelectionPropType = PropTypes.shape({
  id: numberOrStringPropType.isRequired,
  name: PropTypes.string.isRequired,
});

export const leanDataCardTypePropType = PropTypes.shape({
  id: numberOrStringPropType.isRequired,
  name: PropTypes.string.isRequired,
  config: PropTypes.shape({
    displayType: PropTypes.string.isRequired,
    displaySubType: PropTypes.string,
  }).isRequired,
});

export const richDashboardTypePropType = PropTypes.shape({
  id: numberOrStringPropType.isRequired,
  name: PropTypes.string.isRequired,
  subjectSelections: PropTypes.arrayOf(leanSubjectSelectionPropType).isRequired,
  dataCardTypes: PropTypes.arrayOf(leanDataCardTypePropType).isRequired,
});

export const dataQueryOptionVariablePropType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  availableValues: PropTypes.arrayOf(PropTypes.oneOf([PropTypes.string, PropTypes.number])),
  exampleValue: PropTypes.oneOf([PropTypes.string, PropTypes.number]),
});

export const leanDataQueryOptionPropType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  script: PropTypes.string.isRequired,
});

export const richDataQueryOptionPropType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  script: PropTypes.string.isRequired,
  variables: PropTypes.arrayOf(dataQueryOptionVariablePropType).isRequired,
});

export const leanDashboardPropType = PropTypes.shape({
  name: PropTypes.string.isRequired,
  dataCardCount: PropTypes.number.isRequired,
  dashboardTypeId: numberOrStringPropType.isRequired,
  id: numberOrStringPropType.isRequired,
});

export const richDashboardPropType = PropTypes.shape({
  dataCards: PropTypes.array.isRequired,
  dashboardType: richDashboardTypePropType.isRequired,
  config: PropTypes.shape({
    defaultSubjectSelection: PropTypes.shape({
      subjectSelectionId: PropTypes.number.isRequired,
      filterValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
    }),
  }).isRequired,
});
