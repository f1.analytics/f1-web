import LineChart from './index';
import { selectAll } from 'd3';
import { dataFormatTypes } from '../../../constants';

describe('D3 line chart tests', () => {
    const chartData = { xFormat: dataFormatTypes.YEAR, xText: 'Season', xKey: 'year', yKey: 'position', data: [{ x: 2013, y: 2 }, { x: 2014, y: 4}] };
    const containerId = 'chart-4';
    const margin = {left: 40, right: 20, top: 20, bottom: 20};
    const chart = new LineChart(containerId, margin);

    selectAll('body')
        .append('div')
        .attr('id', containerId)
        .append('div')
        .attr('class', 'tooltip')
        .append('div')
        .attr('class', 'tooltip-text');

    it('Should make data display', () => {
        jest.spyOn(LineChart.prototype, '_animateDataDisplay').mockImplementation(jest.fn());

        chart.initChart();
        chart.drawChart();
        chart.updateData(chartData);

        expect(selectAll(`#${containerId} svg g .data-circle`).size()).toBe(chartData.data.length);
        expect(selectAll(`#${containerId} svg g .data-line`).size()).toBe(1);
    });
});
