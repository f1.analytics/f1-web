import Chart from '../index';
import { line, max } from 'd3';

class LineChart extends Chart {
    constructor(containerId, margin) {
        super(containerId, margin);

        this.dataLineClass = 'data-line';
        this.dataCircleClass = 'data-circle';
    }

    _setDomains() {
        this.xScale.domain(this.chartData.data.map(this.getXValue));

        const yMax = max(this.chartData.data, this.getYMaxValue);
        this.yScale.domain([0, yMax + 2 + Math.floor(yMax / 10)]);
    }

    _createDataDisplay() {
        this.chart.selectAll('.line-and-dots').remove();

        const lineAndDots = this.chart.append('g')
            .attr('class', 'line-and-dots');

        lineAndDots.append('path')
            .attr('class', this.dataLineClass)
            .datum(this.chartData.data)
            .attr('d', line()
                .x(d => this.xScale(this.getXValue(d)) + this.xScale.bandwidth() / 2)
                .y(d => this.yScale(this.getYValue(d))));

        lineAndDots.selectAll(`.${this.dataCircleClass}`)
            .data(this.chartData.data)
            .enter().append('circle')
            .attr('class', (d, i)  => `${this.dataCircleClass} ${this.dataCircleClass}-${i}`)
            .attr('r', 3.5)
            .attr('cx', d => this.xScale(this.getXValue(d)) + this.xScale.bandwidth() / 2)
            .attr('cy', d => this.yScale(this.getYValue(d)));
    }

    _animateDataDisplay() {
        const path = this.chart.selectAll(`.${this.dataLineClass}`);

        if (path.attr('d')) {
            const totalLength = path.node().getTotalLength();

            path.attr('stroke-dasharray', totalLength + ' ' + totalLength)
                .attr('stroke-dashoffset', totalLength)
                .transition()
                .duration(500)
                .attr('stroke-dashoffset', 0);
        }
    }

    _highlightHoveredData(hoverIndex) {
        this.chart.selectAll(`.${this.dataCircleClass}-${hoverIndex}`)
            .style('fill', '#4ff5ff');
    }

    _removeDataHighlight() {
        this.chart.selectAll(`.${this.dataCircleClass}`)
            .style('fill', 'steelblue');
    }
}

export default LineChart;
