import { selectAll, scaleLinear, scaleBand, axisBottom, axisLeft, mouse } from 'd3';
import { formatData } from '../../utility';

class Chart {
    constructor(containerId, margin) {
        this.containerId = containerId;
        this.margin = margin;
        this.chartData = null;
        this.innerWidth = null;
        this.innerHeight = null;

        this._handleHover = this._handleHover.bind(this);
        this._clearHover = this._clearHover.bind(this);
        this.getYMaxValue = this.getYMaxValue.bind(this);
    }

    initChart() {
        this._appendChart();
        this._setInnerWidth();
        this._setInnerHeight();
        this._setScales();
    }

    clearChart() {
        this.chart.selectAll('*').remove();
        this._hideTooltip();
    }

    drawChart() {
        this.clearChart();
        this._setAxis();
        this._createHoverOverlay();

        this.updateData();
    }

    resize() {
        const prevWidth = this.innerWidth;
        this._setInnerWidth();

        if (prevWidth !== this.innerWidth) {
            this._setScales();
            this.drawChart();
        }
    }

    updateData(chartData) {
        if (chartData) {
            this.chartData = chartData;
        }

        if (this.chartData) {
            this._updateTicks();
            this._setDomains();
            this._animateAxis();

            this._setNewHoverBars();
            this._createDataDisplay();
            this._animateDataDisplay();
        }
    }

    getXValue(d) {
        return d.x;
    }

    getYValue(d) {
        return d.y;
    }

    getYMaxValue(d) {
        return d.yMax || d.y;
    }

    _appendChart() {
        if (!this.svg) {
            this.container = selectAll(`#${this.containerId}`);
            this.svg = this.container.append('svg');
            this.chart = this.svg.append('g')
                .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);
        }
    }

    _setScales() {
        this.xScale = scaleBand()
            .range([0, this.innerWidth])
            .padding(0.1);

        this.yScale = scaleLinear()
            .range([this.innerHeight, 0]);
    }

    _setAxis() {
        this.xAxis = axisBottom(this.xScale)
            .tickSize(0)
            .tickPadding(10);

        this.yAxis = axisLeft(this.yScale)
            .tickSize(-this.innerWidth)
            .tickSizeOuter(0)
            .tickPadding(10);

        this.chart.append('g')
            .attr('transform', 'translate(0,' + this.innerHeight + ')')
            .attr('class', 'x-axis');

        this.chart.append('g')
            .attr('class', 'y-axis');
    }

    _animateAxis() {
        this.chart.selectAll('.x-axis').transition()
            .duration(500)
            .call(this.xAxis);

        this.chart.selectAll('.y-axis')
            .transition()
            .duration(500)
            .call(this.yAxis);
    }

    _updateTicks() {
        const dataLength = this.chartData.data.length;
        this.xAxis
            .tickValues(this.chartData.data.filter((d, i) =>
                i % Math.ceil(dataLength / 12) === 0
            ).map(this.getXValue))
            .tickFormat(value => formatData(value, this.chartData.xFormat));
    }

    _createHoverOverlay() {
        this.svg.append('rect')
            .attr('class', 'overlay')
            .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`)
            .attr('width', this.innerWidth)
            .attr('height', this.innerHeight)
            .on('mouseover', this._handleHover)
            .on('mouseout', this._clearHover)
            .on('mousemove', this._handleHover);
    }

    _setNewHoverBars() {
        this.chart.selectAll('.hover-bar').remove();

        this.chart.selectAll('.hover-bar')
            .data(this.chartData.data)
            .enter().append('rect')
            .attr('class', (d, i) => `hover-bar hover-bar-${i}`)
            .attr('x', d => this.xScale(this.getXValue(d)))
            .attr('width', this.xScale.bandwidth())
            .attr('y', 0)
            .attr('height', this.innerHeight);
    }

    _handleHover() {
        const hoverIndex = this._getHoverXIndex();

        if (this.xScale.domain()[hoverIndex]) {
            this._clearHover();
            this._showHoverBar(hoverIndex);
            this._moveTooltip();

            this._highlightHoveredData(hoverIndex);
            this._makeTooltipText(hoverIndex);
        }
    }

    _clearHover() {
        this.svg.selectAll('.hover-bar')
            .style('opacity', 0);

        this._hideTooltip();
        this._removeDataHighlight();
    }

    _showHoverBar(hoverIndex) {
        this.svg.selectAll(`.hover-bar-${hoverIndex}`)
            .style('opacity', 0.2);
    }

    _moveTooltip() {
        this.container.selectAll('.tooltip')
            .style('display', 'block')
            .style('top', (mouse(this.svg.node())[1]) + 'px')
            .style('left', (mouse(this.svg.node())[0]) + 'px');
    }

    _hideTooltip() {
        this.container.selectAll('.tooltip').style('display', 'none');
    }

    _makeTooltipText(hoverIndex) {
        const data = this.chartData.data[hoverIndex];
        const xValue = this.getXValue(data);
        const yValue = this.getYValue(data);

        this.container.selectAll('.tooltip-text p')
            .remove();
        this.container.selectAll('.tooltip-text')
            .append('p')
            .text(`${this.chartData.xText}: ${xValue}`)
            .append('p')
            .text(`${this.chartData.yText}: ${yValue}`);
    }

    _getHoverXIndex() {
        const x = mouse(this.svg.node())[0] - this.xScale.bandwidth() / 2 -this. margin.left;
        const bandStep = this.xScale.step();
        return Math.round((x / bandStep));
    }

    _setInnerWidth() {
        const width = +this.svg.node().getBoundingClientRect().width;
        this.innerWidth = width - this.margin.left - this.margin.right;
    }

    _setInnerHeight() {
        const height = +this.svg.node().getBoundingClientRect().height;
        this.innerHeight = height - this.margin.left - this.margin.right;
    }

    _setDomains() {}

    _createDataDisplay() {}

    _animateDataDisplay() {}

    _highlightHoveredData() {}

    _removeDataHighlight() {}
}

export default Chart;
