import Chart from './index';
import { selectAll } from 'd3';
import { dataFormatTypes } from '../../constants';

describe('D3 chart tests', () => {
    const chartData = {
        xFormat: dataFormatTypes.YEAR,
        data: [{ x: 2013, y: 2, yMax: 2 }, { x: 2014, y: 4, yMax: 4 }],
        xText: 'Season',
    };
    const containerId = 'chart-4';
    const margin = { left: 40, right: 20, top: 20, bottom: 20 };
    const chart = new Chart(containerId, margin);

    selectAll('body')
        .append('div')
        .attr('id', containerId)
        .append('div')
        .attr('class', 'tooltip')
        .append('div')
        .attr('class', 'tooltip-text');

    it('Should initialize chart', () => {
        chart.initChart();

        expect(selectAll(`#${containerId} svg g`).size()).toBe(1);
    });

    it('Should initialize chart', () => {
        chart.initChart();

        expect(selectAll(`#${containerId} svg g`).size()).toBe(1);
    });

    it('Should draw chart', () => {
        chart.drawChart();

        expect(selectAll(`#${containerId} svg g .y-axis`).size()).toBe(1);
        expect(selectAll(`#${containerId} svg g .x-axis`).size()).toBe(1);
        expect(selectAll(`#${containerId} svg .overlay`).size()).toBe(1);
    });

    it('Updates chart data', () => {
        chart.updateData(chartData);

        expect(selectAll(`#${containerId} svg g .hover-bar`).size()).toBe(chartData.data.length);
        expect(selectAll(`#${containerId} svg g .x-axis .tick`).size()).toBe(chartData.data.length);
        expect(selectAll(`#${containerId} svg g .y-axis .tick`).size() > 0).toBe(true);
    });

    it('Should make tooltip text', () => {
        chart._makeTooltipText(0);

        expect(selectAll(`#${containerId} .tooltip-text`).text()).toContain(`Season: ${chartData.data[0].x}`);
    });

    it('Clears chart', () => {
        chart.clearChart();

        expect(selectAll(`#${containerId} svg g .hover-bar`).size()).toBe(0);
        expect(selectAll(`#${containerId} svg g .x-axis .tick`).size()).toBe(0);
        expect(selectAll(`#${containerId} svg g .y-axis .tick`).size()).toBe(0);
    });
});
