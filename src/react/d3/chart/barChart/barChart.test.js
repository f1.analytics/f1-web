import BarChart from './index';
import { selectAll } from 'd3';
import { dataFormatTypes } from '../../../constants';

describe('D3 bar chart tests', () => {
    const chartData = { xFormat: dataFormatTypes.YEAR, xText: 'Season', xKey: 'year', yKey: 'position', data: [{ x: 2013, y: 2 }, { x: 2014, y: 4}] };
    const containerId = 'chart-4';
    const margin = {left: 40, right: 20, top: 20, bottom: 20};
    const chart = new BarChart(containerId, margin);

    selectAll('body')
        .append('div')
        .attr('id', containerId)
        .append('div')
        .attr('class', 'tooltip')
        .append('div')
        .attr('class', 'tooltip-text');

    it('Should make data display', () => {

        chart.initChart();
        chart.drawChart();
        chart.updateData(chartData);

        expect(selectAll(`#${containerId} svg g .data-bar`).size()).toBe(chartData.data.length);
    });
});
