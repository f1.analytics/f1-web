import Chart from '../index';
import { max } from 'd3';

class BarChart extends Chart {
    constructor(containerId, margin) {
        super(containerId, margin);

        this.dataBarClass = 'data-bar';
    }

    _setDomains() {
        this.xScale.domain(this.chartData.data.map(this.getXValue));

        const yMax =  max(this.chartData.data, this.getYMaxValue);
        this.yScale.domain([0, yMax ? yMax : 1]);
    }

    _createDataDisplay() {
        this.chart.selectAll(`.${this.dataBarClass}`).remove();

        this.chart.selectAll(`.${this.dataBarClass}`)
            .data(this.chartData.data)
            .enter().append('rect')
            .attr('class', (d, i) => `${this.dataBarClass} ${this.dataBarClass}-${i}`)
            .attr('x', d => this.xScale(this.getXValue(d)))
            .attr('y', () => this.innerHeight)
            .attr('width', this.xScale.bandwidth())
            .attr('height', 0);
    }

    _animateDataDisplay() {
        const fullDuration = 500;
        const barDuration = fullDuration / this.chartData.data.length;

        this.chart.selectAll(`.${this.dataBarClass}`)
            .transition()
            .duration(barDuration)
            .attr('y', d => this.yScale(this.getYValue(d)))
            .attr('height', d => this.innerHeight - this.yScale(this.getYValue(d)))
            .delay((d, i) => i*barDuration);
    }

    _highlightHoveredData(hoverIndex) {
        this.chart.selectAll(`.${this.dataBarClass}-${hoverIndex}`)
            .style('fill', '#45d1db');
    }

    _removeDataHighlight() {
        this.chart.selectAll(`.${this.dataBarClass}`)
            .style('fill', 'steelblue');
    }
}

export default BarChart;
