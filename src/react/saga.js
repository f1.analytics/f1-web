import { all } from 'redux-saga/effects';

import userSaga from './redux/user/user-saga';
import dashboardsSaga from './redux/dashboards/dashboards-saga';
import adminSaga from './redux/admin/admin-saga';
import settingsSaga from './redux/settings/settings-saga';

export default function* rootSaga() {
	yield all([
		adminSaga(),
		dashboardsSaga(),
		userSaga(),
		settingsSaga(),
	]);
}
