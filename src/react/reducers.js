import { combineReducers } from 'redux';
import admin from './redux/admin/admin-reducer';
import ui from './redux/ui/ui-reducer';
import session from './redux/session/session-reducer';
import user from './redux/user/user-reducer';
import dashboards from './redux/dashboards/dashboards-reducer';
import settings from './redux/settings/settings-reducer';

export default combineReducers({
	admin,
	ui,
	session,
	user,
	dashboards,
	settings,
});
