import { dataFormatTypes, filterTypes } from '../constants';

export const formatData = (value, xFormat) => {
    switch (xFormat) {
        case dataFormatTypes.YEAR:
            return `'${value.toString().substring(2)}`;
        default:
            return value;
    }
};

export const makeSubjectFetchQuery = (filterType, filterValue) => {
    if (filterTypes[filterType].secondaryFilter) {
        if (filterValue !== 'all') {
            return `${filterType}=${filterValue}`;
        }
        return null;
    }

    return filterTypes[filterType].selectionQuery;
};
