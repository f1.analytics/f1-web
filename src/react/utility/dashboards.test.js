import { formatData, makeSubjectFetchQuery } from './dashboards';
import { dataFormatTypes, filterTypes } from '../constants';

describe('Dashboards utility tests', () => {
    describe('formatData function', () => {
        it('Should format years', () => {
            expect(formatData(2016, dataFormatTypes.YEAR)).toBe('\'16');
        });

        it('Should return unformatted value', () => {
            expect(formatData(2016, 'unrecognized')).toBe(2016);
        });
    });

    describe('makeSubjectFetchQuery function', () => {
        it('Should return selection query', () => {
            expect(makeSubjectFetchQuery(filterTypes.champions.value)).toBe(filterTypes.champions.selectionQuery);
        });

        it('Should return formated query with filterValue', () => {
            expect(makeSubjectFetchQuery(filterTypes.season.value, 2017)).toBe('season=2017');
        });

        it('Should return null if filterValue is "all"', () => {
            expect(makeSubjectFetchQuery(filterTypes.season.value, 'all')).toBe(null);
        });
    });
});
