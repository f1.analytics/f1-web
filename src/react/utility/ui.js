import * as Sentry from '@sentry/browser';
import { toast } from 'react-toastify';
import { errors } from '../constants';

export const isMobile = screenSize => screenSize.index < 1;

export const toastError = error => {
	if (error.type === 'warning') {
		toast.warning(error.text);
	} else {
		toast.error(error.text);
	}
};

export const toastResponseError = responseError => {
	let error;
	if (responseError.response) {
		error = _getResponseErrorConstant(responseError);
	} else {
		error = errors.INTERNAL_SERVER_ERROR;
	}
	
	toastError(error);
};

export const toastLoginRequired = () => {
	toast.info('Login is required for this action.');
};

export const logError = (error, errorInfo) => {
	if (process.env.NODE_ENV === 'production') {
		Sentry.withScope(scope => {
			scope.setExtras(errorInfo);
			Sentry.captureException(error);
		});
	}
};

const _getResponseErrorConstant = responseError => {
	for (let key in errors) {
		if (errors[key].value === responseError.response.data.code) {
			return errors[key];
		}
	}

	return errors.INTERNAL_SERVER_ERROR;
};
