import { screenSizes, errors } from '../constants';
import { toast } from 'react-toastify';
import {
	isMobile,
	toastError,
	toastResponseError,
	toastLoginRequired,
} from './ui';

jest.mock('react-toastify', () => ({
	toast: {
		warning: jest.fn(),
		error: jest.fn(),
		info: jest.fn(),
	},
}));

describe('UI utility tests', () => {
	afterEach(() => {
		jest.clearAllMocks();
	});

	describe('Function isMobile tests', () => {
		it('Should return true when screenSize is mobile', () => {
			expect(isMobile(screenSizes.SMALL)).toBe(true);
		});

		it('Should return false when screenSize is not mobile', () => {
			expect(isMobile(screenSizes.MEDIUM)).toBe(false);
		});
	});

	describe('Function toastError', () => {
		it('Should display warning toast', () => {
			const error = { type: 'warning', text: 'Test warning' };

			toastError(error);

			expect(toast.warning).toHaveBeenCalledTimes(1);
			expect(toast.warning).toHaveBeenCalledWith(error.text);
		});

		it('Should display error toast', () => {
			const error = { type: 'error', text: 'Test warning' };

			toastError(error);

			expect(toast.error).toHaveBeenCalledTimes(1);
			expect(toast.error).toHaveBeenCalledWith(error.text);
		});
	});

	describe('Function toastResponseError', () => {
		it('Should toast a listed error', () => {
			const error = new Error();
			error.response = { data: { code: errors.WRONG_CREDENTIALS.value } };

			toastResponseError(error);

			expect(toast.warning).toHaveBeenCalledTimes(1);
			expect(toast.warning).toHaveBeenCalledWith(errors.WRONG_CREDENTIALS.text);
		});

		it('Should toast internal error if error is not listed', () => {
			const error = new Error();
			error.response = { data: { code: 'notListedError' } };

			toastResponseError(error);

			expect(toast.error).toHaveBeenCalledTimes(1);
			expect(toast.error).toHaveBeenCalledWith(errors.INTERNAL_SERVER_ERROR.text);
		});

		it('Should toast internal error if error is not response type', () => {
			const error = new Error();

			toastResponseError(error);

			expect(toast.error).toHaveBeenCalledTimes(1);
			expect(toast.error).toHaveBeenCalledWith(errors.INTERNAL_SERVER_ERROR.text);
		});
	});

	describe('Function toastLoginRequired tests', () => {
		it('Should toast info message', () => {
			toastLoginRequired();

			expect(toast.info).toHaveBeenCalledTimes(1);
			expect(toast.info).toHaveBeenCalledWith('Login is required for this action.');
		});
	});
});
