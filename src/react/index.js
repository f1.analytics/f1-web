import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import { BrowserRouter, Route } from 'react-router-dom';
import * as Sentry from '@sentry/browser';
import { RewriteFrames } from '@sentry/integrations';
import App from './components/app';
import store from './store';
import './sass/index.scss';
import 'semantic-ui-css/semantic.min.css';
import 'react-toastify/dist/ReactToastify.css';
import '@babel/polyfill';

const persistor = persistStore(store);

Sentry.init({
	dsn: 'https://8a98f124b0714272a1ca381e012f06cc@sentry.io/1540617',
	integrations: [new RewriteFrames()],
});

ReactDOM.render((
		<Provider store={store}>
			<PersistGate loading={null} persistor={persistor}>
				<BrowserRouter>
					<Route
						path="/"
						render={props => (
							<App {...props}/>
						)}
					/>
				</BrowserRouter>
			</PersistGate>
		</Provider>
	),
	document.getElementById('root')
);
