import React from 'react';
import { mount } from 'enzyme';
import Register from './register.jsx';
import { errors } from '../../constants';
import { toastError } from '../../utility';

jest.mock('../../utility', () => ({
	toastError: jest.fn(),
}));
jest.mock('../common/webPage', () => ({ children }) => children);

const findWrapperInput = (wrapper, name) => wrapper.find(`input[name="${name}"]`);

const writeToWrapperInput = (wrapper, name, text) => {
	const input = findWrapperInput(wrapper, name);
	input.simulate('change', { target: { value: text } });
};

describe('Register component tests', () => {
	const username = 'user1';
	const email = 'user@gmail.com';
	const confirmEmail = 'user@gmail.com';
	const password = 'password1234';
	const confirmPassword = 'password1234';
	const wrapper = mount(
		<Register
			clearSession={jest.fn()}
			register={jest.fn()}
			registerLoading={false}
			history={{ push: jest.fn() }}
		/>
	);

	beforeEach(() => {
		jest.clearAllMocks();
	});

	it('Should render without crashing', () => {
		expect(wrapper).toBeDefined();
	});

	it('Should render registration inputs', () => {
		expect(findWrapperInput(wrapper, 'username').hostNodes().length).toBe(1);
		expect(findWrapperInput(wrapper, 'email').hostNodes().length).toBe(1);
		expect(findWrapperInput(wrapper, 'confirmEmail').hostNodes().length).toBe(1);
		expect(findWrapperInput(wrapper, 'password').hostNodes().length).toBe(1);
		expect(findWrapperInput(wrapper, 'confirmPassword').hostNodes().length).toBe(1);
	});

	it('Should render submit button', () => {
		expect(wrapper.find('button[type="submit"]').hostNodes().length).toBe(1);
	});

	it('Should not allow inputs longer than 128 characters', () => {
		const longString = 'x'.repeat(129);

		writeToWrapperInput(wrapper, 'username', longString);
		writeToWrapperInput(wrapper, 'email', longString);
		writeToWrapperInput(wrapper, 'confirmEmail', longString);
		writeToWrapperInput(wrapper, 'password', longString);
		writeToWrapperInput(wrapper, 'confirmPassword', longString);

		expect(wrapper.find('input[name="username"]').text()).toBe('');
		expect(wrapper.find('input[name="email"]').text()).toBe('');
		expect(wrapper.find('input[name="confirmEmail"]').text()).toBe('');
		expect(wrapper.find('input[name="password"]').text()).toBe('');
		expect(wrapper.find('input[name="confirmPassword"]').text()).toBe('');
	});

	it('Should clear session after mount', () => {
		const clearSession = jest.fn();
		mount(
			<Register
				clearSession={clearSession}
				register={jest.fn()}
				registerLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		expect(clearSession).toHaveBeenCalledTimes(1);
	});

	it('Should redirect to homepage after register', () => {
		const push = jest.fn();
		const wrapper = mount(
			<Register
				clearSession={jest.fn()}
				register={jest.fn()}
				registerLoading={false}
				history={{ push }}
				userData={null}
			/>
		);

		wrapper.setProps({ userData: { username: 'user1' } });

		expect(push).toHaveBeenCalledTimes(1);
		expect(push).toHaveBeenCalledWith('/');
	});

	it('Should have disabled button before inputs change', () => {
		const register = jest.fn();
		const wrapper = mount(
			<Register
				clearSession={jest.fn()}
				register={register}
				registerLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		expect(wrapper.find('button[type="submit"]').hasClass('disabled')).toBe(true);
	});

	it('Should have loading button fetching register action', () => {
		const wrapper = mount(
			<Register
				clearSession={jest.fn()}
				register={jest.fn()}
				registerLoading={true}
				history={{ push: jest.fn() }}
			/>
		);

		expect(wrapper.find('button[type="submit"]').hasClass('loading')).toBe(true);
	});

	it('Should show submition warning when email is wrong format', () => {
		const register = jest.fn();
		const wrapper = mount(
			<Register
				clearSession={jest.fn()}
				register={register}
				registerLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		writeToWrapperInput(wrapper, 'username', username);
		writeToWrapperInput(wrapper, 'email', 'test');
		writeToWrapperInput(wrapper, 'confirmEmail', 'test');
		writeToWrapperInput(wrapper, 'password', password);
		writeToWrapperInput(wrapper, 'confirmPassword', confirmPassword);
		wrapper.find('form').simulate('submit');

		expect(register).toHaveBeenCalledTimes(0);
		expect(toastError).toHaveBeenCalledTimes(1);
		expect(toastError).toHaveBeenCalledWith(errors.INVALID_EMAIL);
	});

	it('Should show submition warning when emails mismatch', () => {
		const register = jest.fn();
		const wrapper = mount(
			<Register
				clearSession={jest.fn()}
				register={register}
				registerLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		writeToWrapperInput(wrapper, 'username', username);
		writeToWrapperInput(wrapper, 'email', email);
		writeToWrapperInput(wrapper, 'confirmEmail', 'test');
		writeToWrapperInput(wrapper, 'password', password);
		writeToWrapperInput(wrapper, 'confirmPassword', confirmPassword);
		wrapper.find('form').simulate('submit');

		expect(register).toHaveBeenCalledTimes(0);
		expect(toastError).toHaveBeenCalledTimes(1);
		expect(toastError).toHaveBeenCalledWith(errors.EMAIL_MISMATCH);
	});

	it('Should show submition warning when password too short', () => {
		const register = jest.fn();
		const wrapper = mount(
			<Register
				clearSession={jest.fn()}
				register={register}
				registerLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		writeToWrapperInput(wrapper, 'username', username);
		writeToWrapperInput(wrapper, 'email', email);
		writeToWrapperInput(wrapper, 'confirmEmail', confirmEmail);
		writeToWrapperInput(wrapper, 'password', '1234');
		writeToWrapperInput(wrapper, 'confirmPassword', '1234');
		wrapper.find('form').simulate('submit');

		expect(register).toHaveBeenCalledTimes(0);
		expect(toastError).toHaveBeenCalledTimes(1);
		expect(toastError).toHaveBeenCalledWith(errors.SHORT_PASSWORD);
	});

	it('Should show submition warning when password too short', () => {
		const register = jest.fn();
		const wrapper = mount(
			<Register
				clearSession={jest.fn()}
				register={register}
				registerLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		writeToWrapperInput(wrapper, 'username', username);
		writeToWrapperInput(wrapper, 'email', email);
		writeToWrapperInput(wrapper, 'confirmEmail', confirmEmail);
		writeToWrapperInput(wrapper, 'password', password);
		writeToWrapperInput(wrapper, 'confirmPassword', '1234');
		wrapper.find('form').simulate('submit');

		expect(register).toHaveBeenCalledTimes(0);
		expect(toastError).toHaveBeenCalledTimes(1);
		expect(toastError).toHaveBeenCalledWith(errors.PASSWORD_MISMATCH);
	});

	it('Should handle register submition', () => {
		const register = jest.fn();
		const wrapper = mount(
			<Register
				clearSession={jest.fn()}
				register={register}
				registerLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		writeToWrapperInput(wrapper, 'username', username);
		writeToWrapperInput(wrapper, 'email', email);
		writeToWrapperInput(wrapper, 'confirmEmail', confirmEmail);
		writeToWrapperInput(wrapper, 'password', password);
		writeToWrapperInput(wrapper, 'confirmPassword', confirmPassword);
		wrapper.find('form').simulate('submit');

		expect(register).toHaveBeenCalledTimes(1);
		expect(register).toHaveBeenCalledWith(email, username, password);
	});
});
