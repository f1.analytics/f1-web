import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Form } from 'semantic-ui-react';
import { errors } from '../../constants';
import { toastError } from '../../utility';
import AuthModal from '../common/authModal';
import WebPage from '../common/webPage';
import './register.scss';

const inputs = [{
	placeholder: 'Username',
	name: 'username',
}, {
	placeholder: 'Your Email',
	name: 'email',
}, {
	placeholder: 'Confirm Email',
	name: 'confirmEmail',
}, {
	placeholder: 'Your Password',
	name: 'password',
	type: 'password',
}, {
	placeholder: 'Confirm Password',
	name: 'confirmPassword',
	type: 'password',
}];

const Register = ({
	register,
	clearSession,
	clearUsernameTakenError,
	clearEmailTakenError,
	registerLoading,
	usernameTakenError,
	emailTakenError,
	history,
	userData,
}) => {
	const [formData, setFormData] = useState({});
	const [formErrors, setFormErrors] = useState({});

	function assignFormData(assignObject) {
		setFormData(Object.assign({}, formData, assignObject));
	}

	function assignFormErrors(assignObject) {
		setFormErrors(Object.assign({}, formErrors, assignObject));
	}

	function isSubmitDisabled() {
		for (const input of inputs) {
			if (!formData[input.name]) return true;
			if (formErrors[input.name]) return true;
		}
		return false;
	}

	const handleChange = useCallback((e, { name, value }) => {
		if (value.length < 128) {
			assignFormData({ [name]: value });
			assignFormErrors({ [name]: false });
		}
	}, [formData, formErrors]);

	const handleSubmit = useCallback(() => {
		let { username, email, confirmEmail, password, confirmPassword } = formData;

		username = username.trim();
		email = email.trim();
		confirmEmail = confirmEmail.trim();

		/* eslint-disable */
		const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		/* eslint-disable */

		if (!emailRegex.test(email.toLowerCase())) {
			assignFormErrors({ email: true });
			toastError(errors.INVALID_EMAIL);
		} else if (email !== confirmEmail) {
			assignFormErrors({ confirmEmail: true });
			toastError(errors.EMAIL_MISMATCH);
		} else if (password.length < 8) {
			assignFormErrors({ password: true });
			toastError(errors.SHORT_PASSWORD);
		} else if (password !== confirmPassword) {
			assignFormErrors({ confirmPassword: true });
			toastError(errors.PASSWORD_MISMATCH);
		} else {
			register(email, username, password);
		}
	}, [formData]);

	useEffect(() => {
		if (userData) history.push('/');
	}, [userData]);

	useEffect(() => {
		if (usernameTakenError) {
			clearUsernameTakenError();
			assignFormErrors({ username: true });
		}
	});

	useEffect(() => {
		if (emailTakenError) {
			clearEmailTakenError();
			assignFormErrors({ email: true, confirmEmail: true });
		}
	}, [emailTakenError]);

	useEffect(clearSession, []);

	return (
		<WebPage id='register-page'>
			<AuthModal id='register-modal' title='SIGN UP'>
				<Form onSubmit={handleSubmit}>
					{inputs.map(input => (
						<Form.Input
							key={input.name}
							className='web-input web-input-1'
							placeholder={input.placeholder}
							name={input.name}
							type={input.type}
							onChange={handleChange}
							error={formErrors[input.name]}
						/>
					))}
					<Form.Button
						className='submit-button'
						type='submit'
						loading={registerLoading}
						disabled={isSubmitDisabled()}
					>
						Submit
					</Form.Button>
				</Form>
			</AuthModal>
		</WebPage>
	);
};

Register.propTypes = {
	register: PropTypes.func.isRequired,
	clearSession: PropTypes.func.isRequired,
	clearUsernameTakenError: PropTypes.func.isRequired,
	clearEmailTakenError: PropTypes.func.isRequired,
	registerLoading: PropTypes.bool.isRequired,
	usernameTakenError: PropTypes.bool.isRequired,
	emailTakenError: PropTypes.bool.isRequired,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired
	}),
	userData: PropTypes.object
};

export default withRouter(Register);
