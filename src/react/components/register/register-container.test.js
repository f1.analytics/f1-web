import { mapStateToProps, mapDispatchToProps } from './register-container.js';
import { clearSession, register, setUsernameTakenError, setEmailTakenError } from '../../actions';

describe('Register container tests', () => {
	const email = 'email@gmail.com';
	const username = 'user1';
	const password = 'password123';
	const emailTakenError = true;
	const usernameTakenError = true;
	const userData = { username };

	it('Should mapStateToProps', () => {
		const registerLoading = false;
		const state = {
			session: {
				userData,
			},
			user: {
				registerLoading,
				emailTakenError,
				usernameTakenError,
			},
		};

		expect(mapStateToProps(state)).toEqual({
			userData,
			registerLoading,
			emailTakenError,
			usernameTakenError,
		});
	});

	describe('mapDispatchToProps tests', () => {
		const dispatch = jest.fn();
		const props = mapDispatchToProps(dispatch);

		it('Should map clearSession', () => {
			props.clearSession(userData);

			expect(dispatch).toHaveBeenCalledWith(clearSession(userData));
		});

		it('Should map register', () => {
			props.register(email, username, password);

			expect(dispatch).toHaveBeenCalledWith(register(email, username, password));
		});

		it('Should map clearEmailTakenError', () => {
			props.clearEmailTakenError();

			expect(dispatch).toHaveBeenCalledWith(setEmailTakenError(false));
		});

		it('Should map clearUsernameTakenError', () => {
			props.clearUsernameTakenError();

			expect(dispatch).toHaveBeenCalledWith(setUsernameTakenError(false));
		});
	});
});
