import { connect } from 'react-redux';
import Register from './register.jsx';
import { clearSession, register, setUsernameTakenError, setEmailTakenError } from '../../actions';

export const mapStateToProps = state => ({
	userData: state.session.userData,
	registerLoading: state.user.registerLoading,
	usernameTakenError: state.user.usernameTakenError,
	emailTakenError: state.user.emailTakenError,
});

export const mapDispatchToProps = dispatch => ({
	clearSession: () => {
		dispatch(clearSession());
	},
	register: (email, username, password) => {
		dispatch(register(email, username, password));
	},
	clearUsernameTakenError: () => {
		dispatch(setUsernameTakenError(false));
	},
	clearEmailTakenError: () => {
		dispatch(setEmailTakenError(false));
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Register);
