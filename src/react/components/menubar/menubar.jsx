import React from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import { menuOptions } from '../../constants';
import MenubarLink from './menubarLink';
import './menubar.scss';

const Menubar = ({
	logout,
	toggleSidebar,
	userData,
	activeMenu,
	isTop,
}) => (
	<div id='menubar' className={isTop ? 'top' : undefined}>
		<div id='sidebar-control' onClick={toggleSidebar}>
			<Icon name='bars'/>
		</div>
		<div id='menubar-options'>
			<MenubarLink
				to={menuOptions.DASHBOARD.path}
				text={menuOptions.DASHBOARD.text}
				active={menuOptions.DASHBOARD.value === activeMenu}
			/>
			{userData && userData.isSuperuser && (
				<MenubarLink
					to={menuOptions.ADMIN.path}
					text={menuOptions.ADMIN.text}
					active={menuOptions.ADMIN.value === activeMenu}
				/>
			)}
			{!userData && (
				<MenubarLink
					to={menuOptions.SIGN_UP.path}
					text={menuOptions.SIGN_UP.text}
					active={menuOptions.SIGN_UP.value === activeMenu}
				/>
			)}
			{!userData && (
				<MenubarLink
					to={menuOptions.LOGIN.path}
					text={menuOptions.LOGIN.text}
					active={menuOptions.LOGIN.value === activeMenu}
				/>
			)}
			{userData && (
				<MenubarLink
					id='logout-button'
					to={menuOptions.LOGOUT.path}
					text={menuOptions.LOGOUT.text}
					onClick={logout}
				/>
			)}
		</div>
	</div>
);

Menubar.propTypes = {
	toggleSidebar: PropTypes.func.isRequired,
	logout: PropTypes.func.isRequired,
	isTop: PropTypes.bool.isRequired,
	activeMenu: PropTypes.string,
	userData: PropTypes.object,
};

export default Menubar;
