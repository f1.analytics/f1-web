import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './menubarLink.scss';

const MenubarLink = ({ id, to, text, onClick, active }) => (
	<div id={id} className={`menubar-link-container${active ? ' active' : ''}`} onClick={onClick}>
		<Link to={to}>
			<div className='menubar-link-text'>
				{text}
			</div>
		</Link>
	</div>
);

MenubarLink.propTypes = {
	to: PropTypes.string.isRequired,
	text: PropTypes.string.isRequired,
	id: PropTypes.string,
	active: PropTypes.bool,
	onClick: PropTypes.func,
};

export default MenubarLink;
