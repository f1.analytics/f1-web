import React from 'react';
import { mount } from 'enzyme';
import Menubar from './menubar.jsx';

describe('Menubar component tests', () => {
	const userData = { username: 'user1' };
	const wrapper = mount(
		<Menubar
			toggleSidebar={jest.fn()}
			userData={null}
		/>
	);

	it('Should render without crashing', () => {
		expect(wrapper).toBeDefined();
	});

	it('Should render login button', () => {
		expect(wrapper.text().includes('Login')).toBe(true);
	});

	it('Should render signup button', () => {
		expect(wrapper.text().includes('Sign up')).toBe(true);
	});

	it('Should handle toggle sidebar button', () => {
		const toggleSidebar = jest.fn();
		const wrapper = mount(
			<Menubar
				toggleSidebar={toggleSidebar}
			/>
		);

		wrapper.find('#sidebar-control').simulate('click');

		expect(toggleSidebar).toHaveBeenCalledTimes(1);
	});

	it('Should render logout button if user is logged in', () => {
		const wrapper = mount(
			<Menubar
				toggleSidebar={jest.fn()}
				userData={userData}
				logout={jest.fn()}
			/>
		);

		expect(wrapper.text().includes('login')).toBe(false);
		expect(wrapper.text().includes('Sign up')).toBe(false);
		expect(wrapper.text().includes('Logout')).toBe(true);
	});

	it('Should handle logout click', () => {
		const logout = jest.fn();
		const wrapper = mount(
			<Menubar
				toggleSidebar={jest.fn()}
				userData={userData}
				logout={logout}
			/>
		);

		wrapper.find('#logout-button').hostNodes().simulate('click');

		expect(logout).toHaveBeenCalledTimes(1);
	});
});
