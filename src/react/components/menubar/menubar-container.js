import { connect } from 'react-redux';
import Menubar from './menubar.jsx';
import { toggleSidebar, clearSession } from '../../actions';

export const mapStateToProps = state => ({
	userData: state.session.userData,
	isTop: state.ui.isTop,
});

export const mapDispatchToProps = dispatch => ({
	toggleSidebar: () => {
		dispatch(toggleSidebar());
	},
	logout: () => {
		dispatch(clearSession());
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Menubar);
