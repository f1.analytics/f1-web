import { mapDispatchToProps, mapStateToProps } from './menubar-container.js';
import { toggleSidebar, clearSession } from '../../actions';

describe('Menubar container tests', () => {
	it('Should mapStateToProps', () => {
		const userData = { username: 'user1' };
		const isTop = true;
		const state = {
			session: {
				userData,
			},
			ui: {
				isTop,
			},
		};

		expect(mapStateToProps(state)).toEqual({
			userData,
			isTop,
		});
	});

	describe('mapDispatchToProps tests', () => {
		const dispatch = jest.fn();
		const props = mapDispatchToProps(dispatch);

		it('Should map clearSession', () => {
			props.toggleSidebar();

			expect(dispatch).toHaveBeenCalledWith(toggleSidebar());
		});

		it('Should map logout', () => {
			props.logout();

			expect(dispatch).toHaveBeenCalledWith(clearSession());
		});
	});
});
