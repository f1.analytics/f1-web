import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';

import DashboardControl from './dashboardControl';
import DashboardView from './dashboardView';
import DashboardSettings from './dashboardSettings';
import CardSelection from './cardSelection';
import LoadingOverlay from '../common/loadingOverlay';
import WebPage from '../common/webPage';
import { toastLoginRequired } from '../../utility';
import { richDashboardPropType } from '../../constants';

const Dashboard = ({
	dashboard,
	userData,
	setDashboard,
	getDashboard,
	match,
	subject,
	createCard,
	creatingCard,
	createCardError,
	dashboardLoading,
}) => {
	const [dashboardSettingsOpen, setDashboardSettingsOpen] = useState(false);
	const [cardSelectionOpen, setCardSelectionOpen] = useState(false);

	const openDashboardSettings = useCallback(() => {
		if (userData) {
			setDashboardSettingsOpen(true);
		} else {
			toastLoginRequired();
		}
	}, [userData]);

	const handleCardDelete = useCallback(cardId => {
		dashboard.dataCards = dashboard.dataCards.filter(card => card.id !== cardId);
		setDashboard(Object.assign({}, dashboard));
	}, [dashboard]);

	useEffect(() => {
		getDashboard(match.params.dashboardId);
	}, []);

	return (
		<WebPage id='dashboard-page'>
			{dashboard && (
				<>
					<DashboardControl dashboard={dashboard} openDashboardSettings={openDashboardSettings} />
					<DashboardView
						userData={userData}
						dataCards={dashboard.dataCards}
						dataCardTypes={dashboard.dashboardType.dataCardTypes}
						subject={subject}
						onCardDelete={handleCardDelete}
						openCardSelection={() => setCardSelectionOpen(true)}
					/>
				</>
			)}
			{dashboardSettingsOpen && (
				<DashboardSettings
					dashboard={dashboard}
					onClose={() => setDashboardSettingsOpen(false)}
				/>
			)}
			{cardSelectionOpen && (
				<CardSelection
					onClose={() => setCardSelectionOpen(false)}
					createCard={createCard}
					creatingCard={creatingCard}
					createCardError={createCardError}
					dashboard={dashboard}
				/>
			)}
			<LoadingOverlay loading={dashboardLoading}/>
		</WebPage>
	);
};

Dashboard.propTypes = {
	match: PropTypes.shape({
		params: PropTypes.shape({
			dashboardId: PropTypes.string.isRequired,
		}).isRequired,
	}).isRequired,
	getDashboard: PropTypes.func.isRequired,
	setDashboard: PropTypes.func.isRequired,
	createCard: PropTypes.func.isRequired,
	dashboardLoading: PropTypes.bool.isRequired,
	creatingCard: PropTypes.bool.isRequired,
	createCardError: PropTypes.bool.isRequired,
	userData: PropTypes.object,
	dashboard: richDashboardPropType,
	subject: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	]),
};

export default Dashboard;
