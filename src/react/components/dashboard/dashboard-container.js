import { connect } from 'react-redux';
import Dashboard from './dashboard.jsx';
import { getDashboard, setDashboard, createCard } from '../../actions';

export const mapStateToProps = state => ({
	dashboard: state.dashboards.dashboard,
	dashboardLoading: state.dashboards.dashboardLoading,
	subject: state.dashboards.subject,
	creatingCard: state.dashboards.creatingCard,
	createCardError: state.dashboards.createCardError,
	userData: state.session.userData,
});

export const mapDispatchToProps = dispatch => ({
	getDashboard: dashboardId => {
		dispatch(getDashboard(dashboardId));
	},
	setDashboard: dashboard => {
		dispatch(setDashboard(dashboard));
	},
	createCard: card => {
		dispatch(createCard(card));
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Dashboard);
