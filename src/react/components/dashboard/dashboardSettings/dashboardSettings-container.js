import { connect } from 'react-redux';
import DashboardSettings from './dashboardSettings.jsx';
import { updateDashboard, deleteDashboard } from '../../../actions';

export const mapStateToProps = state => ({
    filterOptionsLoading: state.dashboards.settingsFilterOptionsLoading,
    filterOptions: state.dashboards.settingsFilterOptions,
    updateDashboardLoading: state.dashboards.updateDashboardLoading,
    updateDashboardError: state.dashboards.updateDashboardError,
    deleteDashboardLoading: state.dashboards.deleteDashboardLoading,
    deleteDashboardError: state.dashboards.deleteDashboardError,
    screenSize: state.ui.screenSize,
});

export const mapDispatchToProps = dispatch => ({
    updateDashboard: dashboard => {
        dispatch(updateDashboard(dashboard));
    },
    deleteDashboard: dashboardId => {
        dispatch(deleteDashboard(dashboardId));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardSettings);
