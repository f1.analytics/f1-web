import { mapStateToProps, mapDispatchToProps } from './dashboardSettings-container';
import { updateDashboard, deleteDashboard } from '../../../actions';

describe('Dashboard settings container tests', () => {
    const filterOptionsLoading = true;
    const updateDashboardLoading = false;
    const updateDashboardError = false;
    const deleteDashboardLoading = true;
    const deleteDashboardError = false;
    const filterOptions = [{ key: 1, value: 2}];
    const screenSize = { index: 0 };
    const state = {
        dashboards: {
            settingsFilterOptionsLoading: filterOptionsLoading,
            settingsFilterOptions: filterOptions,
            updateDashboardLoading,
            updateDashboardError,
            deleteDashboardLoading,
            deleteDashboardError,
        },
        ui: {
            screenSize,
        },
    };

    it('Should mapStateToProps', () => {
        expect(mapStateToProps(state)).toEqual({
            filterOptionsLoading,
            filterOptions,
            updateDashboardLoading,
            updateDashboardError,
            deleteDashboardLoading,
            deleteDashboardError,
            screenSize,
        });
    });

    describe('mapDispatchToProps tests', () => {
        const dashboardId = 4;
        const dashboard = { type: 'driver' };
        const dispatch = jest.fn();
        const props = mapDispatchToProps(dispatch);

        it('Should map updateDashboard', () => {
            props.updateDashboard(dashboard);

            expect(dispatch).toHaveBeenCalledWith(updateDashboard(dashboard));
        });

        it('Should map deleteDashboard', () => {
            props.deleteDashboard(dashboardId);

            expect(dispatch).toHaveBeenCalledWith(deleteDashboard(dashboardId));
        });
    });
});
