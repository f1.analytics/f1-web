import React, { useState, useEffect } from 'react';
import { useDidUpdateEffect } from '../../../utility/hooks';
import PropTypes from 'prop-types';
import WebModal from '../../common/webModal';
import { withRouter } from 'react-router-dom';
import { Dropdown, Button, Input } from 'semantic-ui-react';
import { isMobile } from '../../../utility';
import { menuOptions, richDashboardPropType } from '../../../constants';
import './dashboardSettings.scss';
import { useDispatch } from 'react-redux';
import { getSettingsFilterOptions } from '../../../redux/dashboards/dashboards-actions';

const DashboardSettings = ({
    dashboard,
    screenSize,
    filterOptions,
    filterOptionsLoading,
    updateDashboardLoading,
    deleteDashboardLoading,
    updateDashboardError,
    deleteDashboardError,
    deleteDashboard,
    updateDashboard,
    onClose,
    history,
}) => {
    const dispatch = useDispatch();

    const getDefaultSubjectSelection = () => {
        if (dashboard.config.defaultSubjectSelection) {
            const foundSubjectSelection = dashboard.dashboardType.subjectSelections.find(({ id }) =>
              id === dashboard.config.defaultSubjectSelection.subjectSelectionId,
            );
            if (foundSubjectSelection) {
                return foundSubjectSelection;
            }
        }
        return dashboard.dashboardType.subjectSelections[0];
    };

    const getDefaultFilterValue = () =>
      dashboard.config.defaultSubjectSelection ? dashboard.config.defaultSubjectSelection.filterValue : null;

    const [subjectSelection, setSubjectSelection] = useState(getDefaultSubjectSelection());
    const [filterValue, setFilterValue] = useState(getDefaultFilterValue());
    const [dashboardCopy, setDashboardCopy] = useState(dashboard);

    const handleNameChange = (e, { value }) => {
        setDashboardCopy(Object.assign({}, dashboardCopy, { name: value }));
    };

    const handleSubjectSelectionChange = (e, { value }) => {
        setSubjectSelection(dashboard.dashboardType.subjectSelections.find(({ id }) => id === value));
        setFilterValue(null);
    };

    const handleFilterChange = (e, { value }) => {
        setFilterValue(value);
    };

    const handleDeleteDashboard = () => {
        deleteDashboard(dashboard.id);
    };

    const handleSave = () => {
        updateDashboard({
            id: dashboardCopy.id,
            name: dashboardCopy.name,
            dashboardTypeId: dashboard.dashboardType.id,
            config: dashboard.config,
        });
    };

    useEffect(() => {
        dashboardCopy.config.defaultSubjectSelection = { subjectSelectionId: subjectSelection.id, filterValue: null };
        if (subjectSelection.config.filterSelect) {
            dispatch(getSettingsFilterOptions(subjectSelection.id));
        }
        setDashboardCopy(Object.assign({}, dashboardCopy));
    }, [subjectSelection]);

    useEffect(() => {
        dashboardCopy.config.defaultSubjectSelection.filterValue = filterValue;
        setDashboardCopy(Object.assign({}, dashboardCopy));
    }, [filterValue]);

    useDidUpdateEffect(() => {
        if (!updateDashboardLoading && !updateDashboardError) {
            onClose();
        }
    }, [updateDashboardLoading]);

    useDidUpdateEffect(() => {
        if (!deleteDashboardLoading && !deleteDashboardError) {
            history.push(menuOptions.DASHBOARD.path);
        }
    }, [deleteDashboardLoading]);

    return (
      <WebModal id='dashboard-settings' title='Dashboard Settings' onClose={onClose}>
          <div id='default-filter-setting' className='setting'>
              <div className='setting-header'>Select default filter</div>
              <div className='setting-content'>
                  <div className='setting-dropdown'>
                      <Dropdown
                        id='filter-type-dropdown'
                        className='web-dropdown'
                        onChange={handleSubjectSelectionChange}
                        options={dashboard.dashboardType.subjectSelections.map(({ id, config: { selectionTypePlaceholder }}) => ({
                            text: selectionTypePlaceholder,
                            value: id,
                        }))}
                        value={subjectSelection ? subjectSelection.id : null}
                        search={!isMobile(screenSize)}
                        upward={false}
                        selection
                      />
                  </div>
                  <div className='setting-dropdown'>
                      <Dropdown
                        id='filter-dropdown'
                        className='web-dropdown'
                        placeholder={subjectSelection && subjectSelection.config.filterSelect && subjectSelection.config.filterSelect.placeholder}
                        onChange={handleFilterChange}
                        value={filterValue}
                        options={filterOptions}
                        disabled={!subjectSelection || !subjectSelection.config.filterSelect}
                        loading={filterOptionsLoading}
                        search={!isMobile(screenSize)}
                        upward={false}
                        selection
                      />
                  </div>
              </div>
          </div>
          <div id='title-setting' className='setting'>
              <div className='setting-header'>Edit dashboard name</div>
              <div className='setting-content'>
                  <div className='setting-input'>
                      <Input
                        onChange={handleNameChange}
                        value={dashboardCopy.name}
                        className='web-input web-input-2'
                      />
                  </div>
              </div>
          </div>
          <div id='delete-setting' className='setting'>
              <div className='setting-header'>Delete dashboard</div>
              <div className='setting-content'>
                  <div className='setting-button'>
                      <Button
                        id='delete-button'
                        className='web-input web-button-3 warning'
                        onClick={handleDeleteDashboard}
                        loading={deleteDashboardLoading}
                        disabled={updateDashboardLoading || deleteDashboardLoading}
                      >
                          Delete
                      </Button>
                  </div>
              </div>
          </div>
          <div className='modal-buttons'>
              <Button
                id='cancel-button'
                className='web-button-3'
                onClick={onClose}
              >
                  Cancel
              </Button>
              <Button
                id='save-button'
                className='web-button-3'
                onClick={handleSave}
                loading={updateDashboardLoading}
                disabled={deleteDashboardLoading || updateDashboardLoading}
              >
                  Save
              </Button>
          </div>
      </WebModal>
    );
};

DashboardSettings.propTypes = {
    filterOptionsLoading: PropTypes.bool.isRequired,
    updateDashboardLoading: PropTypes.bool.isRequired,
    updateDashboardError: PropTypes.bool.isRequired,
    deleteDashboardLoading: PropTypes.bool.isRequired,
    deleteDashboardError: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    updateDashboard: PropTypes.func.isRequired,
    deleteDashboard: PropTypes.func.isRequired,
    screenSize: PropTypes.object.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired,
    dashboard: richDashboardPropType.isRequired,
    filterOptions: PropTypes.arrayOf(PropTypes.shape({
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]),
        text: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number,
        ]).isRequired,
    })),
};

export default withRouter(DashboardSettings);
