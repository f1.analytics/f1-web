import React from 'react';
import { mount } from 'enzyme';
import DashboardSettings from './dashboardSettings.jsx';
import { dashboardTypes, filterTypes, menuOptions } from '../../../constants';

describe.skip('Dashboard settings component tests', () => {
    const dashboard = {
        type: dashboardTypes.driver.value,
        title: 'Test title',
        id: 1,
        dataCards: [],
        config: { defaultFilter: { filterType: filterTypes.season.value, filterValue: 2016 } },
    };
    const filterOptions = [{ key: 2016, value: 2016, text: 2016 }, { key: 2017, value: 2017, text: 2017 }];
    const screenSize = { index: 2 };
    const getFilterOptions = jest.fn();
    const deleteDashboard = jest.fn();

    const wrapper = mount(
        <DashboardSettings
            dashboard={dashboard}
            screenSize={screenSize}
            filterOptions={filterOptions}
            getFilterOptions={getFilterOptions}
            deleteDashboard={deleteDashboard}
        />
    );

    it('Should render without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should display input with title', () => {
        expect(wrapper.find('#title-setting input').hostNodes().props()['value']).toBe(dashboard.title);
    });

    it('Should handle title input change', () => {
        wrapper.find('#title-setting input').hostNodes().simulate('change', { target: { value: 'test' } });

        expect(wrapper.find('#title-setting input').hostNodes().props()['value']).toBe('test');
    });

    it('Should have fetched filter options and display them', () => {
        expect(getFilterOptions).toHaveBeenCalledTimes(1);
        expect(getFilterOptions).toHaveBeenCalledWith(filterTypes.season.value);
        expect(wrapper.find('#filter-type-dropdown').hostNodes().text()).toContain(filterTypes.season.text);
        expect(wrapper.find('#filter-dropdown').hostNodes().text()).toContain(filterOptions[0].text);
    });

    it('Should handle filter dropdowns change', () => {
        getFilterOptions.mockReset();

        expect(wrapper.find('#filter-dropdown').hostNodes().prop('onChange')(null, 2017));
        expect(wrapper.find('#filter-dropdown div.text').hostNodes().text()).toContain(2017);

        expect(wrapper.find('#filter-type-dropdown').hostNodes().prop('onChange')(null, filterTypes.driverNationality.value));
        expect(wrapper.find('#filter-type-dropdown div.text').hostNodes().text()).toContain(filterTypes.driverNationality.text);
        expect(getFilterOptions).toHaveBeenCalledTimes(1);
        expect(getFilterOptions).toHaveBeenCalledWith(filterTypes.driverNationality.value);
    });

    it('Should handle removing filter', () => {
        getFilterOptions.mockReset();

        expect(wrapper.find('#filter-type-dropdown').hostNodes().prop('onChange')(null, null));
        expect(wrapper.find('#filter-type-dropdown div.text').hostNodes().text()).toContain('Filter by');
        expect(getFilterOptions).toHaveBeenCalledTimes(0);
    });

    it('Should handle delete', () => {
        wrapper.find('#delete-button').hostNodes().simulate('click');

        expect(deleteDashboard).toHaveBeenCalledTimes(1);
        expect(deleteDashboard).toHaveBeenCalledWith(dashboard.id);
    });

    it('Should handle save', () => {
        const updateDashboard = jest.fn();
        const wrapper = mount(
            <DashboardSettings
                dashboard={dashboard}
                screenSize={screenSize}
                filterOptions={filterOptions}
                getFilterOptions={jest.fn()}
                deleteDashboard={jest.fn()}
                updateDashboard={updateDashboard}
            />
        );

        wrapper.find('#save-button').hostNodes().simulate('click');

        expect(updateDashboard).toHaveBeenCalledTimes(1);
        expect(updateDashboard).toHaveBeenCalledWith(dashboard);
    });

    it('Should close on update success', () => {
        const onClose = jest.fn();
        const wrapper = mount(
            <DashboardSettings
                dashboard={dashboard}
                screenSize={screenSize}
                filterOptions={filterOptions}
                getFilterOptions={jest.fn()}
                deleteDashboard={jest.fn()}
                updateDashboard={jest.fn()}
                onClose={onClose}
                updateDashboardLoading={true}
            />
        );

        wrapper.setProps({ updateDashboardLoading: false, updateDashboardError: false });

        expect(onClose).toHaveBeenCalledTimes(1);
    });

    it('Should redirect to dashboard list page on delete', () => {
        const history = { push: jest.fn() };
        const wrapper = mount(
            <DashboardSettings
                dashboard={dashboard}
                screenSize={screenSize}
                filterOptions={filterOptions}
                getFilterOptions={jest.fn()}
                deleteDashboard={jest.fn()}
                updateDashboard={jest.fn()}
                history={history}
                deleteDashboardLoading={true}
            />
        );

        wrapper.setProps({ deleteDashboardLoading: false, deleteDashboardError: false });

        expect(history.push).toHaveBeenCalledTimes(1);
        expect(history.push).toHaveBeenCalledWith(menuOptions.DASHBOARD.path);
    });
});
