import { mapStateToProps, mapDispatchToProps } from './dashboard-container.js';
import { getDashboard, setDashboard, createCard } from '../../actions';

describe('Dashboard container tests', () => {
	const dashboard = { type: 'driver' };
	const dashboardLoading = true;
	const subject = 4;
	const userData = { id: 2 };
	const creatingCard = false;
	const createCardError = false;
	const state = {
		dashboards: {
			dashboard,
			dashboardLoading,
			subject,
			creatingCard,
			createCardError,
		},
		session: {
			userData,
		},
	};

	it('Should mapStateToProps', () => {
		expect(mapStateToProps(state)).toEqual({
			dashboard,
			dashboardLoading,
			subject,
			creatingCard,
			createCardError,
			userData,
		});
	});

	describe('mapDispatchToProps tests', () => {
		const dashboardId = 4;
		const card = { id: 2 };
		const dispatch = jest.fn();
		const props = mapDispatchToProps(dispatch);

		it('Should map getDashboard', () => {
			props.getDashboard(dashboardId);

			expect(dispatch).toHaveBeenCalledWith(getDashboard(dashboardId));
		});

		it('Should map setDashboard', () => {
			props.setDashboard(dashboard);

			expect(dispatch).toHaveBeenCalledWith(setDashboard(dashboard));
		});

		it('Should map createCard', () => {
			props.createCard(card);

			expect(dispatch).toHaveBeenCalledWith(createCard(card));
		});
	});
});
