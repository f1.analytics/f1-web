import React from 'react';
import { mount } from 'enzyme';
import Dashboard from './dashboard.jsx';
import { toastLoginRequired } from '../../utility';

jest.mock('./dashboardControl/subjectSelection');
jest.mock('./dashboardSettings');
jest.mock('../../utility');
jest.mock('../../d3/chart/lineChart');
jest.mock('../../d3/chart/barChart');
jest.mock('../../api');
jest.mock('../common/webPage', () => ({ children }) => children);

describe('Dashboard component tests', () => {
	const match = {
		params: {
			dashboardId: 3,
		},
	};
	const dashboard = {
		dashboardType: {
			dataCardTypes: [{
				id: 1,
				type: 'infoBox',
			}, {
				id: 2,
				type: 'infoBox',
			}],
			name: 'Test type',
		},
		name: 'Test title', id: 1,
		dataCards: [],
		config: {},
	};
	const userData = { id: 2 };
	const getSelectionOptions = jest.fn();
	const subject = 4;
	const wrapper = mount(
		<Dashboard
			match={match}
			getDashboard={jest.fn()}
			getSelectionOptions={getSelectionOptions}
		/>
	);

	beforeEach(() => {
		jest.resetAllMocks();
	});

	it('Should render without crashing', () => {
		expect(wrapper).toBeDefined();
	});

	it('Should get dashboard on mount', () => {
		const getDashboard = jest.fn();
		mount(
			<Dashboard
				match={match}
				getDashboard={getDashboard}
				getSelectionOptions={jest.fn()}
				dashboard={null}
			/>
		);

		expect(getDashboard).toHaveBeenCalledTimes(1);
		expect(getDashboard).toHaveBeenCalledWith(match.params.dashboardId);
	});

	it('Should display dashboard type and title', () => {
		const wrapper = mount(
			<Dashboard
				match={match}
				getDashboard={jest.fn()}
				getSelectionOptions={jest.fn()}
				dashboard={dashboard}
			/>
		);

		const dashboardControl = wrapper.find('#dashboard-control').hostNodes();

		expect(dashboardControl.text()).toContain(dashboard.name);
		expect(dashboardControl.text()).toContain(dashboard.dashboardType.name);
	});

	it('Should display select subject message if subject is not selected', () => {
		const wrapper = mount(
			<Dashboard
				match={match}
				getDashboard={jest.fn()}
				getSelectionOptions={jest.fn()}
				dashboard={dashboard}
			/>
		);

		expect(wrapper.find('.message').hostNodes().length).toBe(1);
		expect(wrapper.find('.message.hidden').hostNodes().length).toBe(0);
		expect(wrapper.text()).toContain('Select subject to view dashboard.');
	});

	it('Should hide select subject message if subject is selected', () => {
		const wrapper = mount(
			<Dashboard
				match={match}
				getDashboard={jest.fn()}
				getSelectionOptions={jest.fn()}
				dashboard={dashboard}
				subject={subject}
			/>
		);

		expect(wrapper.find('.message.hidden').hostNodes().length).toBe(1);
	});

	it('Should not show dashboard settings if user is not logged in', () => {
		const wrapper = mount(
			<Dashboard
				match={match}
				getDashboard={jest.fn()}
				getSelectionOptions={jest.fn()}
				dashboard={dashboard}
				subject={subject}
				userData={null}
			/>
		);

		wrapper.find('#settings-button').hostNodes().simulate('click');
		expect(toastLoginRequired).toHaveBeenCalledTimes(1);
	});

	it('Should open card selection', () => {
		const wrapper = mount(
			<Dashboard
				match={match}
				getDashboard={jest.fn()}
				getSelectionOptions={jest.fn()}
				dashboard={dashboard}
				subject={subject}
				userData={userData}
			/>
		);

		wrapper.find('#new-card').hostNodes().simulate('click');

		expect(wrapper.find('#card-selection').hostNodes().length).toBe(1);
	});
});
