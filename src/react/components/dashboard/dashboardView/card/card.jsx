import React from 'react';
import PropTypes from 'prop-types';
import LoadingOverlay from '../../../common/loadingOverlay';
import WebChart from './webChart';
import InfoBox from './infoBox';
import { Dropdown, Icon } from 'semantic-ui-react';
import axios from 'axios';
import { getCardData, deleteCard } from '../../../../api';
import { toastError, toastLoginRequired } from '../../../../utility';
import { cardDisplayTypes, cardDisplaySubTypes, errors, leanDataCardTypePropType } from '../../../../constants';
import './card.scss';

const CancelToken = axios.CancelToken;

class Card extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            cardData: null,
        };

        this.setCancelAxios = this.setCancelAxios.bind(this);
        this.handleNewCardData = this.handleNewCardData.bind(this);
        this.handleDataFetchError = this.handleDataFetchError.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
        this.handleDeteleError = this.handleDeteleError.bind(this);
    }

    getCardDisplay() {
        const { card } = this.props;
        const { cardData } = this.state;

        if (cardData) {
            switch (cardData.displayType) {
                case cardDisplayTypes.INFO_BOX:
                    return (
                        <InfoBox
                            infoData={cardData}
                            id={`info-box-${card.id}`}
                        />
                    );
                case cardDisplayTypes.CHART:
                    return (
                        <WebChart
                            chartData={cardData}
                            id={`chart-${card.id}`}
                            type={cardData.displaySubType}
                            className={cardDisplaySubTypes[cardData.displaySubType].className}
                        />
                    );
                default:
                    throw new Error(`Display type ${cardData.displayType} not recognized!`);
            }
        }

        return null;
    }

    async getCardData() {
        const { card, subject } = this.props;

        this.setState({ loading: true });

        const cancelToken = this.createAxiosCancelToken();
        await getCardData(card.dataCardTypeId, `subject=${subject}`, cancelToken)
            .then(this.handleNewCardData)
            .catch(this.handleDataFetchError);
    }

    handleNewCardData({ cardData }) {
        this.setState({
            cardData,
            loading: false,
        });
        this.setCancelAxios(null);
    }

    handleDataFetchError(error) {
        if (!axios.isCancel(error)) {
            this.setState({ cardData: { data: [] }, loading: false });
            toastError(errors.INTERNAL_SERVER_ERROR);
        }
    }

    handleDeleteClick() {
        const { userData } = this.props;

        if (!userData) {
            toastLoginRequired();
        } else {
            this.deleteCard();
        }
    }

    async deleteCard() {
        const { card, onDelete } = this.props;

        this.setState({ loading: true });

        await deleteCard(card.id)
            .then(() => {
                onDelete(card.id);
            }).catch(this.handleDeteleError);
    }

    handleDeteleError() {
        this.setState({ loading: false });
        toastError(errors.INTERNAL_SERVER_ERROR);
    }

    cancelCardDataFetch() {
        if (this.cancelAxios) {
            this.cancelAxios();
        }
    }

    createAxiosCancelToken() {
        return new CancelToken(this.setCancelAxios);
    }

    setCancelAxios(cancelAxios) {
        this.cancelAxios = cancelAxios;
    }

    componentDidMount() {
        this.getCardData();
    }

    componentWillUnmount() {
        this.cancelCardDataFetch();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.subject !== this.props.subject && this.props.subject) {
            this.cancelCardDataFetch();
            this.getCardData();
        }
    }

    render() {
        const { card, dataCardType } = this.props;
        const { loading } = this.state;

        return (
            <div id={`card-${card.id}`} className={`card ${dataCardType.config.displayType}`}>
                <LoadingOverlay dark loading={loading}/>
                <div className='header'>
                    <div className='title'>{dataCardType.name}</div>
                    {dataCardType.config.note && <div className='note'>{`(${dataCardType.config.note})`}</div>}
                    <div className='settings'>
                        <Dropdown
                            trigger={<Icon name='ellipsis vertical'/>}
                            direction='left'
                            icon={null}
                            disabled={loading}
                        >
                            <Dropdown.Menu>
                                <Dropdown.Item  id='delete-card-option' text='Delete' onClick={this.handleDeleteClick}/>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                </div>
                {this.getCardDisplay()}
            </div>
        );
    }
}

Card.propTypes = {
    subject: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]).isRequired,
    dataCardType: leanDataCardTypePropType.isRequired,
    card: PropTypes.shape({
        id: PropTypes.number.isRequired,
        dataCardTypeId: PropTypes.number.isRequired,
    }).isRequired,
    onDelete: PropTypes.func.isRequired,
    userData: PropTypes.object,
};

export default Card;
