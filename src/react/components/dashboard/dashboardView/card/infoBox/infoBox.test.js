import React from 'react';
import { mount } from 'enzyme';
import InfoBox from './infoBox.jsx';

describe('InfoBox component tests', () => {
	const id = 'test';
	const infoData = { data: [{ value: '22', title: 'Race wins' }] };
	const wrapper = mount(
		<InfoBox
			id={id}
			infoData={infoData}
		/>
	);

	it('Should render without crashing', () => {
		expect(wrapper).toBeDefined();
	});

	it('Should render data', () => {
		expect(wrapper.find(`#${id} .value`).hostNodes().first().text()).toBe(infoData.data[0].value);
		expect(wrapper.find(`#${id} .title`).hostNodes().first().text()).toBe(infoData.data[0].title);
	});
});
