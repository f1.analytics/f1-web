import React from 'react';
import PropTypes from 'prop-types';
import './infoBox.scss';

const InfoBox = ({ infoData, id }) => (
    <div id={id} className='info-box'>
        {infoData.data.map((d, i) => (
            <div key={i} className='info-box-section'>
                <div className='title'>{d.title}</div>
                <div className='value'>{d.value}</div>
            </div>
        ))}
    </div>
);

InfoBox.propTypes = {
	id: PropTypes.string.isRequired,
	infoData: PropTypes.shape({
		data: PropTypes.arrayOf(PropTypes.shape({
			value: PropTypes.number.isRequired,
			title: PropTypes.string.isRequired,
		})).isRequired,
	}).isRequired,
};

export default InfoBox;
