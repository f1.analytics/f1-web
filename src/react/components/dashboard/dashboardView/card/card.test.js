import React from 'react';
import Card from './card.jsx';
import { mount } from 'enzyme';
import axios from 'axios';
import { cardDisplaySubTypes, cardDisplayTypes, cardTypes, dataFormatTypes, errors } from '../../../../constants';
import { getCardData, deleteCard } from '../../../../api';
import { toastError } from '../../../../utility';

jest.mock('axios');
jest.mock('../../../../api', () => ({
    getCardData: jest.fn(),
    deleteCard: jest.fn(),
}));
jest.mock('../../../../utility', () => ({
    toastError: jest.fn(),
    toastLoginRequired: jest.fn(),
}));
jest.mock('../../../../d3/chart/lineChart');
jest.mock('../../../../d3/chart/barChart');

describe.skip('Card component tests', () => {
    const subject = 4;
    const cardData = {
        data: [{
            y: 4,
            x: 2014,
        }, {
            y: 5,
            x: 2012,
        }],
        xFormat: dataFormatTypes.YEAR,
        xKey: 'position',
        yKey: 'season',
    };
    const userData = { id: 2 };
    const card = {
        id: 1,
        type: cardTypes.driverQualifyingCount.value,
        title: 'Test title',
        displayType: cardDisplayTypes.CHART,
        config: { displaySubType: cardDisplaySubTypes.lineChart.value },
    };
    const wrapper = mount(
        <Card
            subject={subject}
            card={card}
        />
    );

    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('Should render without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should render loader', () => {
        expect(wrapper.find('.loader').hostNodes().length).toBe(1);
    });

    it('Should render title', () => {
        expect(wrapper.text()).toContain(card.title);
    });

    it('Should get container data', async () => {
        getCardData.mockRestore();
        getCardData.mockImplementation(() => Promise.resolve({ cardData }));
        mount(
            <Card
                subject={subject}
                card={card}
            />
        );

        await Promise.resolve();

        expect(getCardData).toHaveBeenCalledTimes(1);
        expect(getCardData).toHaveBeenCalledWith(card.id, `id=${subject}`, expect.any(axios.CancelToken));
    });

    it('Should toast error on getting data', async () => {
        const promise = Promise.reject(new Error('test'));
        getCardData.mockRestore();
        getCardData.mockImplementation(() => promise);
        mount(
            <Card
                subject={subject}
                card={card}
            />
        );

        try {
            await promise.then();
        } catch (e) {}  // eslint-disable-line

        expect(toastError).toHaveBeenCalledTimes(1);
        expect(toastError).toHaveBeenCalledWith(errors.INTERNAL_SERVER_ERROR);
    });

    it('Should cancel axios request on unmount', () => {
        const cancelAxios = jest.fn();
        axios.CancelToken.mockImplementation(jest.fn(callback => callback(cancelAxios)));
        getCardData.mockImplementation(() => Promise.resolve({ cardData }));
        const wrapper = mount(
            <Card
                subject={subject}
                card={card}
            />
        );

        wrapper.unmount();

        expect(cancelAxios).toHaveBeenCalledTimes(1);
        expect(toastError).toHaveBeenCalledTimes(0);
    });

    it('Should cancel axios request on subject change', () => {
        const cancelAxios = jest.fn();
        axios.CancelToken.mockImplementation(jest.fn(callback => callback(cancelAxios)));
        getCardData.mockImplementation(() => Promise.resolve({ cardData }));
        const wrapper = mount(
            <Card
                subject={subject}
                card={card}
            />
        );

        wrapper.setProps({ subject: Object.assign({}, subject) });

        expect(cancelAxios).toHaveBeenCalledTimes(1);
        expect(toastError).toHaveBeenCalledTimes(0);
    });

    it('Should not delete card if user is not logged in', async () => {
        deleteCard.mockRestore();
        deleteCard.mockImplementation(() => Promise.resolve());
        const wrapper = mount(
            <Card
                subject={subject}
                card={card}
                userData={null}
            />
        );

        wrapper.find('#delete-card-option').hostNodes().simulate('click');
        await Promise.resolve();

        expect(deleteCard).toHaveBeenCalledTimes(0);
    });

    it('Should delete card', async () => {
        deleteCard.mockRestore();
        deleteCard.mockImplementation(() => Promise.resolve());
        const wrapper = mount(
            <Card
                subject={subject}
                card={card}
                userData={userData}
            />
        );

        wrapper.find('#delete-card-option').hostNodes().simulate('click');
        await Promise.resolve();

        expect(deleteCard).toHaveBeenCalledTimes(1);
        expect(deleteCard).toHaveBeenCalledWith(card.id);
    });
});
