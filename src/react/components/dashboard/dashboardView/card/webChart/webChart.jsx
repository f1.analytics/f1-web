import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import LineChart from '../../../../../d3/chart/lineChart';
import BarChart from '../../../../../d3/chart/barChart';
import { cardDisplaySubTypes } from '../../../../../constants';
import './webChart.scss';

const WebChart = ({
  chartData,
  className,
  type,
  id,
}) => {
    const [chart] = useState(getChartInstance());

    function getChartInstance() {
        const margin = { left: 40, right: 20, top: 20, bottom: 30 };

        switch (type) {
            case cardDisplaySubTypes.barChart.value:
                return new BarChart(id, margin);
            case cardDisplaySubTypes.lineChart.value:
                return new LineChart(id, margin);
        }
    }

    useEffect(() => {
        if (chart) {
            chart.initChart();
            chart.drawChart();
            chart.updateData(chartData);

            window.addEventListener('resize', () => chart.resize());

            return () => window.removeEventListener('resize', () => chart.resize());
        }
    }, [chart]);

    useEffect(() => {
        if (chartData && chart) {
            chart.updateData(chartData);
        }
    }, [chartData, chart]);

    return (
      <div id={id} className={`web-chart${className ? ` ${className}` : ''}`}>
          <div className='tooltip'>
              <div className='tooltip-text'/>
          </div>
      </div>
    );
};

WebChart.propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.oneOf(
        Object.keys(cardDisplaySubTypes).map(key => cardDisplaySubTypes[key].value)
    ).isRequired,
    chartData: PropTypes.object.isRequired,
    className: PropTypes.string,
};

export default WebChart;
