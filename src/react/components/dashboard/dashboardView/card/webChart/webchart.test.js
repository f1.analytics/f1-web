import React from 'react';
import { mount } from 'enzyme/build';
import WebChart from './webChart.jsx';
import LineChart from '../../../../../d3/chart/lineChart';
import BarChart from '../../../../../d3/chart/barChart';
import { cardDisplaySubTypes } from '../../../../../constants';

jest.mock('../../../../../d3/chart/lineChart');
jest.mock('../../../../../d3/chart/barChart');

describe('WebChart component tests', () => {
    const id = 'chart-4';
    const className = 'classTest';
    const wrapper = mount(
        <WebChart
            id={id}
            className={className}
            type={cardDisplaySubTypes.lineChart.value}
        />
    );

    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('Should render without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should render with id and className', () => {
        expect(wrapper.find(`#${id}`).hostNodes().length).toBe(1);
        expect(wrapper.find(`.${className}`).hostNodes().length).toBe(1);
    });

    it('Should render hover tooltip', () => {
        expect(wrapper.find('.tooltip').hostNodes().length).toBe(1);
    });

    it('Should update chart data on new data', () => {
        const chartData = [{ x: 2, y: 3 }];
        wrapper.setProps({ chartData });

        expect(LineChart.prototype.updateData).toHaveBeenCalledTimes(1);
        expect(LineChart.prototype.updateData).toHaveBeenCalledWith(chartData);
    });

    it('Should resize chart', () => {
        const map = {};
        window.addEventListener = jest.fn((event, callback) => {
            map[event] = callback;
        });

        window.dispatchEvent(new Event('resize'));

        expect(LineChart.prototype.resize).toHaveBeenCalledTimes(1);
    });

    it('Should remove resize event listener', () => {
        window.removeEventListener = jest.fn();
        wrapper.unmount();

        expect(window.removeEventListener).toHaveBeenCalledWith('resize', expect.any(Function));
    });

    it('Should have initialized and drawn chart on mount', () => {
        mount(
            <WebChart
                id={id}
                type={cardDisplaySubTypes.lineChart.value}
            />
        );
        expect(LineChart.prototype.initChart).toHaveBeenCalledTimes(1);
        expect(LineChart.prototype.drawChart).toHaveBeenCalledTimes(1);
    });

    it('Should have initialized and drawn chart on mount', () => {
        mount(
            <WebChart
                id={id}
                type={cardDisplaySubTypes.lineChart.value}
            />
        );
        expect(LineChart.prototype.initChart).toHaveBeenCalledTimes(1);
        expect(LineChart.prototype.drawChart).toHaveBeenCalledTimes(1);
    });

    it('Should create chart instance by type', () => {
        [{
            type: cardDisplaySubTypes.lineChart.value,
            class: LineChart,
        }, {
            type: cardDisplaySubTypes.barChart.value,
            class: BarChart,
        }].forEach(typeToClass => {
            mount(
                <WebChart
                    id={id}
                    type={typeToClass.type}
                />
            );

            expect(typeToClass.class).toHaveBeenCalledTimes(1);
            expect(typeToClass.class).toHaveBeenCalledWith(id, { left: 40, right: 20, top: 20, bottom: 30 });
        });
    });
});
