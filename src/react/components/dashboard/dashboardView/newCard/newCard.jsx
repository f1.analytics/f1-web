import React from 'react';
import PropTypes from 'prop-types';
import PlusIcon from '-!svg-react-loader!../../../../assets/images/plus.svg';
import './newCard.scss';

const NewCard = ({ onClick }) => (
    <div id='new-card' className='card' onClick={onClick}>
        <PlusIcon/>
    </div>
);

NewCard.propTypes = {
    onClick: PropTypes.func.isRequired,
};

export default NewCard;
