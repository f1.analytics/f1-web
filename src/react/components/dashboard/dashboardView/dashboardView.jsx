import React from 'react';
import PropTypes from 'prop-types';
import Message from '../../common/message';
import Card from './card';
import NewCard from './newCard';
import ComponentErrorBoundary from '../../common/componentErrorBoundary';
import './dashboardView.scss';
import { leanDataCardTypePropType } from '../../../constants';

const DashboardView = ({ dataCards, subject, onCardDelete, openCardSelection, userData, dataCardTypes }) => (
    <div id='dashboard-view'>
        <Message
            hidden={Boolean(subject)}
            header='Select Subject'
            text={'Select subject to view dashboard.'}
        />
        {subject && dataCards.map(card => (
            <ComponentErrorBoundary key={card.id} >
                <Card
                    card={card}
                    subject={subject}
                    onDelete={onCardDelete}
                    userData={userData}
                    dataCardType={dataCardTypes.find(({ id }) => card.dataCardTypeId === id)}
                />
            </ComponentErrorBoundary>
        ))}
        {(subject && userData) && <NewCard onClick={openCardSelection}/>}
    </div>
);

DashboardView.propTypes = {
    dataCards: PropTypes.array.isRequired,
    onCardDelete: PropTypes.func.isRequired,
    openCardSelection: PropTypes.func.isRequired,
    dataCardTypes: PropTypes.arrayOf(leanDataCardTypePropType).isRequired,
    subject: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    userData: PropTypes.object,
};

export default DashboardView;
