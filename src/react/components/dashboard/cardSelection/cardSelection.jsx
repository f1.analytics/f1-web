import React, { useState, useEffect } from 'react';
import { useDidUpdateEffect } from '../../../utility/hooks';
import PropTypes from 'prop-types';
import WebModal from '../../common/webModal';
import { Button, Dropdown } from 'semantic-ui-react';
import { richDashboardPropType } from '../../../constants';
import './cardSelection.scss';

const CardSelection = ({
  dashboard,
  createCard,
  creatingCard,
  createCardError,
  onClose,
}) => {
    const [selectedCardType, setSelectedCardType] = useState(null);
    const [cardOptions, setCardOptions] = useState([]);

    const handleSave = () => {
        const dataCardType = dashboard.dashboardType.dataCardTypes.find(({ id }) => id === selectedCardType);
        createCard({
            dataCardTypeId: dataCardType.id,
            dashboardId: dashboard.id,
        });
    };

    const handleCardSelect = (e, { value }) => {
        setSelectedCardType(value);
    };

    function getCardOptions(dataCardTypes) {
        return dataCardTypes.sort((a, b) => {
            if (a.name < b.name) {
                return -1;
            }
            if (a.name > b.name) {
                return 1;
            }
            return 0;
        }).map(card => ({
            key: card.id,
            value: card.id,
            text: card.name,
        }));
    }

    useDidUpdateEffect(() => {
        if (!creatingCard && !createCardError) {
            onClose();
        }
    }, [creatingCard]);

    useEffect(() => {
        setCardOptions(getCardOptions(dashboard.dashboardType.dataCardTypes));
    }, []);

    return (
      <WebModal id='card-selection' title='Add Data Card' onClose={onClose}>
          <Dropdown
            id='filter-type-dropdown'
            className='web-dropdown'
            placeholder='Select card'
            value={selectedCardType}
            onChange={handleCardSelect}
            options={cardOptions}
            upward={false}
            selection
          />
          <div className='modal-buttons'>
              <Button
                id='cancel-button'
                className='web-button-3'
                onClick={onClose}
              >
                  Cancel
              </Button>
              <Button
                id='save-button'
                className='web-button-3'
                onClick={handleSave}
                loading={creatingCard}
                disabled={!selectedCardType}
              >
                  Save
              </Button>
          </div>
      </WebModal>
    );
};

CardSelection.propTypes = {
    creatingCard: PropTypes.bool.isRequired,
    createCardError: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    createCard: PropTypes.func.isRequired,
    dashboard: richDashboardPropType.isRequired,
};

export default CardSelection;
