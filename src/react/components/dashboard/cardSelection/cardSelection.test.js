import React from 'react';
import CardSelection from './cardSelection.jsx';
import { mount } from 'enzyme';

describe('CardSelection component tests', () => {
  const dashboard = {
    config: {},
    dashboardType: {
      dataCardTypes: [{
        'id': 1,
        'name': 'Driver race wins',
        'dataQuery': [
          {
            'id': 6,
            'variables': [
              {
                'exportAs': 'driverId',
                'exportFrom': 'subject',
              },
              {
                'exportAs': 'queryKey',
                'exportValue': 'position',
              },
              {
                'exportAs': 'queryValues',
                'exportValue': '1',
              },
            ],
          },
        ],
        'variables': [
          {
            'exportAs': 'x',
            'exportFrom': 'dataQuery',
            'exportKey': 'year',
          },
          {
            'exportAs': 'y',
            'exportFrom': 'dataQuery',
            'exportKey': 'resultCount',
          },
          {
            'exportAs': 'yMax',
            'exportFrom': 'dataQuery',
            'exportKey': 'raceCount',
          },
        ],
        'config': {
          'displayType': 'chart',
          'displaySubType': 'barChart',
          'xText': 'Season',
          'yText': 'Wins count',
          'xFormat': 'year',
        },
        'createDate': '2020-04-26T09:42:55.180Z',
      }],
    },
  };
	const createCard = jest.fn();
	const onClose = jest.fn();
    const wrapper = mount(
        <CardSelection
        	dashboard={dashboard}
        	createCard={createCard}
        	creatingCard={false}
        	createCardError={false}
        	onClose={onClose}
        />
    );

    beforeEach(() => {
        jest.resetAllMocks();
    });

    it('Should render without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it.skip('Should render dropdown items', () => {
    	expect(wrapper.find('.dropdown .item').hostNodes().length > 5).toBe(true);
    });

    it('Should render disabled save button', () => {
    	expect(wrapper.find('#save-button.disabled').hostNodes().length).toBe(1);
    });

    it('Should handle card select and save', () => {
    	wrapper.find('.dropdown .item').hostNodes().first().simulate('click');
    	wrapper.find('#save-button').hostNodes().simulate('click');

    	expect(createCard).toHaveBeenCalledTimes(1);
    });

    it('Should close on card creation', () => {
    	wrapper.setProps({ creatingCard: true });
    	wrapper.setProps({ creatingCard: false });

    	expect(onClose).toHaveBeenCalledTimes(1);
    });
});
