import React from 'react';
import PropTypes from 'prop-types';
import SettingsIcon from '-!svg-react-loader!../../../../assets/images/settings.svg';
import { Link } from 'react-router-dom';
import { menuOptions, richDashboardPropType } from '../../../../constants';
import './dashboardDetails.scss';

const DashboardDetails = ({ dashboard, openDashboardSettings }) => (
	<div id='dashboard-details'>
		<div id='dashboard-name'>
			<div className='title'>{dashboard.name}</div>
			<div className='type'>Dashboard type: {dashboard.dashboardType.name}</div>
		</div>
		<div id='dashboard-control-buttons'>
			<div id='settings-button' onClick={openDashboardSettings}>
				<SettingsIcon/>
			</div>
			<div id='go-back-button'>
				<Link to={menuOptions.DASHBOARD.path}>
					<button className='web-button-3'>Go Back</button>
				</Link>
			</div>
		</div>
	</div>
);

DashboardDetails.propTypes = {
	dashboard: richDashboardPropType.isRequired,
	openDashboardSettings: PropTypes.func.isRequired,
};

export default DashboardDetails;
