import React from 'react';
import PropTypes from 'prop-types';

import DashboardDetails from './dashboardDetails';
import SubjectSelection from './subjectSelection';
import { richDashboardPropType } from '../../../constants';
import './dashboardControl.scss';

const DashboardControl = ({ dashboard, openDashboardSettings }) => (
	<div id='dashboard-control'>
		<DashboardDetails dashboard={dashboard} openDashboardSettings={openDashboardSettings} />
		<SubjectSelection dashboard={dashboard} />
	</div>
);

DashboardControl.propTypes = {
	dashboard: richDashboardPropType.isRequired,
	openDashboardSettings: PropTypes.func.isRequired,
};

export default DashboardControl;
