import React from 'react';
import { mount } from 'enzyme';
import SubjectSelection from './subjectSelection.jsx';
import { dashboardTypes } from '../../../../constants';

describe('SubjectSelection component tests', () => {
    const selection = dashboardTypes.driver.selection;
    const selectionOptions = [{ key: 3, value: 3, text: 'Fernando Alonso' }];
    const filterOptions = [{ key: 2016, value: 2016, text: 2016 }];
    const getFilterOptions = jest.fn();
    const getSelectionOptions = jest.fn();
    const setSelectedSubject = jest.fn();
    const screenSize = { index: 1, width: 2000 };
    const getSubjects = jest.fn();
    const dashboard = {
        config: {},
        dashboardType: {
            subjectSelections: [{
                'id': 1,
                'name': 'Get drivers',
                'config': {
                    'selectionTypePlaceholder': 'All',
                    'subjectSelect': {
                        'placeholder': 'Select driver',
                        'dataQuery': {
                            'id': 3,
                            'variables': [],
                        },
                        'variables': [
                            {
                                'exportAs': 'value',
                                'exportFrom': 'dataQuery',
                                'exportKey': 'id',
                            },
                            {
                                'exportAs': 'text',
                                'exportFrom': 'dataQuery',
                                'exportKey': 'name',
                            },
                        ],
                    },
                },
                'createDate': '2020-04-26T09:42:53.255Z',
            }],
        },
    };

    const wrapper = mount(
        <SubjectSelection
            selection={selection}
            selectionOptions={selectionOptions}
            filterOptions={filterOptions}
            selectionOptionsLoading={false}
            filterOptionsLoading={false}
            getFilterOptions={getFilterOptions}
            getSelectionOptions={getSelectionOptions}
            setSelectedSubject={setSelectedSubject}
            screenSize={screenSize}
            dashboard={dashboard}
            getSubjects={getSubjects}
        />
    );

    it('Should render without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it.skip('Renders subject selection dropdown', () => {
        expect(wrapper.find('#subject-dropdown').hostNodes().length).toBe(1);
        expect(wrapper.find('#subject-dropdown').hostNodes().text()).toContain(selectionOptions[0].text);
    });

    it.skip('Renders filter dropdowns', () => {
        expect(wrapper.find('#filter-type-dropdown').hostNodes().length).toBe(1);
        expect(wrapper.find('#filter-type-dropdown').hostNodes().text()).toContain(selection.filterTypes[0].text);
        expect(wrapper.find('#filter-dropdown').hostNodes().length).toBe(1);
        expect(wrapper.find('#filter-dropdown').hostNodes().text()).toContain(filterOptions[0].text);
    });

    it.skip('Handles filter change', () => {
        const filterType = selection.filterTypes[0];
        const filterOption = filterOptions[0];

        expect(wrapper.find('#filter-type-dropdown').hostNodes().prop('onChange')(null, filterType.value));
        expect(wrapper.find('#filter-type-dropdown div.text').hostNodes().text()).toContain(filterType.text);
        expect(getFilterOptions).toHaveBeenCalledTimes(1);
        expect(getFilterOptions).toHaveBeenCalledWith(filterType.value);

        expect(wrapper.find('#filter-dropdown').hostNodes().prop('onChange')(null, filterOption.value));
        expect(wrapper.find('#filter-dropdown div.text').hostNodes().text()).toContain(filterOption.text);
        expect(getSelectionOptions).toHaveBeenCalledTimes(1);
        expect(getSelectionOptions).toHaveBeenCalledWith(`${filterType.value}=${filterOption.value}`);
    });

    it('Handles subject change', () => {
        expect(wrapper.find('#subject-dropdown').hostNodes().prop('onChange')(null, 4));
        expect(setSelectedSubject).toHaveBeenCalledTimes(1);
        expect(setSelectedSubject).toHaveBeenCalledWith(4);
    });

    it.skip('Does not render filter dropdowns if dashboard type has no filter options', () => {
        const wrapper = mount(
            <SubjectSelection
                selection={dashboardTypes.season.selection}
                selectionOptionsLoading={false}
                filterOptionsLoading={false}
                getFilterOptions={jest.fn()}
                getSelectionOptions={jest.fn()}
                screenSize={screenSize}
                getSubjects={getSubjects}
            />
        );

        expect(wrapper.find('#filter-type-dropdown').hostNodes().length).toBe(0);
        expect(wrapper.find('#filter-dropdown').hostNodes().length).toBe(0);
    });

    it.skip('Renders selected subject', () => {
        const wrapper = mount(
            <SubjectSelection
                selection={dashboardTypes.season.selection}
                selectionOptionsLoading={false}
                filterOptionsLoading={false}
                getFilterOptions={jest.fn()}
                getSelectionOptions={jest.fn()}
                setSelectedSubject={jest.fn()}
                selectionOptions={selectionOptions}
                subject={selectionOptions[0].value}
                screenSize={screenSize}
                getSubjects={getSubjects}
            />
        );

        expect(wrapper.find('#subject-dropdown div.text').hostNodes().text()).toContain(selectionOptions[0].text);
    });
});
