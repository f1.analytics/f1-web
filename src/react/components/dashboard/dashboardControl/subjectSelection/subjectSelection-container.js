import { connect } from 'react-redux';
import SubjectSelection from './subjectSelection.jsx';
import { setSelectedSubject, getSubjects, getFilterOptions } from '../../../../actions';

export const mapStateToProps = state => ({
    subjectsLoading: state.dashboards.subjectsLoading,
    subjects: state.dashboards.subjects,
    filterOptionsLoading: state.dashboards.filterOptionsLoading,
    filterOptions: state.dashboards.filterOptions,
    subject: state.dashboards.subject,
    screenSize: state.ui.screenSize,
});

export const mapDispatchToProps = dispatch => ({
    setSelectedSubject: subject => {
        dispatch(setSelectedSubject(subject));
    },
    getSubjects: (subjectSelectionId, filterValue) => {
        dispatch(getSubjects(subjectSelectionId, filterValue));
    },
    getFilterOptions: (id) => {
        dispatch(getFilterOptions(id));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SubjectSelection);
