import { mapStateToProps, mapDispatchToProps } from './subjectSelection-container.js';
import { getFilterOptions, setSelectedSubject } from '../../../../actions';

describe('Subject selection container tests', () => {
    const filterOptions = [{ value: 4, text: '2019' }];
    const filterOptionsLoading = true;
    const subject = 4;
    const subjects = [subject];
    const screenSize = { width: 200, index: 0 };
    const subjectsLoading = false;
    const state = {
        dashboards: {
            filterOptions,
            filterOptionsLoading,
            subject,
            subjects,
            subjectsLoading,
        },
        ui: {
            screenSize,
        },
    };

    it('Should mapStateToProps', () => {
        expect(mapStateToProps(state)).toEqual({
            filterOptions,
            filterOptionsLoading,
            subject,
            screenSize,
            subjects,
            subjectsLoading,
        });
    });

    describe('mapDispatchToProps tests', () => {
        const filterType = 'season';
        const dispatch = jest.fn();
        const props = mapDispatchToProps(dispatch);

        it('Should map getFilterOptions', () => {
            props.getFilterOptions(filterType);

            expect(dispatch).toHaveBeenCalledWith(getFilterOptions(filterType));
        });

        it('Should map setSelectedSubject', () => {
            props.setSelectedSubject(subject);

            expect(dispatch).toHaveBeenCalledWith(setSelectedSubject(subject));
        });
    });
});
