import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Dropdown } from 'semantic-ui-react';

import { isMobile } from '../../../../utility';
import { richDashboardPropType } from '../../../../constants';
import './subjectSelection.scss';

const SubjectSelection = ({
	dashboard,
	setSelectedSubject,
	subjects,
	subjectsLoading,
	filterOptions,
	filterOptionsLoading,
	subject,
	screenSize,
	getFilterOptions,
	getSubjects,
}) => {
	const getDefaultSubjectSelection = () => {
		if (dashboard.config.defaultSubjectSelection) {
			const foundSubjectSelection = dashboard.dashboardType.subjectSelections.find(({ id }) =>
				id === dashboard.config.defaultSubjectSelection.subjectSelectionId,
			);
			if (foundSubjectSelection) {
				return foundSubjectSelection;
			}
		}
		return dashboard.dashboardType.subjectSelections[0];
	};

	const getDefaultFilterValue = () =>
		dashboard.config.defaultSubjectSelection ? dashboard.config.defaultSubjectSelection.filterValue : null;

	const [subjectSelection, setSubjectSelection] = useState(getDefaultSubjectSelection());
	const [filterValue, setFilterValue] = useState(getDefaultFilterValue());

	const handleSubjectChange = (e, { value }) => {
		setSelectedSubject(value);
	};

	const handleSubjectSelectionChange = (e, { value }) => {
		setSubjectSelection(dashboard.dashboardType.subjectSelections.find(({ id }) => id === value));
		setFilterValue(null);
	};

	const handleFilterChange = (e, { value }) => {
		setFilterValue(value);
	};

	useEffect(() => {
		if (subjectSelection.config.filterSelect) {
			getFilterOptions(subjectSelection.id);
		} else {
			getSubjects(subjectSelection.id);
		}
	}, [subjectSelection]);

	useEffect(() => {
		if (filterValue) {
			getSubjects(subjectSelection.id, filterValue);
		}
	}, [filterValue]);

	return (
		<div id='subject-selection'>
			<div id='selections-container'>
				<div id='selection'>
					<Dropdown
						id='subject-dropdown'
						className='web-dropdown'
						onChange={handleSubjectChange}
						placeholder={subjectSelection && subjectSelection.config.subjectSelect.placeholder}
						options={subjects}
						loading={subjectsLoading}
						value={subject}
						search={!isMobile(screenSize)}
						upward={false}
						selection
					/>
				</div>
				<div id='filter-section'>
					<div id='filter-selection'>
						<Dropdown
							id='filter-type-dropdown'
							className='web-dropdown'
							placeholder='Filter by'
							onChange={handleSubjectSelectionChange}
							options={dashboard.dashboardType.subjectSelections.map((subjectSelection) => ({
								value: subjectSelection.id,
								key: subjectSelection.id,
								text: subjectSelection.config.selectionTypePlaceholder,
							}))}
							value={subjectSelection && subjectSelection.id}
							search={!isMobile(screenSize)}
							upward={false}
							selection
						/>
						<Dropdown
							id='filter-dropdown'
							className='web-dropdown'
							placeholder={subjectSelection && subjectSelection.config.filterSelect && subjectSelection.config.filterSelect.placeholder}
							onChange={handleFilterChange}
							value={filterValue}
							options={filterOptions && filterOptions.map(filterOption => ({
								value: filterOption.value,
								text: filterOption.text,
								key: filterOption.value,
							}))}
							disabled={!filterOptions || !subjectSelection || !subjectSelection.config.filterSelect}
							loading={filterOptionsLoading}
							search={!isMobile(screenSize)}
							upward={false}
							selection
						/>
					</div>
				</div>
			</div>
		</div>
	);
};

SubjectSelection.propTypes = {
	dashboard: richDashboardPropType,
	subjectsLoading: PropTypes.bool.isRequired,
	filterOptionsLoading: PropTypes.bool.isRequired,
	setSelectedSubject: PropTypes.func.isRequired,
	screenSize: PropTypes.object.isRequired,
	getFilterOptions: PropTypes.func.isRequired,
	getSubjects: PropTypes.func.isRequired,
	subjects: PropTypes.arrayOf(PropTypes.shape({
		value: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.number,
		]).isRequired,
		text: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.number,
		]).isRequired,
	})),
	filterOptions: PropTypes.arrayOf(PropTypes.shape({
		value: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.number,
		]),
		text: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.number,
		]).isRequired,
	})),
	subject: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	]),
};

export default SubjectSelection;
