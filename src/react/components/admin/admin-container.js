import { connect } from 'react-redux';

import Admin from './admin.jsx';

export const mapStateToProps = () => ({});

export const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Admin);
