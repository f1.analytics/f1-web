import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Table } from 'semantic-ui-react';

import { leanDashboardTypePropType } from '../../../constants';

const DashboardTypeList = ({
  dashboardTypes,
  getDashboardTypes,
  onRowClick,
}) => {
  useEffect(() => {
    getDashboardTypes();
  }, []);

  const handleRowClick = ({ id }) => {
    onRowClick({ id });
  };

  const handleNewClick = () => {
    onRowClick({
      dashboardType: {
        name: 'New dashboard type',
      },
    });
  };

  return (
    <div className='admin-list' id='dashboard-type-list'>
      {dashboardTypes && (
        <>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {dashboardTypes.map((dashboardType) => (
                <Table.Row key={dashboardType.id} onClick={() => handleRowClick(dashboardType)}>
                  <Table.Cell>
                    {dashboardType.name}
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
          <Button className='web-button add' onClick={handleNewClick}>
            <Icon name='plus'/>
          </Button>
        </>
      )}
    </div>
  );
};

DashboardTypeList.propTypes = {
  getDashboardTypes: PropTypes.func.isRequired,
  onRowClick: PropTypes.func.isRequired,
  dashboardTypes: PropTypes.arrayOf(leanDashboardTypePropType),
};

export default DashboardTypeList;
