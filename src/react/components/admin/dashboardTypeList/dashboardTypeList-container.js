import { connect } from 'react-redux';

import DashboardTypeList from './dashboardTypeList.jsx';
import { getDashboardTypes } from '../../../actions';

export const mapStateToProps = state => ({
  dashboardTypes: state.admin.dashboardTypes,
});

export const mapDispatchToProps = dispatch => ({
  getDashboardTypes: () => {
    dispatch(getDashboardTypes());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashboardTypeList);
