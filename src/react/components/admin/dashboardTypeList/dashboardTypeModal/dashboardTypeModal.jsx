import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'semantic-ui-react';

import WebModal from '../../../common/webModal';
import { richDashboardTypePropType } from '../../../../constants';
import { useDidUpdateEffect } from '../../../../utility/hooks';

const DashboardTypeModal = ({
  onClose,
  getDashboardType,
  clearDashboardType,
  getDashboardTypes,
  id,
  dashboardType,
  saveDashboardType,
  deleteDashboardType,
  saveDashboardTypeLoading,
  deleteDashboardTypeLoading,
}) => {
  const [newDashboardType, setNewDashboardType] = useState(null);

  const handleNameChange = (e, { value }) => {
    setNewDashboardType({
      ...newDashboardType,
      name: value,
    });
  };

  const handleClose = () => {
    clearDashboardType();
    onClose();
  };

  useEffect(() => {
    if (id) {
      getDashboardType(id);
    }
  }, []);

  useEffect(() => {
    setNewDashboardType(dashboardType);
  }, [dashboardType]);

  useDidUpdateEffect(() => {
    if (!saveDashboardTypeLoading && !deleteDashboardTypeLoading) {
      getDashboardTypes();
      handleClose();
    }
  }, [saveDashboardTypeLoading, deleteDashboardTypeLoading]);

  if (!newDashboardType) {
    return null;
  }

  return (
    <WebModal title='Dashboard type' onClose={handleClose}>
      <div id='dashboard-type-modal-content'>
        <div className='modal-field' id='subject-selection-config'>
          <div className='modal-field-header'>Edit dashboard type name</div>
          <div className='modal-field-content'>
            <div className='modal-input'>
              <Input
                onChange={handleNameChange}
                value={newDashboardType.name}
                className='web-input web-input-2'
              />
            </div>
          </div>
          {id && (
            <>
              <div className='modal-field-header'>Delete dashboard type</div>
              <div className='modal-field-content'>
                <Button
                  id='delete-button'
                  className='web-input web-button-3 warning'
                  onClick={() => deleteDashboardType(id)}
                  loading={deleteDashboardTypeLoading}
                >
                  Delete
                </Button>
              </div>
            </>
          )}
          <div className='modal-buttons'>
            <Button
              id='cancel-button'
              className='web-button-3'
              onClick={handleClose}
            >
              Cancel
            </Button>
            <Button
              id='save-button'
              className='web-button-3'
              onClick={() => saveDashboardType({ ...newDashboardType, subjectSelections: undefined })}
              loading={saveDashboardTypeLoading}
            >
              Save
            </Button>
          </div>
        </div>
      </div>
    </WebModal>
  );
};

DashboardTypeModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  getDashboardType: PropTypes.func.isRequired,
  clearDashboardType: PropTypes.func.isRequired,
  getDashboardTypes: PropTypes.func.isRequired,
  saveDashboardType: PropTypes.func.isRequired,
  deleteDashboardType: PropTypes.func.isRequired,
  saveDashboardTypeLoading: PropTypes.bool.isRequired,
  deleteDashboardTypeLoading: PropTypes.bool.isRequired,
  id: PropTypes.number,
  dashboardType: richDashboardTypePropType,
};

export default DashboardTypeModal;
