import { connect } from 'react-redux';
import {
  deleteDashboardType,
  getDashboardType,
  getDashboardTypes,
  saveDashboardType,
  setDashboardType,
} from '../../../../redux/admin/admin-actions';
import DashboardTypeModal from './dashboardTypeModal';

const mapStateToProps = (state, ownProps) => ({
  dashboardType: state.admin.dashboardType || ownProps.dashboardType,
  saveDashboardTypeLoading: state.admin.saveDashboardTypeLoading,
  deleteDashboardTypeLoading: state.admin.deleteDashboardTypeLoading,
});

const mapDispatchToProps = (dispatch) => ({
  getDashboardType: (id) => {
    dispatch(getDashboardType(id));
  },
  clearDashboardType: () => {
    dispatch(setDashboardType(null));
  },
  getDashboardTypes: () => {
    dispatch(getDashboardTypes());
  },
  saveDashboardType: (dashboardType) => {
    dispatch(saveDashboardType(dashboardType));
  },
  deleteDashboardType: (id) => {
    dispatch(deleteDashboardType(id));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DashboardTypeModal);
