import React, { useState } from 'react';

import WebPage from '../common/webPage';
import DashboardTypeList from './dashboardTypeList';
import DataQueryList from './dataQueryList';
import DashboardTypeModal from './dashboardTypeList/dashboardTypeModal';
import SubjectSelectionList from './subjectSelectionList/subjectSelectionList-container';
import SubjectSelectionModal from './subjectSelectionList/subjectSelectionModal';
import './admin.scss';

const selectableViews = {
  dashboardTypeList: {
    value: 'dashboardTypeList',
    text: 'DASHBOARD TYPES',
    view: DashboardTypeList,
    modal: DashboardTypeModal,
  },
  dataQueryList: {
    value: 'dataQueryList',
    text: 'DATA QUERIES',
    view: DataQueryList,
    modal: DashboardTypeModal,
  },
  subjectSelectionList: {
    value: 'subjectSelectionList',
    text: 'SUBJECT SELECTIONS',
    view: SubjectSelectionList,
    modal: SubjectSelectionModal,
  },
};

const Admin = () => {
  const [ selectedView, setSelectedView ] = useState(selectableViews.dashboardTypeList.value);
  const [ modalProps, setModalProps ] = useState(null);

  const isViewActive = (view) => selectedView === view.value;
  const buttonClassName = (view) => `web-button${isViewActive(view) ? ' selected' : ''}`;

  const selectedViewObj = selectableViews[selectedView];
  const ActiveView = selectedViewObj.view;
  const ActiveModal = selectedViewObj.modal;

  return (
    <WebPage id='admin-page'>
      <div id='admin-navigation'>
        {Object.keys(selectableViews).map((viewKey) => (
          <button
            key={viewKey}
            className={buttonClassName(selectableViews[viewKey])}
            onClick={() => setSelectedView(selectableViews[viewKey].value)}
          >
            {selectableViews[viewKey].text}
          </button>
        ))}
      </div>
      <ActiveView onRowClick={(modalProps) => setModalProps(modalProps)} />
      {modalProps && <ActiveModal onClose={() => setModalProps(null)} {...modalProps} />}
    </WebPage>
  );
};

export default Admin;
