import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Table, TableBody, TableCell, TableRow } from 'semantic-ui-react';

const SelectTable = ({ data, onSelect, selectedId }) => (
  <Table className='secondary-table'>
    <TableBody>
      {data.map((item) => {
        const selected = item.id === selectedId ? 'selected' : '';
        return (
          <TableRow
            key={item.id}
            className={item.id === selectedId ? 'selected' : ''}
            onClick={() => onSelect(item)}
          >
            <TableCell>
              {item.name}
            </TableCell>
            <TableCell className='icon'>
              <Icon name={selected ? 'minus' : 'plus'}/>
            </TableCell>
          </TableRow>
        );
      })}
    </TableBody>
  </Table>
);

SelectTable.propTypes = ({
  data: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
  })).isRequired,
  onSelect: PropTypes.func.isRequired,
  selectedId: PropTypes.number,
});

export default SelectTable;
