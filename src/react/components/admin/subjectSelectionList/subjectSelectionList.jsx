import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Table } from 'semantic-ui-react';

import { leanSubjectSelectionPropType } from '../../../constants';

const SubjectSelectionList = ({
  subjectSelections,
  getSubjectSelections,
  onRowClick,
}) => {
  useEffect(() => {
    getSubjectSelections();
  }, []);

  const handleRowClick = ({ id }) => {
    onRowClick({ id });
  };

  const handleNewClick = () => {
    onRowClick({
      subjectSelection: {
        name: 'New subject selection',
        config: {
          selectionTypePlaceholder: 'Select by',
          subjectSelect: {
            placeholder: 'Select Drivers',
            dataQuery: {
              variables: [],
            },
          },
        },
      },
    });
  };

  return (
    <div className='admin-list' id='subject-selection-list'>
      {subjectSelections && (
        <>
          <Table celled>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Name</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {subjectSelections.map((subjectSelection) => (
                <Table.Row key={subjectSelection.id} onClick={() => handleRowClick(subjectSelection)}>
                  <Table.Cell>
                    {subjectSelection.name}
                  </Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
          <Button className='web-button add' onClick={handleNewClick}>
            <Icon name='plus'/>
          </Button>
        </>
      )}
    </div>
  );
};

SubjectSelectionList.propTypes = {
  subjectSelections: PropTypes.arrayOf(leanSubjectSelectionPropType),
  getSubjectSelections: PropTypes.func.isRequired,
  onRowClick: PropTypes.func.isRequired,
};

export default SubjectSelectionList;
