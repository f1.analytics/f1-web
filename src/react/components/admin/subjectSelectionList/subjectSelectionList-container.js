import { connect } from 'react-redux';

import SubjectSelectionList from './subjectSelectionList.jsx';
import { getSubjectSelections } from '../../../actions';

export const mapStateToProps = state => ({
  subjectSelections: state.admin.subjectSelections,
});

export const mapDispatchToProps = dispatch => ({
  getSubjectSelections: () => {
    dispatch(getSubjectSelections());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SubjectSelectionList);
