import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Dropdown } from 'semantic-ui-react';

import { leanDataQueryOptionPropType, selectConfigPropType } from '../../../../constants';
import SelectTable from '../../selectTable';
import { getDataQuery, getExampleResolvedData } from '../../../../redux/admin/admin-api';

const SelectConfig = ({ config, onChange, dataQueries, variableTypeOptions }) => {
  const [dataQuery, setDataQuery] = useState(null);
  // eslint-disable-next-line no-unused-vars
  const [exampleResolvedData, setExampleResolvedData] = useState(null);

  const handleChange = (updateConfig) => {
    onChange({
      ...config,
      ...updateConfig,
    });
  };

  useEffect(() => {
    getExampleResolvedData(config.dataQuery.id).then((exampleResolvedData) => {
      setExampleResolvedData(exampleResolvedData);
    });
  }, [config.dataQuery.id]);

  const selectDataQuery = async ({ id }) => {
    const dataQuery = await getDataQuery(id);
    setDataQuery(dataQuery);
    handleChange({
      dataQuery: {
        id: dataQuery.id,
        variables: dataQuery.variables.map((variable) => ({
          exportAs: variable.name,
          exportFrom: variable.exampleValue,
        })),
      },
    });
  };

  const handleVariableChange = (newVariable, i) => {
    config.dataQuery.variables[i] = {
      ...config.dataQuery.variables[i],
      ...newVariable,
    };
    handleChange(config);
  };

  const getVariableType = (variableType) => variableTypeOptions.find((option) => option === variableType);

  return (
    <>
      <div className='modal-field-header-mini' style={{ paddingTop: 0 }}>Placeholder</div>
      <div className='modal-input'>
        <Input
          onChange={(e, { value }) => handleChange({ placeholder: value })}
          value={config.placeholder}
          className='web-input web-input-2'
        />
      </div>
      <div className='modal-field-header-mini'>Select data query</div>
      <div className='modal-field-content scrollable'>
        <SelectTable onSelect={selectDataQuery} data={dataQueries} selectedId={config.dataQuery.id} />
      </div>
      <div className='modal-field-header-mini'>Configure data query variables</div>
      <div className='variables'>
        {config.dataQuery.variables.map((variable, i) => {
          const configVariable = dataQuery.variables.find((configVariable) => configVariable.name === variable.exportAs);
          if (!configVariable) return null;
          return (
            <div className='variable' key={variable.exportAs}>
              <div className='variable-name'>{variable.exportAs}:</div>
              <Dropdown
                className='web-dropdown variable-type'
                options={[...variableTypeOptions, { value: 'static', text: 'Static' }]}
                value={getVariableType(variable.exportFrom) || 'static'}
                onChange={(e, {value}) => handleVariableChange({ exportFrom: value }, i)}
                selection
              />
              {variable.exportFrom !== 'subjectSelect' && configVariable.availableValues ? (
                <Dropdown
                  className='web-dropdown variable-value'
                  options={configVariable.availableValues.map((availableValue) => ({
                    value: availableValue,
                    text: availableValue,
                  }))}
                  value={variable.exportFrom}
                  onChange={(e, { value }) => handleVariableChange({ exportFrom: value })}
                  selection
                />
              ) : (
                <Input
                  className='web-input web-input-2 variable-valuie'
                  value={variable.exportFrom}
                  onChange={(e, { value }) => handleVariableChange({ exportFrom: value })}
                />
              )}
            </div>
          );
        })}
      </div>
    </>
  );
};

SelectConfig.propTypes = {
  config: selectConfigPropType.isRequired,
  onChange: PropTypes.func.isRequired,
  variableTypeOptions: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
  })).isRequired,
  dataQueries: PropTypes.arrayOf(leanDataQueryOptionPropType).isRequired,
};

export default SelectConfig;
