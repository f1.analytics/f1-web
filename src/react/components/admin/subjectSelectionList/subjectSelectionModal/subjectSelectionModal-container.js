import { connect } from 'react-redux';
import {
  deleteSubjectSelection,
  getDashboardTypes, getDataQueries,
  getSubjectSelection,
  getSubjectSelections,
  saveSubjectSelection,
  setSubjectSelection,
} from '../../../../redux/admin/admin-actions';
import SubjectSelectionModal from './subjectSelectionModal';

const mapStateToProps = (state, ownProps) => ({
  subjectSelection: state.admin.subjectSelection || ownProps.subjectSelection,
  dashboardTypes: state.admin.dashboardTypes,
  dataQueries: state.admin.dataQueries,
  saveSubjectSelectionLoading: state.admin.saveSubjectSelectionLoading,
  deleteSubjectSelectionLoading: state.admin.deleteSubjectSelectionLoading,
});

const mapDispatchToProps = (dispatch) => ({
  getSubjectSelection: (id) => {
    dispatch(getSubjectSelection(id));
  },
  getSubjectSelections: () => {
    dispatch(getSubjectSelections());
  },
  clearSubjectSelection: () => {
    dispatch(setSubjectSelection(null));
  },
  getDashboardTypes: () => {
    dispatch(getDashboardTypes());
  },
  saveSubjectSelection: (dashboardType) => {
    dispatch(saveSubjectSelection(dashboardType));
  },
  deleteSubjectSelection: (id) => {
    dispatch(deleteSubjectSelection(id));
  },
  getDataQueries: () => {
    dispatch(getDataQueries());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SubjectSelectionModal);
