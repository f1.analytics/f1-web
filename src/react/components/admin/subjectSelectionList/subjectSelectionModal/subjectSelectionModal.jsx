import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'semantic-ui-react';

import WebModal from '../../../common/webModal';
import { leanDataQueryOptionPropType, leanDashboardTypePropType, richSubjectSelectionPropType } from '../../../../constants';
import { useDidUpdateEffect } from '../../../../utility/hooks';
import SelectTable from '../../selectTable';
import SelectConfig from './selectConfig';

const SubjectSelectionModal = ({
  onClose,
  getSubjectSelection,
  clearSubjectSelection,
  getSubjectSelections,
  getDashboardTypes,
  dashboardTypes,
  id,
  subjectSelection,
  saveSubjectSelection,
  deleteSubjectSelection,
  saveSubjectSelectionLoading,
  deleteSubjectSelectionLoading,
  dataQueries,
  getDataQueries,
}) => {
  const [newSubjectSelection, setNewSubjectSelection] = useState(null);

  const handleNameChange = (e, { value }) => {
    setNewSubjectSelection({
      ...newSubjectSelection,
      name: value,
    });
  };

  const handleClose = () => {
    clearSubjectSelection();
    onClose();
  };

  const handelDashboardTypeSelect = (dashboardType) => {
    setNewSubjectSelection({
      ...newSubjectSelection,
      dashboardType,
    });
  };

  const handleConfigChange = (updatedConfig) => {
    setNewSubjectSelection({
      ...newSubjectSelection,
      config: {
        ...newSubjectSelection.config,
        ...updatedConfig,
      },
    });
  };

  const toggleFilterSelect = () => {
    handleConfigChange({ filterSelect: newSubjectSelection.config.filterSelect
        ? undefined
        : {
          placeholder: 'Filter by',
          dataQuery: {
           id: dataQueries[0].id,
            variables: [],
          },
        },
    });
  };

  useEffect(() => {
    getDashboardTypes();
    getDataQueries();
    if (id) {
      getSubjectSelection(id);
    }
  }, []);

  useEffect(() => {
    setNewSubjectSelection(subjectSelection);
  }, [subjectSelection]);

  useEffect(() => {
    if (dashboardTypes && newSubjectSelection && !newSubjectSelection.dashboardType) {
      setNewSubjectSelection({
        ...newSubjectSelection,
        ...dashboardTypes[0],
      });
    }
  }, [dashboardTypes]);


  useDidUpdateEffect(() => {
    if (!saveSubjectSelectionLoading && !deleteSubjectSelectionLoading) {
      getSubjectSelections();
      handleClose();
    }
  }, [saveSubjectSelectionLoading, deleteSubjectSelectionLoading]);

  if (!newSubjectSelection || !dashboardTypes || !dataQueries) {
    return null;
  }

  return (
    <WebModal title='Dashboard type' onClose={handleClose}>
      <div id='dashboard-type-modal-content'>
        <div className='modal-field' id='subject-selection-config'>
          <div className='modal-field-header'>Edit dashboard type name</div>
          <div className='modal-field-content'>
            <div className='modal-input'>
              <Input
                onChange={handleNameChange}
                value={newSubjectSelection.name}
                className='web-input web-input-2'
              />
            </div>
          </div>
          {id && (
            <>
              <div className='modal-field-header'>Delete dashboard type</div>
              <div className='modal-field-content'>
                <Button
                  id='delete-button'
                  className='web-input web-button-3 warning'
                  onClick={() => deleteSubjectSelection(id)}
                  loading={deleteSubjectSelectionLoading}
                >
                  Delete
                </Button>
              </div>
            </>
          )}
          <div className='modal-field-header'>Select dashboard type</div>
          <div className='modal-field-content scrollable'>
            <SelectTable
              data={dashboardTypes}
              selectedId={newSubjectSelection.dashboardType ? newSubjectSelection.dashboardType.id : undefined}
              onSelect={handelDashboardTypeSelect}
            />
          </div>
          <div className='modal-field-header'>Selection type placeholder</div>
          <div className='modal-field-content'>
            <div className='modal-input'>
              <Input
              onChange={(e, { value }) => handleConfigChange({ selectionTypePlaceholder: value })}
              value={newSubjectSelection.config.selectionTypePlaceholder}
              className='web-input web-input-2'
            />
            </div>
          </div>
          <div className='modal-field-header'>Filter selection config</div>
          <button onClick={toggleFilterSelect}>{newSubjectSelection.config.filterSelect ? 'Disable' : 'Enable'}</button>
          {newSubjectSelection.config.filterSelect && (
            <div className='modal-field-content'>
              <SelectConfig
                config={newSubjectSelection.config.filterSelect}
                onChange={(filterSelect) => handleConfigChange({ filterSelect })}
                dataQueries={dataQueries}
                variableTypeOptions={[]}
              />
           </div>
          )}
          <div className='modal-field-header'>Subject selection config</div>
          <div className='modal-field-content'>
            <SelectConfig
              config={newSubjectSelection.config.subjectSelect}
              onChange={(subjectSelect) => handleConfigChange({ subjectSelect })}
              dataQueries={dataQueries}
              variableTypeOptions={[{
                value: 'subject',
                text: 'Subject selection',
              }]}
            />
          </div>
          <div className='modal-buttons'>
            <Button
              id='cancel-button'
              className='web-button-3'
              onClick={handleClose}
            >
              Cancel
            </Button>
            <Button
              id='save-button'
              className='web-button-3'
              onClick={() => saveSubjectSelection({ ...newSubjectSelection, subjectSelections: undefined })}
              loading={saveSubjectSelectionLoading}
            >
              Save
            </Button>
          </div>
        </div>
      </div>
    </WebModal>
  );
};

SubjectSelectionModal.propTypes = {
  onClose: PropTypes.func.isRequired,
  getSubjectSelection: PropTypes.func.isRequired,
  clearSubjectSelection: PropTypes.func.isRequired,
  getSubjectSelections: PropTypes.func.isRequired,
  getDashboardTypes: PropTypes.func.isRequired,
  getDataQueries: PropTypes.func.isRequired,
  saveSubjectSelection: PropTypes.func.isRequired,
  deleteSubjectSelection: PropTypes.func.isRequired,
  saveSubjectSelectionLoading: PropTypes.bool.isRequired,
  deleteSubjectSelectionLoading: PropTypes.bool.isRequired,
  dashboardTypes: PropTypes.arrayOf(leanDashboardTypePropType),
  dataQueries: PropTypes.arrayOf(leanDataQueryOptionPropType),
  id: PropTypes.number,
  subjectSelection: richSubjectSelectionPropType,
};

export default SubjectSelectionModal;
