import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Table } from 'semantic-ui-react';

const DataQueryList = ({
  dataQueries,
  getDataQueries,
}) => {
  useEffect(() => {
    getDataQueries();
  }, []);

  return (
    <div className='admin-list' id='dashboard-type-list'>
      {dataQueries && (
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {dataQueries.map((dataQuery) => (
              <Table.Row key={dataQuery.id}>
                <Table.Cell>
                  {dataQuery.name}
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      )}
    </div>
  );
};

DataQueryList.propTypes = {
  dataQueries: PropTypes.arrayOf(),
  getDataQueries: PropTypes.func.isRequired,
};

export default DataQueryList;
