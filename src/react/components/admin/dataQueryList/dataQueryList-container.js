import { connect } from 'react-redux';

import DataQueryList from './dataQueryList.jsx';
import { getDataQueries } from '../../../actions';

export const mapStateToProps = state => ({
  dataQueries: state.admin.dataQueries,
});

export const mapDispatchToProps = dispatch => ({
  getDataQueries: () => {
    dispatch(getDataQueries());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DataQueryList);
