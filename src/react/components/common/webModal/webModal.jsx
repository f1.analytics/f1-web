import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Icon } from 'semantic-ui-react';
import './webModal.scss';

const WebModal = props => {
    const wrapperRef = useRef(null);

    function handleEscapeClick(event) {
        if (event.keyCode === 27) {
            props.onClose();
        }
    }

    function handleClickOutside(event) {
        if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
            props.onClose();
        }
    }

    useEffect(() => {
        document.addEventListener('keydown', handleEscapeClick);
        document.addEventListener('mousedown', handleClickOutside);
        return () => {
            document.removeEventListener('keydown', handleEscapeClick);
            document.removeEventListener('mousedown', handleClickOutside);
        };
    }, []);

    return (
        <div id={props.id} className='web-modal'>
            <div ref={wrapperRef} className='web-modal-content'>
                <div className='header'>
                    {props.title && <div className='title'>{props.title}</div>}
                    <div className='close-button' onClick={props.onClose}>
                        <Icon name='close'/>
                    </div>
                </div>
                {props.children}
            </div>
        </div>
    );
};

WebModal.propTypes = {
    children: PropTypes.node.isRequired,
    onClose: PropTypes.func.isRequired,
    title: PropTypes.string,
    id: PropTypes.string,
};

export default WebModal;
