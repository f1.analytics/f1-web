import React from 'react';
import { mount } from 'enzyme';
import WebModal from './webModal.jsx';

describe('Web modal component tests', () => {
    const title = 'test title';
    const onClose = jest.fn();
    const wrapper = mount(
        <WebModal
            title={title}
            onClose={onClose}
        >
            <div id='test'>Test</div>
        </WebModal>
    );

    it('Should render without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should render children', () => {
        expect(wrapper.find('#test').hostNodes().length).toBe(1);
    });

    it('Should render title', () => {
        expect(wrapper.text()).toContain(title);
    });

    it('Should close on close-button click', () => {
        wrapper.find('.close-button').hostNodes().simulate('click');

        expect(onClose).toHaveBeenCalledTimes(1);
    });

    it('Should close on escape click', () => {
        onClose.mockReset();

        var event = new KeyboardEvent('keydown', {'keyCode': 27});
        document.dispatchEvent(event);

        expect(onClose).toHaveBeenCalledTimes(1);
    });
});
