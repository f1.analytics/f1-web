import React from 'react';
import { mount } from 'enzyme';
import WebPage from './webPage.jsx';
import ReactGA from 'react-ga';

jest.mock('react-ga');

describe('WebPage component tests', () => {
	const id = 'test-page';
	const location = { pathname: '/test' };
	const updateIsTop = jest.fn();
	const wrapper = mount(
		<WebPage
			id={id}
			location={location}
			updateIsTop={updateIsTop}
			isTop={true}
		>
			<div id='children-test'/>
		</WebPage>
	);

	it('Should render without crashing', () => {
		expect(wrapper).toBeDefined();
	});

	it('Should render children', () => {
		expect(wrapper.find(`#${id} #children-test`).hostNodes().length).toBe(1);
	});

	it('Should init google analytics', () => {
		expect(ReactGA.initialize).toHaveBeenCalledTimes(1);
		expect(ReactGA.initialize).toHaveBeenCalledWith(expect.any(String));
		expect(ReactGA.pageview).toHaveBeenCalledTimes(1);
		expect(ReactGA.pageview).toHaveBeenCalledWith(location.pathname);
	});
});
