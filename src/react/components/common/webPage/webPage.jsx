import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import ReactGA from 'react-ga';

const WebPage = props => {
	const ref = useRef(null);

	function handleScroll() {
		const isTop = ref.current.scrollTop < 10;
		if (props.isTop !== isTop) {
			props.updateIsTop(isTop);
		}
	}

	function initializeReactGA() {
		if (process.env.NODE_ENV === 'production') {
	    	ReactGA.initialize('UA-146657283-1');
			ReactGA.pageview(props.location.pathname);
		}
	}

	useEffect(() => {
		initializeReactGA();
		props.updateIsTop(true);
	}, []);

	return (
		<div className='page' id={props.id} ref={ref} onScroll={handleScroll}>
			{props.children}
		</div>
	);
};

WebPage.propTypes = {
	children: PropTypes.node.isRequired,
	updateIsTop: PropTypes.func.isRequired,
	isTop: PropTypes.bool.isRequired,
	location: PropTypes.shape({
		pathname: PropTypes.string.isRequired,
	}).isRequired,
	id: PropTypes.string,
};

export default withRouter(WebPage);
