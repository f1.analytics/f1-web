import { connect } from 'react-redux';
import WebPage from './webPage.jsx';
import { updateIsTop } from '../../../actions';

export const mapStateToProps = state => ({
	isTop: state.ui.isTop,
});

export const mapDispatchToProps = dispatch => ({
	updateIsTop: isTop => {
		dispatch(updateIsTop(isTop));
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WebPage);
