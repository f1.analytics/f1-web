import { mapDispatchToProps, mapStateToProps } from './webPage-container.js';
import { updateIsTop } from '../../../actions';

describe('WebPage container tests', () => {
	const isTop = true;
	
	it('Should mapStateToProps', () => {
		const state = {
			ui: {
				isTop,
			},
		};

		expect(mapStateToProps(state)).toEqual({
			isTop,
		});
	});

	describe('mapDispatchToProps tests', () => {
		const dispatch = jest.fn();
		const props = mapDispatchToProps(dispatch);

		it('Should map updateIsTop', () => {
			props.updateIsTop(isTop);

			expect(dispatch).toHaveBeenCalledWith(updateIsTop(isTop));
		});
	});
});
