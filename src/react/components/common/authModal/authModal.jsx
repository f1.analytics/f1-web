import React from 'react';
import PropTypes from 'prop-types';
import './authModal.scss';


const AuthModal = ({ id, children, title }) => (
	<div id={id} className='auth-modal'>
		{title && <div className='title'>{title}</div>}
		{children}
	</div>
);

AuthModal.propTypes = {
	children: PropTypes.node,
	title: PropTypes.string,
	id: PropTypes.string,
};

export default AuthModal;
