import React from 'react';
import { mount } from 'enzyme';
import AuthModal from './index.js';

describe('AuthModal component test', () => {
	const id = 'test-modal';
	const title = 'Title';
	const wrapper = mount(
		<AuthModal title={title} id={id}>
			Hello World
		</AuthModal>
	);

	it('Should render', () => {
		expect(wrapper).toBeDefined();
	});

	it('Should save id', () => {
		expect(wrapper.find(`#${id}`).hostNodes().length).toBe(1);
	});

	it('Should display title', () => {
		expect(wrapper.text()).toContain(title);
	});

	it('Should display children', () => {
		expect(wrapper.text()).toContain('Hello World');
	});
});
