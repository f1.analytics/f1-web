import React from 'react';
import PropTypes from 'prop-types';
import InfoIcon from '-!svg-react-loader!../../../assets/images/info.svg';
import './message.scss';

const Message = ({ header, text, hidden, error }) => (
    <div className={`message${hidden ? ' hidden' : ''}${error ? ' error' : ''}`}>
        <div className='message-icon'>
            <InfoIcon/>
        </div>
        <div className='message-text'>
            <div className='message-header'>
                <span>{header}</span>
            </div>
            <div className='message-explanation'>
                <span>{text}</span>
            </div>
        </div>
    </div>
);

Message.propTypes = {
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    hidden: PropTypes.bool,
    error: PropTypes.bool,
};

export default Message;
