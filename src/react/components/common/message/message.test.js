import React from 'react';
import { mount } from 'enzyme';
import Message from './message.jsx';

describe('Message component test', () => {
    const header = 'Header test';
    const text = 'Text example';
    const wrapper = mount(
        <Message
            header={header}
            text={text}
        />
    );

    it('Should render', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should render header', () => {
        expect(wrapper.text()).toContain(header);
    });

    it('Should render text', () => {
        expect(wrapper.text()).toContain(text);
    });

    it('Should render hidden', () => {
        const wrapper = mount(
            <Message
                header={header}
                text={text}
                hidden
            />
        );

        expect(wrapper.find('.message.hidden').hostNodes().length).toBe(1);
    });
});
