import React from 'react';
import PropTypes from 'prop-types';
import { RingLoader } from 'react-spinners';
import './loadingOverlay.scss';

const LoadingOverlay = ({ loading, dark }) => {
    if (!loading) {
        return null;
    }

    return (
        <div className={`loading-overlay${dark ? ' dark' : ''}`}>
            <div className='loader'>
                <RingLoader
                    color={'#ff3838'}
                />
            </div>
        </div>
    );
};

LoadingOverlay.defaultProps = {
    loading: true,
};

LoadingOverlay.propTypes = {
    loading: PropTypes.bool,
    dark: PropTypes.bool,
};

export default LoadingOverlay;
