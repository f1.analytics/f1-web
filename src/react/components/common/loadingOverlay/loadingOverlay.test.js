import React from 'react';
import { mount } from 'enzyme';
import LoadingOverlay from './loadingOverlay.jsx';

describe('LoadingOverlay component test', () => {
    const wrapper = mount(
        <LoadingOverlay/>
    );

    it('Should render', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should show loading overlay', () => {
        expect(wrapper.find('.loading-overlay').hostNodes().length).toBe(1);
    });

    it('Should not show loading overlay if loading is false', () => {
        const wrapper = mount(
            <LoadingOverlay loading={false}/>
        );

        expect(wrapper.find('.loading-overlay').length).toBe(0);
    });
});
