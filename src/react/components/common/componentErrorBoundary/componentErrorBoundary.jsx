import React from 'react';
import PropTypes from 'prop-types';
import { logError, toastError } from '../../../utility';
import { errors } from '../../../constants';

class PageErrorBoundary extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
        };
    }

    componentDidCatch(error, errorInfo) {
        this.setState({ error });
        logError(error, errorInfo);
        toastError(errors.INTERNAL_SERVER_ERROR);
    }

    render() {
        const { children } = this.props;
        const { error } = this.state;

        if (error) {
            return null;
        }

        return children;
    }
}

PageErrorBoundary.propTypes = {
    children: PropTypes.node.isRequired,
};

export default PageErrorBoundary;
