import React from 'react';
import ComponentErrorBoundary from './componentErrorBoundary';
import AuthModal from '../authModal';
import { mount } from 'enzyme';
import { toastError, logError } from '../../../utility';
import { errors } from '../../../constants';

jest.mock('../../../utility');

describe('Component error boundary tests', () => {
    const wrapper = mount(
        <ComponentErrorBoundary>
            <AuthModal/>
        </ComponentErrorBoundary>
    );

    it('Should render without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should render children', () => {
        expect(wrapper.find('.auth-modal').hostNodes().length).toBe(1);
    });

    it('Should handle children error', () => {
        wrapper.find(AuthModal).simulateError(new Error('test'));

        expect(toastError).toHaveBeenCalledWith(errors.INTERNAL_SERVER_ERROR);
        expect(logError).toHaveBeenCalledTimes(1);
        expect(wrapper.find('.auth-modal').hostNodes().length).toBe(0);
    });
});
