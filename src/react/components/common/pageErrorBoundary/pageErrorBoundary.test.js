import React from 'react';
import PageErrorBoundary from './pageErrorBoundary';
import AuthModal from '../authModal';
import { mount } from 'enzyme';
import { logError } from '../../../utility';

jest.mock('../../../utility');

describe('Page error boundary tests', () => {
    const wrapper = mount(
        <PageErrorBoundary>
            <AuthModal/>
        </PageErrorBoundary>
    );

    it('Should render without crashing', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should render children', () => {
        expect(wrapper.find('.auth-modal').hostNodes().length).toBe(1);
    });

    it('Should handle children error and display message', () => {
        wrapper.find(AuthModal).simulateError(new Error('test'));

        expect(logError).toHaveBeenCalledTimes(1);
        expect(wrapper.find('.auth-modal').hostNodes().length).toBe(0);
        expect(wrapper.find('.message').hostNodes().length).toBe(1);
    });
});
