import React from 'react';
import PropTypes from 'prop-types';
import Message from '../message';
import './pageErrorBoundary.scss';
import { logError } from '../../../utility';

class PageErrorBoundary extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            error: null,
        };
    }

    componentDidCatch(error, errorInfo) {
        this.setState({ error });
        logError(error, errorInfo);
    }

    render() {
        const { children } = this.props;
        const { error } = this.state;

        if (error) {
            return (
                <div id='page-error'>
                    <Message header='Oooops!' text='We are sorry! Something went wrong on this page' error/>;
                </div>
            );
        }

        return children;
    }
}

PageErrorBoundary.propTypes = {
    children: PropTypes.node.isRequired,
};

export default PageErrorBoundary;
