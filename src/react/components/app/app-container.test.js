import { mapStateToProps, mapDispatchToProps } from './app-container.js';
import { updateScreenSize } from '../../actions';

describe('App container tests', () => {
	const isSidebarOpen = true;
	const screenSize = { value: 'big' };
	const state = {
		ui: {
			isSidebarOpen,
			screenSize,
		},
		session: {},
	};

	it('Should mapStateToProps', () => {
		expect(mapStateToProps(state)).toEqual({
			isSidebarOpen,
			screenSize,
		});
	});

	describe('mapDispatchToProps tests', () => {
		const dispatch = jest.fn();
		const props = mapDispatchToProps(dispatch);

		it('Should map updateScreenSize', () => {
			props.updateScreenSize(screenSize);

			expect(dispatch).toHaveBeenCalledWith(updateScreenSize(screenSize));
		});
	});
});
