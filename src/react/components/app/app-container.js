import { connect } from 'react-redux';
import App from './app.jsx';
import { updateScreenSize } from '../../actions';

export const mapStateToProps = state => ({
	isSidebarOpen: state.ui.isSidebarOpen,
	screenSize: state.ui.screenSize,
	userData: state.session.userData,
});

export const mapDispatchToProps = dispatch => ({
	updateScreenSize: screenSize => {
		dispatch(updateScreenSize(screenSize));
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);
