import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import Menubar from '../menubar';
import Sidebar from '../sidebar';
import DashboardList from '../dashboardList';
import Dashboard from '../dashboard';
import Login from '../login';
import Register from '../register';
import PageErrorBoundary from '../common/pageErrorBoundary';
import { menuOptions, screenSizes } from '../../constants';
import './app.scss';
import Admin from '../admin';

const App = props => {
	const [activeMenu, setActiveMenu] = useState(null);

	function updateActiveMenu() {
		for (const key in menuOptions) {
			const option = menuOptions[key];
			if (props.location.pathname.endsWith(option.path)) {
				setActiveMenu(option.value);
				return;
			}
		}
	}

	function updateScreenSize() {
		for (const key in screenSizes) {
			const screenSize = screenSizes[key];
			if (window.innerWidth < screenSize.widthLimit) {
				props.updateScreenSize(screenSize);
				return;
			}
		}
	}

	useEffect(() => {
		updateScreenSize();
		window.addEventListener('resize', updateScreenSize);
		return () => window.removeEventListener('resize', updateScreenSize);
	}, []);

	useEffect(() => {
		updateActiveMenu();
	}, [props.location]);

	return (
		<div id='app'>
			<Menubar activeMenu={activeMenu}/>
			<Sidebar activeMenu={activeMenu} isOpen={props.isSidebarOpen}/>
			{props.isSidebarOpen && <div id='overlay'/>}
			<PageErrorBoundary>
				<Switch>
					<Route path={`${menuOptions.DASHBOARD.path}/:dashboardId`} component={Dashboard}/>
					<Route exact path={menuOptions.DASHBOARD.path} component={DashboardList}/>
					{props.userData && props.userData.isSuperuser && <Route path={menuOptions.ADMIN.path} component={Admin}/>}
					<Route path={menuOptions.SIGN_UP.path} component={Register}/>
					<Route path={menuOptions.LOGIN.path} component={Login}/>
					<Route render={() => <Redirect to={menuOptions.DASHBOARD.path}/>}/>
				</Switch>
			</PageErrorBoundary>
			<ToastContainer/>
		</div>
	);
};

App.propTypes = {
	location: PropTypes.object.isRequired,
	isSidebarOpen: PropTypes.bool.isRequired,
	updateScreenSize: PropTypes.func.isRequired,
	screenSize: PropTypes.object,
	userData: PropTypes.object,
};

export default App;
