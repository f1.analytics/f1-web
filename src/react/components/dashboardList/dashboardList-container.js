import { connect } from 'react-redux';
import DashboardList from './dashboardList.jsx';
import { getDashboards, createDashboard, getSettings } from '../../actions';

export const mapStateToProps = state => ({
    dashboards: state.dashboards.dashboards,
    dashboardsLoading: state.dashboards.dashboardsLoading,
    createDashboardLoading: state.dashboards.createDashboardLoading,
    createdDashboardId: state.dashboards.createdDashboardId,
    settingsLoading: state.settings.settingsLoading,
    userData: state.session.userData,
    dashboardTypes: state.settings.settings.dashboardTypes,
});

export const mapDispatchToProps = dispatch => ({
    getDashboards: () => {
        dispatch(getDashboards());
    },
    getSettings: () => {
        dispatch(getSettings());
    },
    createDashboard: dashboard => {
        dispatch(createDashboard(dashboard));
    },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardList);
