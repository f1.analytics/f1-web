import { mapStateToProps, mapDispatchToProps } from './dashboardList-container.js';
import { getDashboards, createDashboard } from '../../actions';

describe('DashboardList container tests', () => {
    const dashboards = [{ type: 'driver' }];

    it('Should mapStateToProps', () => {
        const dashboardsLoading = true;
        const userData = { id: 1 };
        const createDashboardLoading = true;
        const createdDashboardId = 4;
        const dashboardTypes = [];
        const settingsLoading = false;
        const state = {
            dashboards: {
                dashboards,
                dashboardsLoading,
                createDashboardLoading,
                createdDashboardId,
            },
            session: {
                userData,
            },
            settings: {
                settings: {
                    dashboardTypes,
                },
                settingsLoading,
            },
        };

        expect(mapStateToProps(state)).toEqual({
            dashboards,
            dashboardsLoading,
            createDashboardLoading,
            createdDashboardId,
            userData,
            dashboardTypes,
            settingsLoading,
        });
    });

    describe('mapDispatchToProps tests', () => {
        const dashboard = { id: 2 };
        const dispatch = jest.fn();
        const props = mapDispatchToProps(dispatch);

        it('Should map getDashboards', () => {
            props.getDashboards();

            expect(dispatch).toHaveBeenCalledWith(getDashboards());
        });

        it('Should map createDashboard', () => {
            props.createDashboard(dashboard);

            expect(dispatch).toHaveBeenCalledWith(createDashboard(dashboard));
        });
    });
});
