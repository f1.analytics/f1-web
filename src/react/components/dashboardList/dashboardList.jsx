import React, { useState, useCallback, useEffect } from 'react';
import PropTypes from 'prop-types';
import NewDashboardSection from './newDashboardSection';
import ExistingDashboardsSection from './existingDashboardsSection';
import LoadingOverlay from '../common/loadingOverlay';
import WebPage from '../common/webPage';
import { withRouter } from 'react-router-dom';
import { toastLoginRequired } from '../../utility';
import { menuOptions } from '../../constants';
import './dashboardList.scss';

const DashboardList = ({
    dashboards,
    dashboardsLoading,
    createDashboard,
    createDashboardLoading,
    settingsLoading,
    userData,
    getDashboards,
    getSettings,
    createdDashboardId,
    history,
    dashboardTypes,
}) => {
    const [newDashboardVisible, setNewDashboardVisible] = useState(false);

    const showNewDashboard = useCallback(() => {
        if (userData) {
            setNewDashboardVisible(true);
        } else {
            toastLoginRequired();
        }
    }, [userData]);

    useEffect(() => {
        getDashboards();
        getSettings();
    }, []);

    useEffect(() => {
        if (createdDashboardId) {
            history.push(`${menuOptions.DASHBOARD.path}/${createdDashboardId}`);
        }
    }, [createdDashboardId]);

    if (dashboardsLoading && settingsLoading) {
        return <LoadingOverlay/>;
    }

    return (
      <WebPage id='dashboard-list-page'>
          <div id='dashboard-list-container'>
              <NewDashboardSection
                newDashboardVisible={newDashboardVisible}
                onShowNewDashboard={showNewDashboard}
                createDashboard={createDashboard}
                loading={createDashboardLoading}
                userData={userData}
                dashboardTypes={dashboardTypes}
              />
              <ExistingDashboardsSection
                newDashboardVisible={newDashboardVisible}
                dashboards={dashboards}
              />
          </div>
      </WebPage>
    );
};

DashboardList.propTypes = {
    dashboardsLoading: PropTypes.bool.isRequired,
    settingsLoading: PropTypes.bool.isRequired,
    createDashboardLoading: PropTypes.bool.isRequired,
    getDashboards: PropTypes.func.isRequired,
    getSettings: PropTypes.func.isRequired,
    createDashboard: PropTypes.func.isRequired,
    createdDashboardId: PropTypes.number,
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired,
    dashboards: PropTypes.array,
    userData: PropTypes.object,
    dashboardTypes: PropTypes.array,
};

export default withRouter(DashboardList);
