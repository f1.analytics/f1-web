import React from 'react';
import PropTypes from 'prop-types';
import ConstructorIcon from '-!svg-react-loader!../../../../../assets/images/constructor.svg';
import DriverIcon from '-!svg-react-loader!../../../../../assets/images/driver.svg';
import SeasonIcon from '-!svg-react-loader!../../../../../assets/images/season.svg';

const iconOptions = {
    helmet: DriverIcon,
    team: ConstructorIcon,
    season: SeasonIcon,
};

const CardIcon = ({ icon }) => {
    const Icon = iconOptions[icon] || iconOptions.driver;

    return (
        <div className='card-icon'>
            <Icon/>
        </div>
    );
};

CardIcon.propTypes = {
    icon: PropTypes.string.isRequired,
};

export default CardIcon;
