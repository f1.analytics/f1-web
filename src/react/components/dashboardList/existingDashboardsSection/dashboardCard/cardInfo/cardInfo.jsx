import React from 'react';
import PropTypes from 'prop-types';
import ChartIcon from '-!svg-react-loader!../../../../../assets/images/chart.svg';
import './cardInfo.scss';

const CardInfo = ({ type, title, dataCardCount }) => (
    <div className='card-info'>
        <div className='dashboard-type'>{type}</div>
        <div className='dashboard-title'>{title}</div>
        <div className='dashboard-details'>
            <div className='card-count'>
                <div className='icon-container'>
                    <ChartIcon/>
                </div>
                <span className='count'><p>{`${dataCardCount} data cards`}</p></span>
            </div>
        </div>
    </div>
);

CardInfo.propTypes = {
  type: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  dataCardCount: PropTypes.number.isRequired,
};

export default CardInfo;
