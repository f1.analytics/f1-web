import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import CardIcon from './cardIcon';
import CardInfo from './cardInfo';
import { leanDashboardPropType, menuOptions } from '../../../../constants';
import './dashboardCard.scss';

const DashboardCard = ({
    dashboard,
    dashboardType,
    style,
    history,
}) => {
    const redirectToDashboard = useCallback(() => {
        history.push(`${menuOptions.DASHBOARD.path}/${dashboard.id}`);
    }, [dashboard]);

    if (!dashboardType) {
        return null;
    }

    return (
      <div className='dashboard-card' style={style} onClick={redirectToDashboard}>
          <div className='card-content'>
              <CardIcon icon={dashboardType.icon}/>
              <CardInfo
                type={dashboardType.name}
                title={dashboard.name}
                dataCardCount={dashboard.dataCardCount}
              />
          </div>
      </div>
    );
};

DashboardCard.propTypes = {
    dashboard: leanDashboardPropType.isRequired,
    dashboardType: leanDashboardPropType.isRequired,
    history: PropTypes.shape({
        push: PropTypes.func.isRequired,
    }).isRequired,
    style: PropTypes.object,
};

export default withRouter(DashboardCard);
