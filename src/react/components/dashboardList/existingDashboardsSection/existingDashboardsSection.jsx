import React from 'react';
import PropTypes from 'prop-types';
import { Trail } from 'react-spring/renderprops';
import { useSelector } from 'react-redux';
import DashboardCard from './dashboardCard';
import './existingDashboardsSection.scss';
import { leanDashboardPropType } from '../../../constants';
import { dashboardTypesSelector } from '../../../redux/settings/settings-selectors';

const ExistingDashboardsSection = ({ newDashboardVisible, dashboards }) => {
  const dashboardTypes = useSelector(dashboardTypesSelector);

  return (
    <div id='existing-dashboards-section' className={newDashboardVisible ? 'moved-down' : undefined}>
      <div className='header'>
        <span>SELECT DASHBOARD</span>
      </div>
      <div id='dashboard-list'>
        {dashboards && (
          (
            <Trail
              key={dashboards.length}
              animated
              items={dashboards}
              keys={dashboard => dashboard.id}
              config={{ mass: 5, tension: 2500, friction: 200 }}
              from={{ opacity: 0 }}
              to={{ opacity: 1 }}
            >
              {dashboard => props =>
                <DashboardCard
                  style={props}
                  dashboard={dashboard}
                  dashboardType={dashboardTypes.find(({ id }) => id === dashboard.dashboardTypeId)}
                />
              }
            </Trail>
          )
        )}
      </div>
    </div>
  );
};

ExistingDashboardsSection.propTypes = {
    newDashboardVisible: PropTypes.bool.isRequired,
    dashboards: PropTypes.arrayOf(leanDashboardPropType),
};

export default ExistingDashboardsSection;
