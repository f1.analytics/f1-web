import React from 'react';
import PropTypes from 'prop-types';
import NewDashboardCard from './newDashboardCard';

const NewDashboardSection = ({
    newDashboardVisible,
    onShowNewDashboard,
    createDashboard,
    loading,
    userData,
    dashboardTypes,
}) => (
    <div id='new-dashboard-section'>
        <div className='header'>
            {!newDashboardVisible ? (
                <button className='web-button' onClick={onShowNewDashboard}>
                    NEW DASHBOARD
                </button>
            ) : (
                <span>NEW DASHBOARD</span>
            )}
        </div>
        <NewDashboardCard
            visible={newDashboardVisible}
            createDashboard={createDashboard}
            loading={loading}
            userData={userData}
            dashboardTypes={dashboardTypes}
        />
    </div>
);

NewDashboardSection.propTypes = {
    newDashboardVisible: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
    onShowNewDashboard: PropTypes.func.isRequired,
    createDashboard: PropTypes.func.isRequired,
    userData: PropTypes.object,
    dashboardTypes: PropTypes.array,
};

export default NewDashboardSection;
