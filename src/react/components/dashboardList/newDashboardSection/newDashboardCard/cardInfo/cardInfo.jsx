import React from 'react';
import PropTypes from 'prop-types';
import { Input, Button } from 'semantic-ui-react';

const CardInfo = ({ typeText, onCreate, loading, onTitleChange, title }) => (
    <div className='card-info'>
        <div className='dashboard-type'>{typeText}</div>
        <div id='title-input'>
            <Input
                onChange={onTitleChange}
                value={title}
                className='web-input web-input-1'
                placeholder='Dashboard title'
            />
        </div>
        <Button
            id='save-dashboard-button'
            className='web-button-2'
            onClick={onCreate}
            loading={loading}
            disabled={!title.length}
        >
            Save
        </Button>
    </div>
);

CardInfo.propTypes = {
    typeText: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    onTitleChange: PropTypes.func.isRequired,
    onCreate: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
};

export default CardInfo;
