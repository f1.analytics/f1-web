import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import CardInfo from './cardInfo';
import CardIcon from './cardIcon';
import './newDashboardCard.scss';

const NewDashboardCard = ({
    visible,
    loading,
    createDashboard,
    dashboardTypes,
    userData,
}) => {
    const [activeTypeIndex, setActiveTypeIndex] = useState(0);
    const [direction, setDirection] = useState('left');
    const [name, setName] = useState('');

    function moveTypeIndexLeft(index) {
        return index === 0 ? dashboardTypes.length - 1 : index - 1;
    }

    function moveTypeIndexRight(index) {
        return index === dashboardTypes.length - 1 ? 0 : index + 1;
    }

    const handleDashboardCreate = useCallback(() => {
        const activeType = dashboardTypes[activeTypeIndex];
        createDashboard({
            dashboardTypeId: activeType.id,
            name,
            userId: (userData && !userData.isSuperuser) ? userData.id : undefined,
        });
    }, [activeTypeIndex, name]);

    const handleTitleChange = useCallback((e, { value }) => {
        setName(value);
    }, []);

    const switchTypeLeft = useCallback(() => {
        setActiveTypeIndex(moveTypeIndexLeft(activeTypeIndex));
        setDirection('left');
    }, [activeTypeIndex]);

    const switchTypeRight = useCallback(() => {
        setActiveTypeIndex(moveTypeIndexRight(activeTypeIndex));
        setDirection('right');
    }, [activeTypeIndex]);

    const activeType = dashboardTypes[activeTypeIndex];

    if (!activeType) {
        return null;
    }

    return (
      <div id='new-dashboard-card' className={`dashboard-card${visible ? ' visible' : ''}`}>
          <div className='card-content'>
              <CardIcon
                icon={activeType.icon}
                direction={direction}
                switchTypeLeft={switchTypeLeft}
                switchTypeRight={switchTypeRight}
              />
              <CardInfo
                title={name}
                typeText={activeType.name}
                onTitleChange={handleTitleChange}
                onCreate={handleDashboardCreate}
                loading={loading}
              />
          </div>
      </div>
    );
};

NewDashboardCard.propTypes = {
    visible: PropTypes.bool.isRequired,
    loading: PropTypes.bool.isRequired,
    createDashboard: PropTypes.func.isRequired,
    userData: PropTypes.object,
    dashboardTypes: PropTypes.array,
};

export default NewDashboardCard;
