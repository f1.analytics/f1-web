import React from 'react';
import { mount } from 'enzyme/build';
import NewDashboardCard from './newDashboardCard.jsx';

describe('NewDashboardCard component test', () => {
    const wrapper = mount(
        <NewDashboardCard visible={true} dashboardTypes={[{
            icon: 'helmet',
            name: 'drivers',
        }, {
            icon: 'team',
            name: 'constructors',
        }]}/>
    );

    it('Should render', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should change type', () => {
        wrapper.find('#right-arrow').hostNodes().simulate('click');
        expect(wrapper.text()).toContain('constructors');

        wrapper.find('#left-arrow').hostNodes().simulate('click');
        expect(wrapper.text()).toContain('drivers');
    });
});
