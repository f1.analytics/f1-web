import React from 'react';
import PropTypes from 'prop-types';
import ConstructorIcon from '-!svg-react-loader!../../../../../assets/images/constructor.svg';
import DriverIcon from '-!svg-react-loader!../../../../../assets/images/driver.svg';
import SeasonIcon from '-!svg-react-loader!../../../../../assets/images/season.svg';
import Arrow from '-!svg-react-loader!../../../../../assets/images/left-arrow.svg';
import { Transition } from 'react-spring/renderprops';
import './cardIcon.scss';

const iconOptions = {
    helmet: DriverIcon,
    team: ConstructorIcon,
    season: SeasonIcon,
};

const getIcon = icon => {
    const Icon = iconOptions[icon] || iconOptions.helmet;

    // eslint-disable-next-line react/display-name
    return props => <Icon style={props}/>;
};

const CardIcon = ({ icon, switchTypeLeft, switchTypeRight, direction }) => (
        <div className='card-icon selection'>
            <Transition
                items={icon}
                from={{ position: 'absolute', transform: `translateX(${direction === 'left' ? '130px' : '-1130px'})`, opacity: -1 }}
                enter={{ transform: 'translateX(0px)', opacity: 1 }}
                leave={{ transform: `translateX(${direction === 'left' ? '-130px' : '130px'})`, opacity: -1 }}
            >
                {toggle => getIcon(toggle)}
            </Transition>
            <div id='left-arrow' onClick={switchTypeLeft}>
                <Arrow/>
            </div>
            <div id='right-arrow' onClick={switchTypeRight}>
                <Arrow/>
            </div>
        </div>
    );

CardIcon.propTypes = {
    icon: PropTypes.string.isRequired,
    direction: PropTypes.oneOf(['left', 'right']),
    switchTypeLeft: PropTypes.func.isRequired,
    switchTypeRight: PropTypes.func.isRequired,
};

export default CardIcon;
