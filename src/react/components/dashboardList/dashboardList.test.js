import React from 'react';
import { mount } from 'enzyme';
import DashboardList from './dashboardList.jsx';
import { dashboardTypes, menuOptions } from '../../constants';
import { toastLoginRequired } from '../../utility';

jest.mock('../../utility', () => ({
    toastLoginRequired: jest.fn(),
}));
jest.mock('../common/webPage', () => ({ children }) => children);

describe.skip('DashboardList component test', () => {
    const createdDashboardId = 4;
    const dashboards = [{
        type: dashboardTypes.driver.value,
        title: 'Driver test',
        dataCardCount: 3,
    }, {
        type: dashboardTypes.constructor.value,
        title: 'Constructor test',
        dataCardCount: 8,
    }, {
        type: dashboardTypes.season.value,
        title: 'Season test',
        dataCardCount: 0,
    }];
    const wrapper = mount(
        <DashboardList
            dashboardsLoading={false}
            dashboards={null}
            getDashboards={jest.fn()}
        />
    );

    it('Should render', () => {
        expect(wrapper).toBeDefined();
    });

    it('Should render without loading overlay', () => {
        expect(wrapper.find('.loading-overlay').length).toBe(0);
    });

    it('Should hide new-dashboard-card', () => {
        expect(wrapper.find('#new-dashboard-card.visible').length).toBe(0);
    });

    it('Should render with loading overlay', () => {
        const wrapper = mount(
            <DashboardList
                dashboardsLoading={true}
                dashboards={null}
                getDashboards={jest.fn()}
            />
        );

        expect(wrapper.find('.loading-overlay').hostNodes().length).toBe(1);
    });

    it('Should fetch dashboards on mount', () => {
        const getDashboards = jest.fn();
        mount(
            <DashboardList
                dashboardsLoading={false}
                dashboards={null}
                getDashboards={getDashboards}
            />
        );

        expect(getDashboards).toHaveBeenCalledTimes(1);
    });

    it('Should map fetched dashboards', () => {
        const wrapper = mount(
            <DashboardList
                dashboardsLoading={false}
                dashboards={dashboards}
                getDashboards={jest.fn()}
            />
        );

        expect(wrapper.find('.dashboard-card:not(#new-dashboard-card)').hostNodes().length).toBe(dashboards.length);
        const firstDashboard = wrapper.find('.dashboard-card:not(#new-dashboard-card)').hostNodes().first();
        expect(firstDashboard.text()).toContain(dashboardTypes.driver.text);
        expect(firstDashboard.text()).toContain(dashboards[0].title);
        expect(firstDashboard.text()).toContain(dashboards[0].dataCardCount);
    });

    it('Should show toast login is required when trying to create dashboard without login', () => {
        const wrapper = mount(
            <DashboardList
                dashboardsLoading={false}
                dashboards={dashboards}
                getDashboards={jest.fn()}
            />
        );

        wrapper.find('.header button').hostNodes().simulate('click');

        expect(wrapper.find('#new-dashboard-card.visible').length).toBe(0);
        expect(toastLoginRequired).toHaveBeenCalledTimes(1);
    });

    it('Should show new-dashboard-card', () => {
        const wrapper = mount(
            <DashboardList
                dashboardsLoading={false}
                dashboards={dashboards}
                getDashboards={jest.fn()}
                userData={{ id: 1 }}
            />
        );

        wrapper.find('.header button').hostNodes().simulate('click');

        expect(wrapper.find('#new-dashboard-card.visible').length).toBe(1);
    });

    it('Should show new-dashboard-card with loading button', () => {
        const wrapper = mount(
            <DashboardList
                dashboardsLoading={false}
                dashboards={dashboards}
                getDashboards={jest.fn()}
                userData={{ id: 1 }}
                createDashboardLoading={true}
            />
        );

        wrapper.find('.header button').hostNodes().simulate('click');

        expect(wrapper.find('#new-dashboard-card.visible .button.loading').length).toBe(1);
    });

    it('Should create on click new-dashboard-card button', () => {
        const createDashboard = jest.fn();
        const title = 'Test title';
        const wrapper = mount(
            <DashboardList
                dashboardsLoading={false}
                dashboards={dashboards}
                getDashboards={jest.fn()}
                userData={{ id: 1 }}
                createDashboard={createDashboard}
            />
        );

        wrapper.find('.header button').hostNodes().simulate('click');
        wrapper.find('#new-dashboard-card.visible .input input').hostNodes().simulate('change', { target: { value: title } });
        wrapper.find('#new-dashboard-card.visible .button').hostNodes().simulate('click');

        expect(createDashboard).toHaveBeenCalledTimes(1);
        expect(createDashboard).toHaveBeenCalledWith({
            type: dashboardTypes.driver.value,
            title,
        });
    });

    it('Should redirect to newly created dashboard', () => {
        const history = { push: jest.fn() };
        const wrapper = mount(
            <DashboardList
                dashboardsLoading={false}
                dashboards={dashboards}
                getDashboards={jest.fn()}
                userData={{ id: 1 }}
                history={history}
                createdDashboardId={null}
            />
        );

        wrapper.setProps({ createdDashboardId });

        expect(history.push).toHaveBeenCalledTimes(1);
        expect(history.push).toHaveBeenCalledWith(`${menuOptions.DASHBOARD.path}/${createdDashboardId}`);
    });
});
