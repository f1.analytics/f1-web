import React from 'react';
import { mount } from 'enzyme';
import Sidebar from './sidebar.jsx';
import { menuOptions } from '../../constants';

describe('Sidebar component tests', () => {
	const userData = { username: 'testUser' };
	const wrapper = mount(
		<Sidebar
			activeMenu='login'
			isOpen={true}
			closeSidebar={jest.fn()}
		/>
	);

	it('Should render without crashing', () => {
		expect(wrapper).toBeDefined();
	});

	it('Should render all sidebar options', () => {
		const text = wrapper.text();
		expect(text).toContain(menuOptions.SIGN_UP.text);
		expect(text).toContain(menuOptions.LOGIN.text);
		expect(text).toContain(menuOptions.DASHBOARD.text);
	});

	it('Should render open', () => {
		const wrapper = mount(
			<Sidebar
				activeMenu='login'
				isOpen={true}
				closeSidebar={jest.fn()}
			/>
		);

		expect(wrapper.find('.open').hostNodes().length).toBe(1);
	});

	it('Should render closed', () => {
		const wrapper = mount(
			<Sidebar
				activeMenu='login'
				isOpen={false}
				closeSidebar={jest.fn()}
			/>
		);

		expect(wrapper.find('.open').hostNodes().length).toBe(0);
	});

	it('Should close sidebar on option click', () => {
		const closeSidebar = jest.fn();
		const wrapper = mount(
			<Sidebar
				activeMenu='login'
				isOpen={true}
				closeSidebar={closeSidebar}
			/>
		);

		wrapper.find('.sidebar-option').hostNodes().at(1).simulate('click');

		expect(closeSidebar).toHaveBeenCalledTimes(1);
	});

	it('Should render sidebar option as selected', () => {
		const closeSidebar = jest.fn();
		const wrapper = mount(
			<Sidebar
				activeMenu={menuOptions.DASHBOARD.value}
				isOpen={true}
				closeSidebar={closeSidebar}
			/>
		);

		expect(wrapper.find('.selected').hostNodes().text()).toContain(menuOptions.DASHBOARD.text);
	});

	it('Should render logout option if user is logged in', () => {
		const wrapper = mount(
			<Sidebar
				activeMenu={menuOptions.DASHBOARD.value}
				isOpen={true}
				closeSidebar={jest.fn()}
				logout={jest.fn()}
				userData={userData}
			/>
		);

		expect(wrapper.find('#logout-option').hostNodes().length).toBe(1);
		expect(wrapper.text()).not.toContain(menuOptions.SIGN_UP.text);
		expect(wrapper.text()).not.toContain(menuOptions.LOGIN.text);
	});

	it('Should handle logout', () => {
		const logout = jest.fn();
		const closeSidebar = jest.fn();
		const wrapper = mount(
			<Sidebar
				activeMenu={menuOptions.DASHBOARD.value}
				isOpen={true}
				closeSidebar={closeSidebar}
				logout={logout}
				userData={userData}
			/>
		);

		wrapper.find('#logout-option').hostNodes().simulate('click');

		expect(logout).toHaveBeenCalledTimes(1);
		expect(closeSidebar).toHaveBeenCalledTimes(1);
	});
});
