import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Icon } from 'semantic-ui-react';
import './sidebarOption.scss';

const SidebarOption = ({ option, activeMenu, onClick, id }) => (
	<Link to={option.path}>
		<div id={id} className={`sidebar-option${activeMenu === option.value ? ' selected' : ''}`} onClick={onClick}>
			<Icon name={option.icon}/>
			<span>{option.text}</span>
		</div>
	</Link>
);

SidebarOption.propTypes = {
	option: PropTypes.shape({
		text: PropTypes.string.isRequired,
		icon: PropTypes.string.isRequired,
		path: PropTypes.string.isRequired,
		value: PropTypes.string.isRequired,
	}).isRequired,
	onClick: PropTypes.func.isRequired,
	activeMenu: PropTypes.string,
	id: PropTypes.string,
};

export default SidebarOption;
