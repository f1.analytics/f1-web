import { connect } from 'react-redux';
import Sidebar from './sidebar.jsx';
import { closeSidebar, clearSession } from '../../actions';

export const mapStateToProps = state => ({
	userData: state.session.userData,
});

export const mapDispatchToProps = dispatch => ({
	closeSidebar: () => {
		dispatch(closeSidebar());
	},
	logout: () => {
		dispatch(clearSession());
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Sidebar);
