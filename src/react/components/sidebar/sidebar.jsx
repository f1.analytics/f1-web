import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import SidebarOption from './sidebarOption';
import { menuOptions } from '../../constants';
import './sidebar.scss';

const Sidebar = ({
	logout,
	closeSidebar,
	userData,
	isOpen,
	activeMenu,
}) => {
	const handleLogout = useCallback(() => {
		logout();
		closeSidebar();
	}, []);

	return (
		<div id='sidebar' className={isOpen ? 'open' : undefined}>
			<div id='sidebar-options'>
				<SidebarOption option={menuOptions.DASHBOARD} activeMenu={activeMenu} onClick={closeSidebar}/>
				{userData && userData.isSuperuser && <SidebarOption option={menuOptions.ADMIN} activeMenu={activeMenu} onClick={closeSidebar}/>}
				{!userData && <SidebarOption option={menuOptions.SIGN_UP} activeMenu={activeMenu} onClick={closeSidebar}/>}
				{!userData && <SidebarOption option={menuOptions.LOGIN} activeMenu={activeMenu} onClick={closeSidebar}/>}
				{userData && <SidebarOption id='logout-option' option={menuOptions.LOGOUT} activeMenu={activeMenu} onClick={handleLogout}/>}
			</div>
		</div>
	);
};

Sidebar.propTypes = {
	isOpen: PropTypes.bool.isRequired,
    closeSidebar: PropTypes.func.isRequired,
	logout: PropTypes.func.isRequired,
	userData: PropTypes.object,
    activeMenu: PropTypes.string,
};

export default Sidebar;
