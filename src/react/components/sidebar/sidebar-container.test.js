import { mapDispatchToProps, mapStateToProps } from './sidebar-container.js';
import { closeSidebar, clearSession } from '../../actions';

describe('Sidebar container tests', () => {
	it('Should mapStateToProps', () => {
		const userData = { username: 'testUser' };
		const state = {
			session: {
				userData,
			},
		};

		expect(mapStateToProps(state)).toEqual({
			userData,
		});
	});

	describe('mapDispatchToProps tests', () => {
		const dispatch = jest.fn();
		const props = mapDispatchToProps(dispatch);

		it('Should map closeSidebar', () => {
			props.closeSidebar();

			expect(dispatch).toHaveBeenCalledWith(closeSidebar());
		});

		it('Should map logout', () => {
			props.logout();

			expect(dispatch).toHaveBeenCalledWith(clearSession());
		});
	});
});
