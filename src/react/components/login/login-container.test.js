import { mapStateToProps, mapDispatchToProps } from './login-container.js';
import { clearSession, login, setWrongCredentialsError } from '../../actions';

describe('Login container tests', () => {
	const userData = { username: 'user1' };
	const wrongCredentialsError = false;

	it('Should mapStateToProps', () => {
		const loginLoading = false;
		const state = {
			session: {
				userData,
			},
			user: {
				loginLoading,
				wrongCredentialsError,
			},
		};

		expect(mapStateToProps(state)).toEqual({
			userData,
			loginLoading,
			wrongCredentialsError,
		});
	});

	describe('mapDispatchToProps tests', () => {
		const email = 'email@gmail.com';
		const password = 'password1';
		const dispatch = jest.fn();
		const props = mapDispatchToProps(dispatch);

		it('Should map clearSession', () => {
			props.clearSession(userData);

			expect(dispatch).toHaveBeenCalledWith(clearSession(userData));
		});

		it('Should map login', () => {
			props.login(email, password);

			expect(dispatch).toHaveBeenCalledWith(login(email, password));
		});

		it('Should map clearWrongCredentialsError', () => {
			props.clearWrongCredentialsError();

			expect(dispatch).toHaveBeenCalledWith(setWrongCredentialsError(false));
		});
	});
});
