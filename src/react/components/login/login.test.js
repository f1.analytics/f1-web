import React from 'react';
import { mount } from 'enzyme';
import Login from './login.jsx';

jest.mock('../common/webPage', () => ({ children }) => children);

describe('Login component tests', () => {
	beforeEach(() => {
		jest.clearAllMocks();
	});

	const email = 'user@gmail.com';
	const password = 'password1234';
	const wrapper = mount(
		<Login
			clearSession={jest.fn()}
			login={jest.fn()}
			loginLoading={false}
			history={{ push: jest.fn() }}
		/>
	);

	it('Should render without crashing', () => {
		expect(wrapper).toBeDefined();
	});

	it('Should render email and password input', () => {
		expect(wrapper.find('input[name="email"]').hostNodes().length).toBe(1);
		expect(wrapper.find('input[name="password"]').hostNodes().length).toBe(1);
	});

	it('Should render submit button', () => {
		expect(wrapper.find('button[type="submit"]').hostNodes().length).toBe(1);
	});

	it('Should have disabled button before inputs change', () => {
		const login = jest.fn();
		const wrapper = mount(
			<Login
				clearSession={jest.fn()}
				login={login}
				loginLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		expect(wrapper.find('button[type="submit"]').hasClass('disabled')).toBe(true);
	});

	it('Should have loading button fetching login action', () => {
		const wrapper = mount(
			<Login
				clearSession={jest.fn()}
				login={jest.fn()}
				loginLoading={true}
				history={{ push: jest.fn() }}
			/>
		);

		expect(wrapper.find('button[type="submit"]').hasClass('loading')).toBe(true);
	});

	it('Should not allow inputs longer than 128 characters', () => {
		const longString = 'x'.repeat(129);

		wrapper.find('input[name="email"]').simulate('change', { target: { value: longString } });
		wrapper.find('input[name="password"]').simulate('change', { target: { value: longString } });

		expect(wrapper.find('input[name="email"]').text()).toBe('');
		expect(wrapper.find('input[name="password"]').text()).toBe('');
	});

	it('Should handle login submition', () => {
		const login = jest.fn();
		const wrapper = mount(
			<Login
				clearSession={jest.fn()}
				login={login}
				loginLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		wrapper.find('input[name="email"]').simulate('change', { target: { value: email } });
		wrapper.find('input[name="password"]').simulate('change', { target: { value: password } });
		wrapper.find('form').simulate('submit');

		expect(login).toHaveBeenCalledTimes(1);
		expect(login).toHaveBeenCalledWith(email, password);
	});

	it('Should clear session on mount', () => {
		const clearSession = jest.fn();
		mount(
			<Login
				clearSession={clearSession}
				login={jest.fn()}
				loginLoading={false}
				history={{ push: jest.fn() }}
			/>
		);

		expect(clearSession).toHaveBeenCalledTimes(1);
	});

	it('Should redirect to homepage on login', () => {
		const push = jest.fn();
		const wrapper = mount(
			<Login
				clearSession={jest.fn()}
				userData={null}
				login={jest.fn()}
				loginLoading={false}
				history={{ push }}
			/>
		);

		wrapper.setProps({ userData: { username: 'user1' } });

		expect(push).toHaveBeenCalledTimes(1);
		expect(push).toHaveBeenCalledWith('/');
	});
});
