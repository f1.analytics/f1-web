import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Form } from 'semantic-ui-react';
import AuthModal from '../common/authModal';
import WebPage from '../common/webPage';
import './login.scss';

const Login = ({
	loginLoading,
	clearSession,
	clearWrongCredentialsError,
 	wrongCredentialsError,
	history,
	userData,
	login,
}) => {
	const [formData, setFormData] = useState({ email: '', password: '' });
	const [error, setError] = useState(false);

	const handleChange = useCallback((e, { name, value }) => {
		if (value.length < 128) {
			setFormData(Object.assign({}, formData, { [name]: value }));
			setError(false);
		}
	}, [formData]);

	const handleSubmit = useCallback(() => {
		const { email, password } = formData;
		login(email, password);
	}, [formData]);

	useEffect(() => {
		if (userData) history.push('/');
	}, [userData]);

	useEffect(() => {
		if (wrongCredentialsError) {
			clearWrongCredentialsError();
			setError(true);
		}
	}, [wrongCredentialsError]);

	useEffect(() => {
		clearSession();
	}, []);

	const { email, password } = formData;

	return (
		<WebPage id='login-page'>
			<AuthModal id='login-modal' title='LOGIN'>
				<Form onSubmit={handleSubmit}>
					<Form.Input
						className='web-input web-input-1'
						placeholder='Your Email'
						name='email'
						onChange={handleChange}
						error={error}
					/>
					<Form.Input
						className='web-input web-input-1'
						type='password'
						placeholder='Password'
						name='password'
						onChange={handleChange}
						error={error}
					/>
					<Form.Button
						className='submit-button'
						type='submit'
						loading={loginLoading}
						disabled={!email || !password}
					>
						Submit
					</Form.Button>
				</Form>
			</AuthModal>
		</WebPage>
	);
};

Login.propTypes = {
	login: PropTypes.func.isRequired,
	clearSession: PropTypes.func.isRequired,
	clearWrongCredentialsError: PropTypes.func.isRequired,
	loginLoading: PropTypes.bool.isRequired,
	wrongCredentialsError: PropTypes.bool.isRequired,
	history: PropTypes.shape({
		push: PropTypes.func.isRequired,
	}),
	userData: PropTypes.object,
};

export default withRouter(Login);
