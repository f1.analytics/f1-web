import { connect } from 'react-redux';
import Login from './login.jsx';
import { clearSession, login, setWrongCredentialsError } from '../../actions';


export const mapStateToProps = state => ({
	userData: state.session.userData,
	loginLoading: state.user.loginLoading,
	wrongCredentialsError: state.user.wrongCredentialsError,
});

export const mapDispatchToProps = dispatch => ({
	clearSession: () => {
		dispatch(clearSession());
	},
	login: (email, password) => {
		dispatch(login(email, password));
	},
	clearWrongCredentialsError: () => {
		dispatch(setWrongCredentialsError(false));
	},
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Login);
