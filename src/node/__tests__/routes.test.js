const request = require('supertest');
const { start } = require('../server');
const { statusCodes } = require('../constants');
const { userAPI, dashboardsAPI } = require('../proxyApi');

describe('Node routes tests', () => {
	const app = start();
	const internalErrorData = {
		code: 'internalError',
		message: 'Internal server error',
	};
	const authorization = 'Bearer accessToken';
	const user = {
		email: 'email@gmail.com',
		password: 'password1234',
		username: 'user1',
		id: '1234',
	};

	afterEach(() => {
		jest.resetAllMocks();
	});

	describe('Middleware tests', () => {
		const requestData = {
			refreshToken: 'refreshToken',
		};

		it('Should resend unauthorized errors', async () => {
			const data = {
				code: 'jwtError',
				message: 'Access token has expired',
			};
			jest.spyOn(userAPI, 'refreshToken').mockImplementation(() => {
				throw {
					response: {
						status: statusCodes.UNAUTHORIZED,
						data,
					},
				};
			});

			const response = await request(app)
				.post('/api/tokens/refresh')
				.set('Authorization', authorization)
				.send(requestData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.UNAUTHORIZED);
			expect(responseMessage).toEqual(data);
		});

		it('Should send internal error if authorization is not set', async () => {
			const response = await request(app)
				.post('/api/tokens/refresh')
				.send(requestData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('POST /tokens tests', () => {
		const loginData = {
			email: user.email,
			password: user.password,
		};

		it('Should get tokens', async () => {
			const data = { accessToken: 'accessToken', refreshToken: 'refreshToken' };
			jest.spyOn(userAPI, 'login').mockImplementation(() => ({ data }));

			const response = await request(app)
				.post('/api/tokens')
				.send(loginData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(userAPI.login).toHaveBeenCalledTimes(1);
			expect(userAPI.login).toHaveBeenCalledWith(loginData.email, loginData.password);
		});

		it('Should resend forbidden error', async () => {
			const data = {
				code: 'wrongCredentials',
				message: 'Wrong credentials',
			};
			jest.spyOn(userAPI, 'login').mockImplementation(() => {
				throw {
					response: {
						status: statusCodes.FORBIDDEN,
						data,
					},
				};
			});

			const response = await request(app)
				.post('/api/tokens')
				.send(loginData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.FORBIDDEN);
			expect(responseMessage).toEqual(data);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(userAPI, 'login').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.post('/api/tokens')
				.send(loginData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('POST /users tests', () => {
		const registerData = {
			email: user.email,
			username: user.username,
			password: user.password,
		};

		it('Should register user', async () => {
			jest.spyOn(userAPI, 'register').mockImplementation(() => {});

			const response = await request(app)
				.post('/api/users')
				.send(registerData);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(userAPI.register).toHaveBeenCalledTimes(1);
			expect(userAPI.register).toHaveBeenCalledWith(registerData.email, registerData.username, registerData.password);
		});

		it('Should resend conflict error', async () => {
			const data = {
				code: 'usernameTaken',
				message: 'username is already taken',
			};
			jest.spyOn(userAPI, 'register').mockImplementation(() => {
				throw {
					response: {
						status: statusCodes.CONFLICT,
						data,
					},
				};
			});

			const response = await request(app)
				.post('/api/users')
				.send(registerData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.CONFLICT);
			expect(responseMessage).toEqual(data);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(userAPI, 'register').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.post('/api/users')
				.send(registerData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('POST /tokens/refresh tests', () => {
		const requestData = {
			refreshToken: 'refreshToken',
		};
		const data = { accessToken: 'accessToken' };

		it('Should get accessToken', async () => {
			jest.spyOn(userAPI, 'refreshToken').mockImplementation(() => ({ data }));

			const response = await request(app)
				.post('/api/tokens/refresh')
				.send(requestData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(userAPI.refreshToken).toHaveBeenCalledTimes(1);
			expect(userAPI.refreshToken).toHaveBeenCalledWith(requestData.refreshToken);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(userAPI, 'refreshToken').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.post('/api/tokens/refresh')
				.send(requestData);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('GET /dashboards', () => {
		const data = { dashboards: [{ type: 'driver' }] };

		it('Should get user dashboards', async () => {
			jest.spyOn(dashboardsAPI, 'getDashboards').mockImplementation(() => ({ data }));

			const response = await request(app)
				.get('/api/dashboards')
				.set('Authorization', authorization);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(dashboardsAPI.getDashboards).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.getDashboards).toHaveBeenCalledWith(authorization);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'getDashboards').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.get('/api/dashboards');
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('GET /dashboards/default', () => {
		const data = { dashboards: [{ type: 'driver' }] };
		it('Should get default dashboards', async () => {
			jest.spyOn(dashboardsAPI, 'getDashboardsDefault').mockImplementation(() => ({ data }));

			const response = await request(app)
				.get('/api/dashboards/default');
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(dashboardsAPI.getDashboardsDefault).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.getDashboardsDefault).toHaveBeenCalledWith();
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'getDashboardsDefault').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.get('/api/dashboards/default');
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('GET /dashboards/:dashboardId', () => {
		const data = { dashboard: { type: 'driver' } };
		const dashboardId = '4';

		it('Should get dashboard', async () => {
			jest.spyOn(dashboardsAPI, 'getDashboard').mockImplementation(() => ({ data }));

			const response = await request(app)
				.get(`/api/dashboards/${dashboardId}`);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(dashboardsAPI.getDashboard).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.getDashboard).toHaveBeenCalledWith(dashboardId);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'getDashboard').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.get(`/api/dashboards/${dashboardId}`);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('GET /dashboards/selectionOptions/:dashboardType', () => {
		const data = { selectionOptions: [{ key: 1, value: 1, text: 'Fernando Alonso' }] };
		const dashboardType = 'driver';
		const query = 'season=2017';

		it('Should get selection options', async () => {
			jest.spyOn(dashboardsAPI, 'getSubjects').mockImplementation(() => ({ data }));

			const response = await request(app)
				.get(`/api/dashboards/selectionOptions/${dashboardType}?${query}`);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(dashboardsAPI.getSubjects).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.getSubjects).toHaveBeenCalledWith(dashboardType, query);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'getSubjects').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.get(`/api/dashboards/selectionOptions/${dashboardType}?${query}`);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe.only('GET /dashboards/subjectSelections/:id/filterOptions', () => {
		const data = { filterOptions: [{ key: 1, value: 1, text: 'Fernando Alonso' }] };
		const subjectSelectionId = '2';

		it('Should get dashboard', async () => {
			jest.spyOn(dashboardsAPI, 'getFilterOptions').mockImplementation(() => ({ data }));

			const response = await request(app)
				.get(`/api/dashboards/subjectSelections/${subjectSelectionId}/filterOptions`);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(dashboardsAPI.getFilterOptions).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.getFilterOptions).toHaveBeenCalledWith(subjectSelectionId);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'getFilterOptions').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.get(`/api/dashboards/subjectSelections/${subjectSelectionId}/filterOptions`);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('GET /dataCards/data/:cardId tests', () => {
		const data = { cardData: { data: [{ y: 2, x: 3 }] }};
		const cardId = '4';
		const query = 'id=3';

		it('Should get card data', async () => {
			jest.spyOn(dashboardsAPI, 'getCardData').mockImplementation(() => ({ data }));

			const response = await request(app)
				.get(`/api/dataCards/data/${cardId}?${query}`);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(dashboardsAPI.getCardData).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.getCardData).toHaveBeenCalledWith(cardId, query);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'getCardData').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.get(`/api/dataCards/data/${cardId}?${query}`);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('PUT /dashboards/ tests', () => {
		const dashboard = { id: 2, type: 'driver' };

		it('Should update dashboard', async () => {
			jest.spyOn(dashboardsAPI, 'updateDashboard').mockImplementation(() => {});

			const response = await request(app)
				.put('/api/dashboards')
				.set('Authorization', authorization)
				.send(dashboard);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(dashboardsAPI.updateDashboard).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.updateDashboard).toHaveBeenCalledWith(authorization, dashboard);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'updateDashboard').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.put('/api/dashboards')
				.send(dashboard);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('POST /dashboards/ tests', () => {
		const data = { dashboardId: 2 };
		const dashboard = { id: 2, type: 'driver' };

		it('Should create dashboard', async () => {
			jest.spyOn(dashboardsAPI, 'createDashboard').mockImplementation(() => ({ data }));

			const response = await request(app)
				.post('/api/dashboards')
				.set('Authorization', authorization)
				.send(dashboard);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(responseMessage).toEqual(data);
			expect(dashboardsAPI.createDashboard).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.createDashboard).toHaveBeenCalledWith(authorization, dashboard);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'createDashboard').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.post('/api/dashboards')
				.set('Authorization', authorization)
				.send(dashboard);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('DELETE /dashboards/ tests', () => {
		const dashboardId = '2';

		it('Should delete dashboard', async () => {
			jest.spyOn(dashboardsAPI, 'deleteDashboard').mockImplementation(() => {});

			const response = await request(app)
				.delete(`/api/dashboards/${dashboardId}`)
				.set('Authorization', authorization);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(dashboardsAPI.deleteDashboard).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.deleteDashboard).toHaveBeenCalledWith(authorization, dashboardId);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'deleteDashboard').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.delete(`/api/dashboards/${dashboardId}`)
				.set('Authorization', authorization);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('DELETE /dataCards/ tests', () => {
		const cardId = '2';
		const data = { dataCardId: 2 };

		it('Should delete card', async () => {
			jest.spyOn(dashboardsAPI, 'deleteCard').mockImplementation(() => ({ data }));

			const response = await request(app)
				.delete(`/api/dataCards/${cardId}`)
				.set('Authorization', authorization);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(dashboardsAPI.deleteCard).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.deleteCard).toHaveBeenCalledWith(authorization, cardId);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'deleteCard').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.delete(`/api/dataCards/${cardId}`)
				.set('Authorization', authorization);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});

	describe('POST /dataCards/ tests', () => {
		const card = { id: 2 };
		const data = { dataCardId: 2 };

		it('Should create card', async () => {
			jest.spyOn(dashboardsAPI, 'createCard').mockImplementation(() => ({ data }));

			const response = await request(app)
				.post('/api/dataCards/')
				.set('Authorization', authorization)
				.send(card);

			expect(response.statusCode).toBe(statusCodes.SUCCESS);
			expect(dashboardsAPI.createCard).toHaveBeenCalledTimes(1);
			expect(dashboardsAPI.createCard).toHaveBeenCalledWith(authorization, card);
		});

		it('Should handle internal error', async () => {
			jest.spyOn(dashboardsAPI, 'createCard').mockImplementation(() => {
				throw new Error();
			});

			const response = await request(app)
				.post('/api/dataCards/')
				.set('Authorization', authorization);
			const responseMessage = JSON.parse(response.text);

			expect(response.statusCode).toBe(statusCodes.INTERNAL_ERROR);
			expect(responseMessage).toEqual(internalErrorData);
		});
	});
});
