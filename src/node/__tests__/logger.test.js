const { errorStackTracerFormat } = require('../logger');

describe('Logger tests', () => {
    describe('Error stack format', () => {
        it('Should include error stack in log message when error level', () => {
            const error = new Error();
            error.stack = 'errorStack';
            const info = {
                message: 'Error message.',
                level: 'error',
                error,
            };

            const returnedInfo = errorStackTracerFormat(info);

            expect(returnedInfo.message).toBe('Error message.\nerrorStack');
        });

        it('Should not include error stack in log message when not error level', () => {
            const error = new Error();
            error.stack = 'errorStack';
            const info = {
                message: 'Error message.',
                level: 'warn',
                error,
            };

            const returnedInfo = errorStackTracerFormat(info);

            expect(returnedInfo.message).toBe('Error message.');
        });

        it('Should not include error stack in log message when error not provided', () => {
            const error = new Error();
            error.stack = 'errorStack';
            const info = {
                message: 'Error message.',
                level: 'warn',
            };

            const returnedInfo = errorStackTracerFormat(info);

            expect(returnedInfo.message).toBe('Error message.');
        });

        it('Should not include error stack in log message when error is not provided', () => {
            const error = new Error();
            error.stack = 'errorStack';
            const info = {
                message: 'Error message.',
                level: 'error',
            };

            const returnedInfo = errorStackTracerFormat(info);

            expect(returnedInfo.message).toBe('Error message.');
        });

        it('Should not include error stack in log message when error is not Error instance', () => {
            const error = {
                stack: 'errorStack',
            };
            const info = {
                message: 'Error message.',
                level: 'error',
                error,
            };

            const returnedInfo = errorStackTracerFormat(info);

            expect(returnedInfo.message).toBe('Error message.');
        });
    });
});
