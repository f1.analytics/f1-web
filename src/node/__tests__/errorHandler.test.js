const ErrorHandler = require('../errorHandler');
const { statusCodes } = require('../constants');
const logger = require('../logger');

jest.mock('../logger', () => ({
	error: jest.fn(),
	warn: jest.fn(),
}));
const send = jest.fn();
const status = jest.fn(() => ({ send }));
const res = { status };

describe('ErrorHandler tests', () => {
	afterEach(() => {
		jest.clearAllMocks();
	});

	it('Should set operational errors', () => {
		const error = new Error();
		error.response = { status: statusCodes.UNAUTHORIZED };

		ErrorHandler.setOperationalErrors(error, [statusCodes.UNAUTHORIZED]);

		expect(error.isOperational).toBe(true);
	});

	it('Should not set operational errors', () => {
		const error = new Error();
		error.response = { status: statusCodes.CONFLICT };

		ErrorHandler.setOperationalErrors(error, []);

		expect(error.isOperational).toBeFalsy();
	});

	describe('Operational response error tests', () => {
		const message = 'Conflict error';
		const operationalError = new Error();
		operationalError.response = {
			status: statusCodes.CONFLICT,
			data: {
				error: {
					code: statusCodes.CONFLICT,
					message,
				},
			},
		};
		operationalError.logMessage = 'Could not create user';
		operationalError.isOperational = true;

		it('Should log warning', () => {
			ErrorHandler.handleError(operationalError, res);

			expect(logger.warn).toHaveBeenCalledTimes(1);
			expect(logger.warn).toHaveBeenCalledWith(`${operationalError.logMessage}: ${message}`);
		});

		it('Should resend response', () => {
			ErrorHandler.handleError(operationalError, res);

			expect(status).toHaveBeenCalledTimes(1);
			expect(status).toHaveBeenCalledWith(operationalError.response.status);
			expect(send).toHaveBeenCalledTimes(1);
			expect(send).toHaveBeenCalledWith(operationalError.response.data);
		});
	});

	describe('Programmer error tests', () => {
		const programmerError = new Error('Type error');
		programmerError.logMessage = 'Could not create user';

		it('Should log response error', () => {
			programmerError.response = {
				status: statusCodes.BAD_REQUEST,
				data: {
					error: {
						code: statusCodes.BAD_REQUEST,
						message: 'bad request error',
					},
				},
			};

			ErrorHandler.handleError(programmerError, res);

			expect(logger.error).toHaveBeenCalledTimes(1);
			expect(logger.error).toHaveBeenCalledWith(`${programmerError.logMessage}: ${programmerError.response.data.error.message}`);
		});

		it('Should log error with stack', () => {
			delete programmerError.response;

			ErrorHandler.handleError(programmerError, res);

			expect(logger.error).toHaveBeenCalledTimes(1);
			expect(logger.error).toHaveBeenCalledWith(`${programmerError.logMessage}: ${programmerError.message}`, { error: programmerError });
		});

		it('Should send internal error response', () => {
			ErrorHandler.handleError(programmerError, res);

			expect(status).toHaveBeenCalledTimes(1);
			expect(status).toHaveBeenCalledWith(statusCodes.INTERNAL_ERROR);
			expect(send).toHaveBeenCalledTimes(1);
			expect(send).toHaveBeenCalledWith({
				code: 'internalError',
				message: 'Internal server error',
			});
		});
	});
});
