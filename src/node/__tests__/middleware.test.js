const { errorHandler, requiredAuthorizationHandler } = require('../middleware');
const ErrorHandler = require('../errorHandler');

describe('Middleware tests', () => {
	it('Should handle errors', () => {
		const res = { status: () => ({ send: () => {} }) };
		const error = new Error('test');
		jest.spyOn(ErrorHandler, 'handleError');

		errorHandler(error, null, res);

		expect(ErrorHandler.handleError).toHaveBeenCalledTimes(1);
		expect(ErrorHandler.handleError).toHaveBeenCalledWith(error, res);
	});

	it('Should save authorization header', () => {
		const req = {
			headers: {
				authorization: 'token',
			},
		};
		const next = jest.fn();

		requiredAuthorizationHandler(req, null, next);

		expect(req.authorization).toBe('token');
		expect(next).toHaveBeenCalledTimes(1);
		expect(next).toHaveBeenCalledWith();
	});

	it('Should send error if authorization header is missing', () => {
		const req = { headers: {} };
		const next = jest.fn();

		requiredAuthorizationHandler(req, null, next);

		expect(req.authorization).toBeFalsy();
		expect(next).toHaveBeenCalledTimes(1);
		expect(next).toHaveBeenCalledWith(new Error('Missing authorization header'));
	});
});
