const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const logger = require('./logger');
const routes = require('./routes');
const { errorHandler } = require('./middleware');
const config = require('../../config.json').server;

const start = () => {
	const app = express();

	app.use(bodyParser.json());
	app.use(express.static(path.resolve(__dirname, '..', '../dist/')));

	app.use('/api', routes());

	app.use(errorHandler);

	app.get('*', (req, res) => {
		res.sendFile(path.resolve(__dirname, '..', '../dist/index.html'));
	});

	app.listen(config.port, config.host, () => {
		logger.info(`Server started on ${config.host}:${config.port}`);
	});

	return app;
};

module.exports = {
	start,
};
