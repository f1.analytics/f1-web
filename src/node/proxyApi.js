const axios = require('axios');
const config = require('../../config.json').api;

module.exports = {
	settingsAPI: {
		getSettings: () => axios.get(`${config.settings}`),
		getDashboardTypes: (Authorization) => axios.get(`${config.settings}/dashboardTypes`, {
			headers: { Authorization },
		}),
		getDashboardType: (Authorization, id) => axios.get(`${config.settings}/dashboardTypes/${id}`, {
			headers: { Authorization },
		}),
		getDataQueries: (Authorization) => axios.get(`${config.settings}/dataQueryOptions`, {
			headers: { Authorization },
		}),
		getSubjectSelections: (Authorization) => axios.get(`${config.settings}/subjectSelections`, {
			headers: { Authorization },
		}),
		updateDashboardType: (Authorization, dashboardType) => axios.put(`${config.settings}/dashboardTypes`, dashboardType, {
			headers: { Authorization },
		}),
		saveDashboardType: (Authorization, dashboardType) => axios.post(`${config.settings}/dashboardTypes`, dashboardType, {
			headers: { Authorization },
		}),
		deleteDashboardType: (Authorization, id) => axios.delete(`${config.settings}/dashboardTypes/${id}`, {
			headers: { Authorization },
		}),
		getSubjectSelection: (Authorization, id) => axios.get(`${config.settings}/subjectSelections/${id}`, {
			headers: { Authorization },
		}),
		updatedSubjectSelection: (Authorization, subjectSelection) => axios.put(`${config.settings}/subjectSelections`, subjectSelection, {
			headers: { Authorization },
		}),
		saveSubjectSelection: (Authorization, subjectSelection) => axios.post(`${config.settings}/subjectSelections`, subjectSelection, {
			headers: { Authorization },
		}),
		deleteSubjectSelection: (Authorization, id) => axios.delete(`${config.settings}/subjectSelections/${id}`, {
			headers: { Authorization },
		}),
		getDataQuery: (Authorization, id) => axios.get(`${config.settings}/dataQueryOptions/${id}`, {
			headers: { Authorization },
		}),
	},
	userAPI: {
		register: (email, username, password) => axios.post(`${config.users}/users`, {
			email,
			username,
			password,
		}),
		refreshToken: refreshToken => axios.post(`${config.users}/tokens/refresh`, {
			refreshToken,
		}),
		login: (email, password) => axios.post(`${config.users}/tokens`, {
			email,
			password,
		}),
		getUser: (Authorization, userId) => axios.get(`${config.users}/users/${userId}`, {
			headers: { Authorization },
		}),
	},
	dashboardsAPI: {
		getDashboards: Authorization => axios.get(`${config.dashboards}/dashboards`, {
			headers: { Authorization: Authorization },
		}),
		updateDashboard: (Authorization, dashboard) => axios.put(`${config.dashboards}/dashboards`, dashboard, {
			headers: { Authorization },
		}),
		createDashboard: (Authorization, dashboard) => axios.post(`${config.dashboards}/dashboards`, dashboard, {
			headers: { Authorization },
		}),
		deleteDashboard: (Authorization, dashboardId) => axios.delete(`${config.dashboards}/dashboards/${dashboardId}`, {
			headers: { Authorization },
		}),
		deleteCard: (Authorization, cardId) => axios.delete(`${config.dashboards}/dataCards/${cardId}`, {
			headers: { Authorization },
		}),
		createCard: (Authorization, card) => axios.post(`${config.dashboards}/dataCards`, card, {
			headers: { Authorization },
		}),
		getDashboardsDefault: () => axios.get(`${config.dashboards}/dashboards`),
		getDashboard: dashboardId => axios.get(`${config.dashboards}/dashboards/${dashboardId}`),
		getSubjects: (id, query) => axios.get(`${config.dashboards}/subjectSelections/${id}/subjects${query ? `?${query}` : ''}`),
		getFilterOptions: id => axios.get(`${config.dashboards}/subjectSelections/${id}/filterOptions`),
		getCardData: (cardId, query) => axios.get(`${config.dashboards}/dataCards/data/${cardId}?${query}`),
	},
	dataApi: {
		getExampleResolvedData: (Authorization, dataQueryId) => axios.get(`${config.data}/example/${dataQueryId}`, {
			headers: { Authorization },
		}),
	},
};
