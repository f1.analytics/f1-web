const statusCodes = {
	SUCCESS: 200,
	UNAUTHORIZED: 401,
	FORBIDDEN: 403,
	CONFLICT: 409,
	INTERNAL_ERROR: 500,
};

module.exports = {
	statusCodes,
};
