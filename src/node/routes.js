const express = require('express');
const url = require('url');
const { settingsAPI, userAPI, dashboardsAPI, dataApi } = require('./proxyApi');
const { statusCodes } = require('./constants');
const { authorizationHandler, requiredAuthorizationHandler } = require('./middleware');
const ErrorHandler = require('./errorHandler');

const routes = () => {
	const router = express.Router();

	router.post('/tokens', async (req, res, next) => {
		const { email, password } = req.body;
		try {
			const { data } = await userAPI.login(email, password);
			res.send(data);
		} catch (error) {
			ErrorHandler.setOperationalErrors(error, [statusCodes.FORBIDDEN]);
			error.logMessage = `Could not create tokens for ${email}`;
			next(error);
		}
	});

	router.post('/tokens/refresh', async (req, res, next) => {
		const { refreshToken } = req.body;
		try {
			const { data } = await userAPI.refreshToken(refreshToken);
			res.send(data);
		} catch (error) {
			error.logMessage = 'Could not refresh token';
			next(error);
		}
	});

	router.post('/users', async (req, res, next) => {
		const { email, username, password } = req.body;
		try {
			await userAPI.register(email, username, password);
			res.send();
		} catch (error) {
			ErrorHandler.setOperationalErrors(error, [statusCodes.CONFLICT]);
			error.logMessage = `Could not create user ${email}`;
			next(error);
		}
	});

	router.get('/dashboards/subjectSelections/:id/subjects', async (req, res, next) => {
		try {
			const query = url.parse(req.url).query;
			const { data } = await dashboardsAPI.getSubjects(req.params.id, query);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could not get dashboard subject for subject selection ${req.params.id}`;
			next(error);
		}
	});

	router.get('/dashboards/subjectSelections/:id/filterOptions', async (req, res, next) => {
		try {
			const { data } = await dashboardsAPI.getFilterOptions(req.params.id);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could not get filter options for subject selection ${req.params.id}`;
			next(error);
		}
	});

	router.get('/dashboards/default', async (req, res, next) => {
		try {
			const { data } = await dashboardsAPI.getDashboardsDefault();
			res.send(data);
		} catch (error) {
			error.logMessage = 'Could not get default dashboards';
			next(error);
		}
	});

	router.get('/dashboards/:dashboardId', async (req, res, next) => {
		try {
			const { data } = await dashboardsAPI.getDashboard(req.params.dashboardId);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could not get ${req.params.dashboardId} dashboards`;
			next(error);
		}
	});

	router.get('/dataCards/data/:cardId', async (req, res, next) => {
		try {
			const query = url.parse(req.url).query;
			const { data } = await dashboardsAPI.getCardData(req.params.cardId, query);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could not get card ${req.params.cardId} data`;
			next(error);
		}
	});

	router.get('/settings', async (req, res, next) => {
		try {
			const { data } = await settingsAPI.getSettings();
			res.send(data);
		} catch (error) {
			error.logMessage = 'Could not get settings';
			next(error);
		}
	});

	router.use(authorizationHandler);

	router.get('/dashboards', async (req, res, next) => {
		try {
			const { data } = await dashboardsAPI.getDashboards(req.authorization);
			res.send(data);
		} catch (error) {
			error.logMessage = 'Could not get user dashboards';
			next(error);
		}
	});

	router.use(requiredAuthorizationHandler);

	router.put('/dashboards/', async (req, res, next) => {
		try {
			await dashboardsAPI.updateDashboard(req.authorization, req.body);
			res.send();
		} catch (error) {
			error.logMessage = `Could not update dashboard ${req.body.id}`;
			next(error);
		}
	});

	router.post('/dashboards/', async (req, res, next) => {
		try {
			const { data } = await dashboardsAPI.createDashboard(req.authorization, req.body);
			res.send(data);
		} catch (error) {
			error.logMessage = 'Could not create dashboard';
			next(error);
		}
	});

	router.delete('/dashboards/:dashboardId', async (req, res, next) => {
		try {
			await dashboardsAPI.deleteDashboard(req.authorization, req.params.dashboardId);
			res.send();
		} catch (error) {
			error.logMessage = `Could not delete dashboard ${req.params.dashboardId}`;
			next(error);
		}
	});

	router.delete('/dataCards/:cardId', async (req, res, next) => {
		try {
			await dashboardsAPI.deleteCard(req.authorization, req.params.cardId);
			res.send();
		} catch (error) {
			error.logMessage = `Could not delete data card ${req.params.cardId}`;
			next(error);
		}
	});

	router.post('/dataCards/', async (req, res, next) => {
		try {
			const { data } = await dashboardsAPI.createCard(req.authorization, req.body);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could not create data card ${req.body.type}`;
			next(error);
		}
	});

	router.get('/users/:id', async (req, res, next) => {
		try {
			const { data } = await userAPI.getUser(req.authorization, req.params.id);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could not get user ${req.params.id}`;
			next(error);
		}
	});

	router.get('/settings/dashboardTypes', async (req, res, next) => {
		try {
			const { data } = await settingsAPI.getDashboardTypes(req.authorization);
			res.send(data);
		} catch (error) {
			error.logMessage = 'Could not get dashboard types';
			next(error);
		}
	});

	router.get('/settings/dashboardTypes/:id', async (req, res, next) => {
		try {
			const { data } = await settingsAPI.getDashboardType(req.authorization, req.params.id);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could not get dashboard type ${req.params.id}`;
			next(error);
		}
	});

	router.get('/settings/dataQueryOptions', async (req, res, next) => {
		try {
			const { data } = await settingsAPI.getDataQueries(req.authorization);
			res.send(data);
		} catch (error) {
			error.logMessage = 'Could not get data query options';
			next(error);
		}
	});

	router.get('/settings/subjectSelections', async (req, res, next) => {
		try {
			const { data } = await settingsAPI.getSubjectSelections(req.authorization);
			res.send(data);
		} catch (error) {
			error.logMessage = 'Could not get subject selections';
			next(error);
		}
	});

	router.put('/settings/dashboardTypes', async (req, res, next) => {
		try {
			if (req.body.id) {
				await settingsAPI.updateDashboardType(req.authorization, req.body);
			} else {
				await settingsAPI.saveDashboardType(req.authorization, req.body);
			}
			res.send();
		} catch (error) {
			error.logMessage = 'Could not save dashboard type';
			next(error);
		}
	});

	router.delete('/settings/dashboardTypes/:id', async (req, res, next) => {
		try {
			await settingsAPI.deleteDashboardType(req.authorization, req.params.id);
			res.send();
		} catch (error) {
			error.logMessage = `Could not delete dashboard type ${req.params.id}`;
			next(error);
		}
	});

	router.get('/settings/subjectSelections/:id', async (req, res, next) => {
		try {
			const { data } = await settingsAPI.getSubjectSelection(req.authorization, req.params.id);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could not get subject selection ${req.params.id}`;
			next(error);
		}
	});

	router.put('/settings/subjectSelections', async (req, res, next) => {
		try {
			if (req.body.id) {
				await settingsAPI.updatedSubjectSelection(req.authorization, req.body);
			} else {
				await settingsAPI.saveSubjectSelection(req.authorization, req.body);
			}
			res.send();
		} catch (error) {
			error.logMessage = 'Could not save subject selection';
			next(error);
		}
	});

	router.delete('/settings/subjectSelections/:id', async (req, res, next) => {
		try {
			await settingsAPI.deleteSubjectSelection(req.authorization, req.params.id);
			res.send();
		} catch (error) {
			error.logMessage = `Could not delete subject selection ${req.params.id}`;
			next(error);
		}
	});

	router.get('/settings/dataQueryOptions/:id', async (req, res, next) => {
		try {
			const { data } = await settingsAPI.getDataQuery(req.authorization, req.params.id);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could get data query ${req.params.id}`;
			next(error);
		}
	});

	router.get('/data/example/:dataQueryId', async (req, res, next) => {
		try {
			const { data } = await dataApi.getExampleResolvedData(req.authorization, req.params.dataQueryId);
			res.send(data);
		} catch (error) {
			error.logMessage = `Could get example data with data query ${req.params.dataQueryId}`;
			next(error);
		}
	});

	return router;
};

module.exports = routes;
