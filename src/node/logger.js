const winston = require('winston');
const config = require('../../config.json').logger;

const errorStackTracerFormat = info => {
    if (info.error && info.error instanceof Error) {
        if (info.level === 'error') {
            info.message = `${info.message}\n${info.error.stack}`;
        }
        delete info.error;
    }
    return info;
};

const logger = winston.createLogger({
    level: config.level,
    format: winston.format.combine(
        winston.format(errorStackTracerFormat)(),
        winston.format.simple()
    ),
    transports: [
        new winston.transports.Console(),
    ],
});

const moduleExport = module.exports = logger;
moduleExport.errorStackTracerFormat = errorStackTracerFormat;
