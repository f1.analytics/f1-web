const logger = require('./logger');
const { statusCodes } = require('./constants');

class ErrorHandler {
	static handleError(error, res) {
		this.__logError(error);
		this.__sendResponse(error, res);
	}

	static setOperationalErrors(error, operationalStatusCodes) {
		if (this.__isErrorOperational(error, operationalStatusCodes)) {
			error.isOperational = true;
		}
	}

	static __sendResponse(error, res) {
		if (error.isOperational || error.response && error.response.status === statusCodes.UNAUTHORIZED) {
			this.__resendResponse(error, res);
		} else {
			this.__sendInternalError(res);
		}
	}

	static __resendResponse(error, res) {
		res.status(error.response.status).send(error.response.data);
	}

	static __sendInternalError(res) {
		res.status(statusCodes.INTERNAL_ERROR).send({
			code: 'internalError',
			message: 'Internal server error',
		});
	}

	static __logError(error) {
		if (error.response) {
			this.__logResponseError(error);
		} else {
			this.__logProgrammerError(error);
		}
	}

	static __logResponseError(error) {
		const errorMessage = error.response.data.error.message;
		const logMessage = error.logMessage ? error.logMessage : 'Server error';
		const logText = `${logMessage}: ${errorMessage}`;

		if (error.isOperational) {
			logger.warn(logText);
		} else {
			logger.error(logText);
		}
	}

	static __logProgrammerError(error) {
		const logMessage = error.logMessage ? error.logMessage : 'Server error';
		const logText = `${logMessage}: ${error.message}`;

		logger.error(logText, { error });
	}

	static __isErrorOperational(error, operationalStatusCodes) {
		return error.response && (operationalStatusCodes.includes(error.response.status));
	}
}

module.exports = ErrorHandler;
