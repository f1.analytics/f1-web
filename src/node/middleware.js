const ErrroHandler = require('./errorHandler');

// eslint-disable-next-line no-unused-vars
const errorHandler = (error, req, res, next) => {
	ErrroHandler.handleError(error, res);
};

const authorizationHandler = (req, res, next) => {
	req.authorization = req.headers['authorization'] || null;
	next();
};

const requiredAuthorizationHandler = (req, res, next) => {
	const authorization = req.headers['authorization'];

	if (!authorization) {
		const error = new Error('Missing authorization header');
		next(error);
	} else {
		req.authorization = authorization;
		next();
	}
};

module.exports = {
	errorHandler,
	authorizationHandler,
	requiredAuthorizationHandler,
};
