import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

jest.mock('react-spinners', () => ({
    RingLoader: () => null
}));

jest.mock('react-router-dom', () => ({
    withRouter: component => component,
    Link: ({ children }) => children
}));

process.env.NODE_ENV = 'production';
